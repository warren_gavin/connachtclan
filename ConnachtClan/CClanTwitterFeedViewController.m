//
//  CClanTwitterFeedViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 29/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanTwitterFeedViewController.h"
#import "CClanTwitterTweetViewCell.h"
#import "CClanTwitter.h"
#import "CClanTweet.h"
#import "CClanBundle.h"
#import "CClanErrorHandler.h"

#import "SWRevealViewController.h"

@interface CClanTwitterFeedViewController ()

@property (nonatomic, copy)   const NSArray        * const timeline;
@property (nonatomic, weak)   const CClanTwitter   * const twitter;
@property (nonatomic)         BOOL                         displayed;

@end

@implementation CClanTwitterFeedViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        self.displayed = NO;
        self.twitter = [CClanTwitter instance];
        
        self.timeline = [self.twitter timeline];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(newTweetsAvailable:)
                                                     name:CCLAN_TWITTER_NEW_TWEETS
                                                   object:self.twitter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(twitterFailed:)
                                                     name:CCLAN_TWITTER_LOAD_FAILED
                                                   object:self.twitter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(twitterDisabled:)
                                                     name:CCLAN_TWITTER_DISABLED
                                                   object:self.twitter];
    }
    
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshTimeline:self.navigationItem.rightBarButtonItem];
    self.displayed = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.displayed = NO;
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    self.timeline = nil;
    [super didReceiveMemoryWarning];
}


- (void) startRefreshing
{
    CGFloat size = self.navigationItem.rightBarButtonItem.width;
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, size, size)];

    [activity setColor:[UIColor blackColor]];
    [activity sizeToFit];
    [activity startAnimating];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activity];
}

- (void) stopRefreshing
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                           target:self
                                                                                           action:@selector(refreshTimeline:)];
}

- (IBAction)refreshTimeline:(UIBarButtonItem *)sender
{
    [self startRefreshing];
    [[CClanTwitter instance] loadTimeline];
}

#pragma mark - Twitter notifications
- (void) twitterFailed:(NSNotification * const) notification
{
    NSError * const error = [notification userInfo][@"error"];
    
    if ( error ) {
        [CClanErrorHandler handleError:error withLoadingView:nil];
    }

    // Kill refreshing animation
    [self stopRefreshing];
}

- (void) newTweetsAvailable:(NSNotification * const) notification
{
    NSArray * const newtimeline = self.twitter.timeline;

    // Only reload the table data if the incoming timeline is different to the
    // one already displayed.
    if ( self.timeline.count != newtimeline.count ) {
        self.timeline = newtimeline;
        if ( self.isViewLoaded ) {
            [self.tableView reloadData];
        }
    }
    else {
        for ( CClanTwitterTweetViewCell * const cell in self.tableView.visibleCells ) {
            [cell resetDisplayRelativeTo:[NSDate date]];
        }
    }

    // Kill refreshing animation
    [self stopRefreshing];
}

- (void) twitterDisabled:(NSNotification *)notification
{
    // Kill refreshing animation if one is running
    [self stopRefreshing];
    self.timeline = nil;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.timeline.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"TwitterCell";
    CClanTwitterTweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ( !cell ) {
        cell = [[CClanTwitterTweetViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:identifier];
    }
    
    [cell displayTweet:self.timeline[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"TwitterCell";
    CClanTwitterTweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    return [cell cellHeightForTweet:self.timeline[indexPath.row]];
}

#pragma mark - Free
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
