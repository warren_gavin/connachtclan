//
//  CClanViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 13/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem  *sidebarButton;

@end
