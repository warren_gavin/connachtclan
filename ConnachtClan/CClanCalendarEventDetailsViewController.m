//
//  CClanCalendarEventDetailsViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 22/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarEventDetailsViewController.h"
#import "CClanCalendarEvent.h"
#import "CClanCalendarEventDetailsView.h"
#import "CClanErrorHandler.h"
#import "CClanBundle.h"
#import "CClanWeatherReportCache.h"
#import "CClanWeatherViewController.h"

#import "UIImage+RoundedCorner.h"
#import "UIImage+Array.h"
#import "NSArray+Blocks.h"
#import "NHCalendarActivity.h"
#import "NHCalendarEvent.h"

#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "EKEvent+CClanEvent.h"
#import "EKEventStore+CClanEvent.h"
#import "EKEventEditViewController+CClanEvent.h"

static NSString * const L_ACTION_ADD    = @"Add to Calendar";
static NSString * const L_ACTION_CANCEL = @"Cancel";
static NSString * const L_ACTION_EDIT   = @"Edit Event";
static NSString * const L_ACTION_DELETE = @"Remove Event";

static NSString * const L_ALERT_CHOICE_YES       = @"Yes";
static NSString * const L_ALERT_CHOICE_NO        = @"No";
static NSString * const L_ALERT_ADD_TO_CALENDAR  = @"Adding to Calendar";
static NSString * const L_ALERT_ADD_MESSAGE      = @"Do you want to set an alarm for this event?";

@interface CClanCalendarEventDetailsViewController () <UIActionSheetDelegate, UIAlertViewDelegate, EKEventEditViewDelegate>

@property (nonatomic, weak)   const CClanCalendarEvent *event;
@property (nonatomic, strong) UIImage * const competition_logo;
@property (nonatomic, strong) UIImage * const broadcaster_logos;
@property (nonatomic, strong) UIActionSheet * const actions;
@property (nonatomic, strong) EKEventStore * const store;
@property (nonatomic, strong) EKEvent      * const stored_event;

@end

@implementation CClanCalendarEventDetailsViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // EKEventStore
    self.store = [[EKEventStore alloc] init];

    // Preload events weather into cache & add weather button
    [self loadWeather];
    UIBarButtonItem *weather = [[UIBarButtonItem alloc] initWithTitle:@"\u2602"
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(presentWeatherViewController)];

    [weather setTitleTextAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:24.0] }
                           forState:UIControlStateNormal];

    self.navigationItem.rightBarButtonItems = @[ self.navigationItem.rightBarButtonItem, weather ];

    [self.eventview setupWithDetails:self.event.url];
}

- (void) viewDidUnload
{
    self.event = nil;
    self.eventview = nil;

    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
         UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        return NO;
    }

    return YES;
}

- (IBAction) userAction:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) loadWeather
{
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([self.event.coordinates[0] floatValue],
                                                                    [self.event.coordinates[1] floatValue]);
    [CClanWeatherReportCache loadWeatherForLocation:self.event.location atCoordinates:coordinates];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CClanWeatherViewController *weather = segue.destinationViewController;
    weather.location = self.event.location;
    weather.coordinates = CLLocationCoordinate2DMake([self.event.coordinates[0] floatValue],
                                                     [self.event.coordinates[1] floatValue]);
}

- (void) presentWeatherViewController
{
    [self performSegueWithIdentifier:@"weather" sender:self];
}

- (void) setEvent:(const CClanCalendarEvent *)event
{
    _event = event;

    id (^createImageFromPath)( id element ) = ^( id element ) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            return [UIImage imageWithContentsOfFile:[CClanBundle thumbnailInBundle:element]];
        }

        return [[UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:element]] roundedCornerImage:4.0
                                                                                             borderSize:1];
    };

    self.competition_logo = [[UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:self.event.competition]] roundedCornerImage:4.0
                                                                                                                         borderSize:1];

    self.broadcaster_logos = [UIImage imageFromArray:[self.event.broadcasters convertWithModifierBlock:createImageFromPath]
                                         separatedBy:10.0];
}

- (void) addEventToDeviceCalendar
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L_ALERT_ADD_TO_CALENDAR
                                                    message:L_ALERT_ADD_MESSAGE
                                                   delegate:self
                                          cancelButtonTitle:L_ACTION_CANCEL
                                          otherButtonTitles:L_ALERT_CHOICE_YES, L_ALERT_CHOICE_NO, nil];
    
    [alert show];
}

#pragma mark - Actions
//! Event actions are to either add an event to the device's calendar or,
//! if the event already exists, to allow the user to edit or delete the
//! event
- (void)displayActions
{
    if ( !self.actions ) {
        NSString *action_title = L_ACTION_ADD;
        NSString *delete_title = nil;

        // If we already have a stored event we don't try to get it a second time
        // this should speed things up a bit
        if ( !self.stored_event ) {
            self.stored_event = [self.store storedClanEvent:self.event];
        }

        if ( self.stored_event ) {
            action_title = L_ACTION_EDIT;
            delete_title = L_ACTION_DELETE;
        }

        self.actions = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:L_ACTION_CANCEL
                                     destructiveButtonTitle:delete_title
                                          otherButtonTitles:action_title, nil];

        [self.actions showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
}

-(NHCalendarEvent *)createCalendarEvent
{
    NHCalendarEvent *calendarEvent = [[NHCalendarEvent alloc] init];
    
    calendarEvent.title = self.event.headline;
    calendarEvent.location = self.event.location;
    calendarEvent.startDate = self.event.date;
    calendarEvent.endDate = [self.event.date dateByAddingTimeInterval:2 * 60 * 60];
    calendarEvent.allDay = NO;
    
    return calendarEvent;
}

- (IBAction)share:(UIBarButtonItem *)sender
{
    if ( [UIActivityViewController class] ) {
        
        NSMutableArray * const items = [NSMutableArray arrayWithObject:[self createCalendarEvent]];
        if ( self.event.url ) {
            [items addObject:[NSURL URLWithString:self.event.url]];
        }
        
        NHCalendarActivity *calendar_activity = [[NHCalendarActivity alloc] init];
        NSArray * const activities = @[ calendar_activity ];

        UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:activities];
        activity.excludedActivityTypes = nil;
        [activity setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if ( completed && [activityType isEqualToString:[calendar_activity activityType]] ) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Event added"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Done", nil];
                
                [alert show];
            }
        }];
        
        [self presentViewController:activity animated:YES completion:nil];
    }
    else {
        // Hand crafted sharing for older version that don't have the
        // UIActivityViewController
        [self displayActions];
    }
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.cancelButtonIndex != buttonIndex ) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];

        if ( [[alertView title] isEqualToString:L_ALERT_ADD_TO_CALENDAR] ) {
            [self.store addClanEvent:self.event
                           withAlert:[[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:L_ALERT_CHOICE_YES]
                          controller:self];
        }
    }
}

#pragma mark - UIActionSheetDelegate
- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( [L_ACTION_ADD isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        [self addEventToDeviceCalendar];
    }
    else if ( [L_ACTION_EDIT isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        EKEventEditViewController *editcontroller = [EKEventEditViewController editorForEvent:self.stored_event
                                                                                        store:self.store
                                                                                     delegate:self];

        [self presentViewController:editcontroller animated:YES completion:nil];
    }
    else if ( [L_ACTION_DELETE isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        [self.store removeEvent:self.stored_event span:EKSpanThisEvent commit:YES error:nil];
        self.stored_event = nil;

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Event Removed"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Done", nil];
        
        [alert show];
    }
    
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    self.actions = nil;
}

#pragma mark - EKEventEditDelegate
- (void) eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    if ( EKEventEditViewActionSaved == action ) {
        self.stored_event = controller.event;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
