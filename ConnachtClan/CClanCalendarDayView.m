//
//  CClanCalendarDayView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarDayView.h"
#import "CClanCalendarWeekViewCell.h"
#import "CClanCalendar.h"
#import "CClanCalendarEvent.h"
#import "CClanBundle.h"

#import "UIImage+RoundedCorner.h"
#import "UIImage+Resize.h"

@implementation CClanCalendarDayView

@synthesize event = _event;

- (void) setDetailsForDate:(NSDate *)date ifInMonth:(NSDate *)month withCalendar:(NSCalendar *)calendar
{
    const NSInteger buttonradius = ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 10 : 3);
    const NSInteger buttonborder = 0;

    // Get the event corresponding to this calendar date
    self.event = [[CClanCalendar instance] calendarEventForDate:date];
    
    // views are reused, reset the alpha in case this was a disabled view on
    // a previous calendar date
    self.alpha = 1.0;

    // Get the button & set the event, set the image overlay 
    const UIButton const *button = (UIButton *)[self viewWithTag:1 + self.tag];
    button.enabled = (self.event != nil);
    [button setImage:[button.imageView.image roundedCornerImage:buttonradius borderSize:buttonborder]
            forState:UIControlStateNormal];

    // Check that the day is within the current month, and disable & dim if not
    const NSDateComponents const *checkmonth  = [calendar components:NSMonthCalendarUnit fromDate:month];
    const NSDateComponents const *activemonth = [calendar components:NSMonthCalendarUnit fromDate:date];
    
    if ( checkmonth.month != activemonth.month ) {
        self.event = nil;
        self.alpha = 0.35;
        button.enabled = NO;
    }

    // display the date
    const UILabel const *day = (UILabel *)[self viewWithTag:3 + self.tag];
    day.text = [NSString stringWithFormat:@"%d", [[calendar components:NSDayCalendarUnit fromDate:date] day]];

    if ( self.event ) {
        UIImage *eventbackground = [[UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"event_bg.png"]] resizedImage:button.frame.size
                                                                                                         interpolationQuality:kCGInterpolationDefault];
        
        [button setBackgroundImage:[eventbackground roundedCornerImage:buttonradius borderSize:buttonborder]
                          forState:UIControlStateNormal];

        const UIImageView const *image = (UIImageView *)[self viewWithTag:2 + self.tag];
        if ( [@"match" isEqualToString:self.event.type] ) {
            image.image = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"connacht_th.png"]];
        }
        else if ( [@"clan event" isEqualToString:self.event.type] ){
            image.image = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"hq_th.png"]];
        }
        
        const UILabel const *detail = (UILabel *)[self viewWithTag:5 + self.tag];
        detail.text = self.event.headline;
    }
    else {
        NSRange           weekdayrange = [calendar maximumRangeOfUnit:NSWeekdayCalendarUnit];
        NSDateComponents *components   = [calendar components:NSWeekdayCalendarUnit fromDate:date];
        
        if (components.weekday == weekdayrange.location || components.weekday == weekdayrange.length) {
            [button setBackgroundImage:[[UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"calendarweekend_bg.png"]] roundedCornerImage:buttonradius
                                                                                                                                       borderSize:buttonborder]
                              forState:UIControlStateNormal];
        }
        else {
            [button setBackgroundImage:[[UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"calendarday_bg.png"]] roundedCornerImage:buttonradius
                                                                                                                                   borderSize:buttonborder]
                              forState:UIControlStateNormal];
        }

        const UIImageView const *image = (UIImageView *)[self viewWithTag:2 + self.tag];
        image.image = nil;

        const UILabel const *detail = (UILabel *)[self viewWithTag:5 + self.tag];
        detail.text = nil;
    }
}

@end
