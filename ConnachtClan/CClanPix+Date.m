//
//  CClanPix+Date.m
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import "CClanPix+Date.h"

@implementation CClanPix (Date)

- (NSComparisonResult) compare:(CClanPix *)toCompare
{
    return [self.creationDate compare:toCompare.creationDate];
}

@end
