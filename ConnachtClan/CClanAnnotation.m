//
//  CClanAnnotation.m
//  ConnachtClan
//
//  Created by Warren Gavin on 30/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanAnnotation.h"
#import "CClanBundle.h"
#import "CClanDownloadedImagesCache.h"
#import "CClanConstants.h"
#import "CClanMapSettings.h"

#import "UIImage+RoundedCorner.h"

NSString * const CCLAN_ANNOTATION_NOTIFY_NEW_ANNOTATIONS = @"be.apokrupto.CClanAnnotation.NewAnnotations";

@implementation CClanAnnotation

+ (UIImage *) imageFromPath:(NSString *)path
{
#if 0
    NSString * const bundlepath = [CClanBundle fileInBundle:path];
    if ( bundlepath ) {
        return [UIImage imageNamed:path];
    }
#endif
    
    return [CClanDownloadedImagesCache imageAtURL:[CCLAN_APP_URL stringByAppendingFormat:@"annotations/v2/%@", path]];
}

+ (CLLocationCoordinate2D) coordinatesFromLocationDetails:(NSArray *)location
{
    CLLocationCoordinate2D  coordinate = { 0, 0 };
    
    if ( location.count == 2 ) {
        coordinate.latitude  = [location[0] doubleValue];
        coordinate.longitude = [location[1] doubleValue];
    }
    
    return coordinate;
}

+ (CClanAnnotation *) createAnnotation:(NSDictionary *)details
{
    CClanAnnotation *annotation = nil;
    
    if ( nil != details[@"annotationimage"] ) {
        CClanExtendedAnnotation *exAnnotation = [[CClanExtendedAnnotation alloc] init];
        exAnnotation.annotationImagePath = [NSString stringWithFormat:@"anno_%@", details[@"annotationimage"]];
        annotation = exAnnotation;
    }
    else {
        annotation = [[CClanAnnotation alloc] init];
    }

    annotation.fixed    = YES;
    annotation.name     = details[@"name"];
    annotation.types    = details[@"type"];
    annotation.image    = [CClanAnnotation imageFromPath:details[@"image"]];
    annotation.address  = details[@"address"];
    annotation.city     = details[@"city"];
    annotation.country  = details[@"country"];
    annotation.phone    = details[@"phone"];
    annotation.website  = details[@"website"];
    annotation.location = [CClanAnnotation coordinatesFromLocationDetails:details[@"location"]];
    annotation.info     = details[@"info"];
    
    return annotation;
}

+ (NSString *) identifier
{
    return @"simple map annotation";
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@   (%f, %f)", self.name, self.location.latitude, self.location.longitude];
}

- (NSString *)details
{
    if ( [_types count] < 2 ) {
        return [_types lastObject];
    }
    
    NSString *subtitle = [[_types subarrayWithRange:NSMakeRange(0, _types.count - 1)] componentsJoinedByString:@", "];
    return [subtitle stringByAppendingFormat:@" & %@", [_types lastObject]];
}

#pragma mark - Annotation view accessory
- (BOOL) shouldDisplay:(NSSet *)showable
{
    if ( [[CClanMapSettings instance] isPermanent:self] ) {
        return YES;
    }
    
    // Annotation should display if one or more of its types
    // are in the collection of displayable annotations
    NSSet *types = [NSSet setWithArray:_types];
    return [types intersectsSet:showable];
}

@end

@implementation CClanExtendedAnnotation

- (instancetype) init
{
    self = [super init];
    
    if ( self ) {
        [self getAnnotationExtensions];
    }
    
    return self;
}

-(instancetype) initWithAnnotationImage:(UIImage *)image
{
    self = [super init];
    
    if ( self ) {
        self.annotationImage = image;
    }
    
    return self;
}

- (void) getAnnotationExtensions
{
    dispatch_queue_t annotation_extension = dispatch_queue_create("download annotation extension", NULL );
    
    dispatch_async( annotation_extension, ^{
        if ( !_annotationImage ) {
            if ( _annotationImagePath ) {
                _annotationImage = [UIImage imageNamed:_annotationImagePath];
            }
            
            if ( !_annotationImage ) {
                _annotationImage = [CClanDownloadedImagesCache imageAtURL:[CCLAN_APP_URL stringByAppendingFormat:@"images/%@", _annotationImagePath]];
            }
        }
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.delegate annotation:self didDownloadAnnotationImage:_annotationImage];
        });
    });
}

+ (NSString *) identifier
{
    return @"extended map annotation";
}

- (BOOL) shouldDisplay:(NSSet *)showable
{
    // Always display extended annotations
    return YES;
}

@end
