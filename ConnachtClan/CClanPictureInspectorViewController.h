//
//  CClanPictureInspectorViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 27/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CClanPix;

@protocol CClanPictureInspectorDelegate <NSObject>

- (void) elementWasSelectedForDeletion:(CClanPix *)image;

@end

//! View controller for a modal display containing a clan photo
@interface CClanPictureInspectorViewController : UIViewController <UIScrollViewDelegate>

//! The inspector can zoom the displayed image and move about in a scroll view
@property (weak, nonatomic) IBOutlet UIScrollView             *scrollView;

//! Inspector delegate to handle saving or deleting
@property (weak, nonatomic) id<CClanPictureInspectorDelegate>  delegate;

//! Sets the image to present in the controller
- (void) setImageToDisplay:(CClanPix *)img;

@end
