//
//  NSString+URL.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSString+URL.h"
#import "NSData+URL.h"

@implementation NSString (URL)

+ (NSString *) stringWithContentsOfURL:(NSURL *)url
                              encoding:(NSStringEncoding)enc
                           cachePolicy:(NSURLRequestCachePolicy)cachePolicy
                       timeoutInterval:(NSTimeInterval)timeoutInterval
{
    NSData * const data = [NSData dataWithContentsOfURL:url cachePolicy:cachePolicy timeoutInterval:timeoutInterval];

    if ( !data ) {
        return nil;
    }
    
    return [[NSString alloc] initWithData:data encoding:enc];
}

@end
