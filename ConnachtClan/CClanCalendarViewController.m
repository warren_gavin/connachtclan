//
//  CClanCalendarViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendar.h"
#import "CClanCalendarViewController.h"
#import "CClanCalendarEvent.h"
#import "CClanCalendarEventDetailsViewController.h"
#import "CClanCalendarEventCell.h"
#import "CClanCalendarEventHeaderViewCell.h"
#import "CClanMatchPreviewViewController.h"
#import "CClanBundle.h"

#import "NSArray+Blocks.h"

#import "EKEventStore+CClanEvent.h"
#import "EKEventEditViewController+CClanEvent.h"
#import "NHCalendarEvent.h"
#import "NHCalendarActivity.h"

static NSString *L_HEADER_IDENTIFIER = @"Calendar Event Header";
static NSString *L_EVENT_IDENTIFIER  = @"Calendar Event";

static NSString *L_ACTION_CANCEL    = @"Cancel";
static NSString *L_ACTION_DELETE    = @"Delete";
static NSString *L_ACTION_EDIT      = @"Edit";
static NSString *L_ACTION_ALARM_ON  = @"On";
static NSString *L_ACTION_ALARM_OFF = @"Off";

@interface CClanCalendarViewController () <CClanCalendarDelegate, UIAlertViewDelegate, UIActionSheetDelegate, EKEventEditViewDelegate>

@property (nonatomic, strong) const CClanCalendarEvent * const event;
@property (nonatomic, strong) const UIActionSheet      * const actions;

@end

@implementation CClanCalendarViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // If there are no events available for display we load them
    if ( ![[CClanCalendar instance] numberOfMonthsWithEvents] ) {
        [self.loading showLoadingDisplay];
        [[CClanCalendar instance] setDelegate:self];
        [[CClanCalendar instance] loadEvents];
    }
    else {
        [self.loading hideLoadingDisplay];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // If we're not visible we've just loaded, scroll to the right month
    if ( !self.table.visibleCells.count ) {
        [self scrollToCurrentMonth];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
         UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        return NO;
    }

    return YES;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:@"match details"] ) {
        CClanCalendarEvent *event = sender;
        [segue.destinationViewController setEvent:event];
    }
    else if ( [segue.identifier isEqualToString:@"match report"] ) {
        NSString *url = sender;
        [segue.destinationViewController setUrl:url];
    }

}

#pragma mark - Table view data source

//! Each calendar month is a section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[CClanCalendar instance] numberOfMonthsWithEvents];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Add one for the header cell
    return [[CClanCalendar instance] numberOfEventsForMonthAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CClanCalendarEventCell *cell = [self.table dequeueReusableCellWithIdentifier:L_EVENT_IDENTIFIER];
    [cell setEventData:[[CClanCalendar instance] eventsForMonth:indexPath.section][indexPath.row]];
    
    return cell;
}

// This delegate method must be implemented in order to provide a button when swiping cells
- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if  ( !self.actions ) {
        CClanCalendarEventCell *cell         = (CClanCalendarEventCell *)[tableView cellForRowAtIndexPath:indexPath];
        EKEventStore           *store        = [[EKEventStore alloc] init];
        EKEvent                *stored_event = [store storedClanEvent:cell.event];
        
        self.event = cell.event;
        
        if ( stored_event ) {
            self.actions = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:L_ACTION_CANCEL
                                         destructiveButtonTitle:L_ACTION_DELETE
                                              otherButtonTitles:L_ACTION_EDIT, nil];
        }
        else {
            self.actions = [[UIActionSheet alloc] initWithTitle:@"Set alarm for this event?"
                                                       delegate:self
                                              cancelButtonTitle:L_ACTION_CANCEL
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:L_ACTION_ALARM_ON, L_ACTION_ALARM_OFF, nil];
        }
        
        [self.actions showInView:tableView];
    }
}

#pragma mark - UITableViewDelegate
- (NSString *) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CClanCalendarEventCell *cell = (CClanCalendarEventCell *)[tableView cellForRowAtIndexPath:indexPath];
    EKEventStore           *store = [[EKEventStore alloc] init];
    EKEvent                *event = [store storedClanEvent:cell.event];

    if ( event ) {
        return @"Edit";
    }
    
    return @"Add";
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CClanCalendarEventHeaderViewCell *cell = [self.table dequeueReusableCellWithIdentifier:L_HEADER_IDENTIFIER];
    cell.month.text = [[CClanCalendar instance] titleForMonthAtIndex:section];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CClanCalendarEventHeaderViewCell *cell = [self.table dequeueReusableCellWithIdentifier:L_HEADER_IDENTIFIER];
    return cell.frame.size.height;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CClanCalendarEventCell *cell = (CClanCalendarEventCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if ( cell.event.url.length ) {
        [self performSegueWithIdentifier:@"match report" sender:cell.event.url];
    }
    else {
        [self performSegueWithIdentifier:@"match details" sender:cell.event];
    }
}

#pragma mark - CClanCalendarDelegate
- (void) scrollToCurrentMonth
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:[[CClanCalendar instance] currentMonthIndex]];
    [self.table scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void) calendarDidFinishLoadingEvents:(CClanCalendar *)calendar
{
    dispatch_async( dispatch_get_main_queue(), ^{
        [self.loading hideLoadingDisplay];
        [self.table reloadData];
        [self scrollToCurrentMonth];
    });
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.cancelButtonIndex != buttonIndex ) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - UIActionSheetDelegate
- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    EKEventStore *store = [[EKEventStore alloc] init];
    EKEvent *event = [store storedClanEvent:self.event];

    if ( [L_ACTION_EDIT isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        EKEventEditViewController *editcontroller = [EKEventEditViewController editorForEvent:event
                                                                                        store:store
                                                                                     delegate:self];
        
        [self presentViewController:editcontroller animated:YES completion:nil];
    } else if ( [L_ACTION_DELETE isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        [store removeEvent:event span:EKSpanThisEvent commit:YES error:nil];

        self.event = nil;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Event Removed"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Done", nil];
        
        [alert show];
    } else if ( [L_ACTION_ALARM_ON isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        [store addClanEvent:self.event withAlert:YES controller:self];
    } else if ( [L_ACTION_ALARM_OFF isEqualToString:[actionSheet buttonTitleAtIndex:buttonIndex]] ) {
        [store addClanEvent:self.event withAlert:NO controller:self];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    self.actions = nil;
}

#pragma mark - EKEventEditDelegate
- (void) eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)displayActions:(id)sender
{
    NSArray *events = [[[CClanCalendar instance] allEvents] convertWithModifierBlock:^id(id element) {
        CClanCalendarEvent *clanEvent = element;
        if ( [clanEvent.date compare:[NSDate date]] == NSOrderedAscending ) {
            return nil;
        }
        
        NHCalendarEvent *calendarEvent = [[NHCalendarEvent alloc] init];
        
        calendarEvent.title = clanEvent.headline;
        calendarEvent.location = clanEvent.location;
        calendarEvent.startDate = clanEvent.date;
        calendarEvent.endDate = [clanEvent.date dateByAddingTimeInterval:2 * 60 * 60];
        calendarEvent.allDay = NO;
        
        return calendarEvent;
    }];
    
    NHCalendarActivity * const calendarActivity = [[NHCalendarActivity alloc] init];
    calendarActivity.title = NSLocalizedString(@"Save all events to Calendar", nil);

    NSArray * const activities = @[ calendarActivity ];
    UIActivityViewController * const activity = [[UIActivityViewController alloc] initWithActivityItems:events applicationActivities:activities];

    activity.excludedActivityTypes = @[ UIActivityTypePostToFacebook,
                                        UIActivityTypePostToTwitter,
                                        UIActivityTypePostToWeibo,
                                        UIActivityTypeMessage,
                                        UIActivityTypeMail,
                                        UIActivityTypePrint,
                                        UIActivityTypeCopyToPasteboard,
                                        UIActivityTypeAssignToContact,
                                        UIActivityTypeSaveToCameraRoll,
                                        UIActivityTypeAddToReadingList,
                                        UIActivityTypePostToFlickr,
                                        UIActivityTypePostToVimeo,
                                        UIActivityTypePostToTencentWeibo,
                                        UIActivityTypeAirDrop ];

    [activity setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if ( completed && [activityType isEqualToString:[calendarActivity activityType]] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Events added"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Done", nil];
            
            [alert show];
        }
    }];
    
    [self presentViewController:activity animated:YES completion:nil];
}

@end
