//
//  CClanDownloadedHtmlCache.h
//  ConnachtClan
//
//  Created by Warren Gavin on 24/09/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CClanDownloadedHtmlCache;
@class CClanArticle;

@protocol CClanDownloadedHtmlCacheDelegate <NSObject>

- (void) htmlCache:(const CClanDownloadedHtmlCache *)cache didDownloadArticle:(const CClanArticle *)article;

@end

@interface CClanDownloadedHtmlCache : NSObject

+ (void) downloadHtmlAtUrl:(NSString * const)url withDelegate:(id<CClanDownloadedHtmlCacheDelegate>)delegate;

@end
