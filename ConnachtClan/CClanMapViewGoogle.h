//
//  CClanMapViewGoogle.h
//  ConnachtClan
//
//  Created by Warren Gavin on 11/11/13.
//
//

#if defined(CCLAN_MAP_USE_GOOGLE)
#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CClanMapView.h"

@interface CClanMapViewGoogle : NSObject <CClanMapView, GMSMapViewDelegate>

@end

#endif