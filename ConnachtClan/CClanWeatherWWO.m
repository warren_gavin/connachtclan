//
//  CClanWeatherWWO.m
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanWeatherWWO.h"
#import "CClanWeatherImagesCache.h"
#import "NSArray+Blocks.h"
#import "NSDate+Friendly.h"

#define L_CODE_MAP_KEYED_ENTRY(key, object) [NSNumber numberWithInteger:object], [NSString stringWithFormat:@"%d", key]

static const NSUInteger L_WIND_INDICATOR_THRESHOLD = 50;
static const NSDictionary *g_codemap;

@implementation CClanWeatherWWO

- (UIImage *) imageForWeatherConditions:(const NSDictionary *)weather
{
    if ( !g_codemap ) {
        g_codemap = [NSDictionary dictionaryWithObjectsAndKeys:
                                         L_CODE_MAP_KEYED_ENTRY( 113, CClanWeatherImagesCacheSunny ),
                                         
                                         L_CODE_MAP_KEYED_ENTRY( 116, CClanWeatherImagesCacheFair ),
                                         
                                         L_CODE_MAP_KEYED_ENTRY( 143, CClanWeatherImagesCacheFog ),
                                         L_CODE_MAP_KEYED_ENTRY( 248, CClanWeatherImagesCacheFog ),
                                         L_CODE_MAP_KEYED_ENTRY( 260, CClanWeatherImagesCacheFog ),
                                         
                                         L_CODE_MAP_KEYED_ENTRY( 119, CClanWeatherImagesCacheCloudy ),
                                         L_CODE_MAP_KEYED_ENTRY( 122, CClanWeatherImagesCacheCloudy ),
                                         
                                         @(CClanWeatherImagesCacheLightShowers),
                                         [NSString stringWithFormat:@"%d", 176],
                                         @(CClanWeatherImagesCacheLightShowers),
                                         [NSString stringWithFormat:@"%d", 263],
                                         @(CClanWeatherImagesCacheLightShowers),
                                         [NSString stringWithFormat:@"%d", 353],
                                         
                                         @(CClanWeatherImagesCacheShowers),
                                         [NSString stringWithFormat:@"%d", 299],
                                         @(CClanWeatherImagesCacheShowers),
                                         [NSString stringWithFormat:@"%d", 305],
                                         @(CClanWeatherImagesCacheShowers),
                                         [NSString stringWithFormat:@"%d", 356],
                                         
                                         @(CClanWeatherImagesCacheLightRain),
                                         [NSString stringWithFormat:@"%d", 266],
                                         @(CClanWeatherImagesCacheLightRain),
                                         [NSString stringWithFormat:@"%d", 293],
                                         @(CClanWeatherImagesCacheLightRain),
                                         [NSString stringWithFormat:@"%d", 296],
                                         
                                         @(CClanWeatherImagesCacheRain),
                                         [NSString stringWithFormat:@"%d", 302],
                                         @(CClanWeatherImagesCacheRain),
                                         [NSString stringWithFormat:@"%d", 308],
                                         @(CClanWeatherImagesCacheRain),
                                         [NSString stringWithFormat:@"%d", 359],
                                         
                                         @(CClanWeatherImagesCacheScatteredThunder),
                                         [NSString stringWithFormat:@"%d", 200],
                                         @(CClanWeatherImagesCacheScatteredThunder),
                                         [NSString stringWithFormat:@"%d", 386],
                                         @(CClanWeatherImagesCacheScatteredThunder),
                                         [NSString stringWithFormat:@"%d", 392],
                                         
                                         @(CClanWeatherImagesCacheThunder),
                                         [NSString stringWithFormat:@"%d", 389],
                                         
                                         @(CClanWeatherImagesCacheSleetShowers),
                                         [NSString stringWithFormat:@"%d", 179],
                                         @(CClanWeatherImagesCacheSleetShowers),
                                         [NSString stringWithFormat:@"%d", 362],
                                         @(CClanWeatherImagesCacheSleetShowers),
                                         [NSString stringWithFormat:@"%d", 365],
                                         @(CClanWeatherImagesCacheSleetShowers),
                                         [NSString stringWithFormat:@"%d", 374],
                                         
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 182],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 185],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 281],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 284],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 311],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 314],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 317],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 350],
                                         @(CClanWeatherImagesCacheSleet),
                                         [NSString stringWithFormat:@"%d", 377],
                                         
                                         @(CClanWeatherImagesCacheLightSnow),
                                         [NSString stringWithFormat:@"%d", 227],
                                         @(CClanWeatherImagesCacheLightSnow),
                                         [NSString stringWithFormat:@"%d", 320],
                                         @(CClanWeatherImagesCacheLightSnow),
                                         [NSString stringWithFormat:@"%d", 323],
                                         @(CClanWeatherImagesCacheLightSnow),
                                         [NSString stringWithFormat:@"%d", 326],
                                         @(CClanWeatherImagesCacheLightSnow),
                                         [NSString stringWithFormat:@"%d", 368],
                                         
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 230],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 329],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 332],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 335],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 338],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 371],
                                         @(CClanWeatherImagesCacheSnow),
                                         [NSString stringWithFormat:@"%d", 395],
                                         
                                         nil];
    }

    NSNumber *code = weather[@"weatherCode"];

    // No astronomy info is passed with the weather condition data, but the URL for the weather
    // icon can be one of two images, the second one is postfixed "_night" and is only referenced
    // when the icon used should be a night time icon. If we find this string in the icon URL
    // we can be safe in the knowledge that we should also display nighttime icons.
    NSString *icon    = weather[@"weatherIconUrl"][0][@"value"];
    BOOL      daytime = (0 == [icon rangeOfString:@"_night"].length );
    
    NSNumber *mappedcode = g_codemap[[NSString stringWithFormat:@"%@", code]];
    return [CClanWeatherImagesCache imageForCondition:[mappedcode intValue] isDaytime:daytime];
}

- (UIImage *) imageIfWindy:(const NSDictionary *)weather
{
    if ( L_WIND_INDICATOR_THRESHOLD <= [weather[@"windspeedKmph"] intValue] ) {
        return [CClanWeatherImagesCache imageForCondition:CClanWeatherImagesCacheWind isDaytime:YES];
    }
    
    return nil;
}

- (void) parseWeather:(NSData * const) weather
{
    if ( !weather ) {
        return;
    }

    const NSDictionary *weather_json = [NSJSONSerialization JSONObjectWithData:weather
                                                                       options:0
                                                                         error:nil];
    const NSDictionary *data     = weather_json[@"data"];
    const NSDictionary *current  = data[@"current_condition"][0];
    const NSArray      *forecast = data[@"weather"];
    
    self.current = [[CClanWeatherItem alloc] init];
    self.current.day            = NSLocalizedString(@"Right Now", nil);
    self.current.temperature    = [NSString stringWithFormat:@"%@°", current[@"temp_C"]];
    self.current.description    = current[@"weatherDesc"][0][@"value"];
    self.current.details        = [NSString stringWithFormat:@"Cloud cover: %@%%\nPrecipitation: %@mm\nWind: %@kmph %@",
                                               current[@"cloudcover"],
                                               current[@"precipMM"],
                                               current[@"windspeedKmph"],
                                               current[@"winddir16Point"]];

    self.current.primary_icon   = [self imageForWeatherConditions:current];
    self.current.secondary_icon = [self imageIfWindy:current];
    
    id (^convertWWOForecast)( id element ) = ^( id element ) {
        const NSDictionary     *weather   = element;
        const CClanWeatherItem *item      = [[CClanWeatherItem alloc] init];
        const NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];

        item.highlow        = [NSString stringWithFormat:@"hi %@° lo %@°", weather[@"tempMaxC"], weather[@"tempMinC"]];
        item.description    = weather[@"weatherDesc"][0][@"value"];
        item.primary_icon   = [self imageForWeatherConditions:weather];
        item.secondary_icon = [self imageIfWindy:weather];
        
        [formatter setDateFormat:@"YYYY-MM-dd"];
        item.day = [[formatter dateFromString:weather[@"date"]] friendlyDayOfWeek];
        
        return item;
    };
    
    self.forecast = [forecast convertWithModifierBlock:convertWWOForecast];
}

- (void) dealloc
{
    g_codemap = nil;
}

@end
