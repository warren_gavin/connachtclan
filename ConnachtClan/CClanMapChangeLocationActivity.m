#import "CClanMapActivity.h"
#import "CClanMapChangeLocationActivity.h"

@implementation CClanMapChangeLocationActivity

+ (CClanMapChangeLocationActivity *) activityWithDelegate:(id<CClanMapChangeLocationActivityDelegate>) delegate
{
    CClanMapChangeLocationActivity *activity = [[CClanMapChangeLocationActivity alloc] init];
    activity.delegate = delegate;

    return activity;
}

- (NSString *) activityType
{
    return @"be.apokrupto.CClanMapChangeLocationActivity";
}

- (NSString *) activityTitle
{
    return NSLocalizedString(@"Change Map", nil);
}

- (UIImage *) activityImage
{
    return [UIImage imageNamed:@"CClanMapChangeLocationActivity"];
}

-(BOOL) canPerformWithActivityItems:(NSArray *)activityItems
{
    for (id item in activityItems) {
        if ( [item isKindOfClass:[CClanMapActivity class]] ) { 
            return YES;
        }
    }
    
    return NO;
}

- (void) prepareWithActivityItems:(NSArray *)activityItems
{
}

- (void) performActivity
{
    [self.delegate displayChangeLocation];
}

@end
