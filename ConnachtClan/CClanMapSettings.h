//
//  CClanMapSettings.h
//  ConnachtClan
//
//  Created by Warren Gavin on 03/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanMapView.h"

@class CClanAnnotation;

//! Delegate to act on changes to display settings for a map
@protocol CClanMapViewSettingsDelegate <NSObject>

//! Show standard map view with labels
- (void) setMapViewStandard;

//! Show satellite view
- (void) setMapViewSatellite;

//! Show satellite view overlaid with labels
- (void) setMapViewHybrid;

//! Switch on/off showing bars
- (void) showBars:(BOOL) selection;

//! Switch on/off showing cafes
- (void) showCafes:(BOOL) selection;

//! Switch on/off showing restaurants
- (void) showRestaurants:(BOOL) selection;

//! Switch on/off showing accommodation
- (void) showAccommodation:(BOOL) selection;

- (void) markLocation:(const CClanAnnotation * const)location permanent:(BOOL)selection;
@end

//! The display settings for a map
@interface CClanMapSettings : NSObject

//! Map location
@property (nonatomic, strong) NSString *location;

//! Map type to display, standard, satellite or hybrid
@property (nonatomic) CClanMapViewType maptype;

//! Map should show bars
@property (nonatomic) BOOL showbars;

//! Map should show cafes
@property (nonatomic) BOOL showcafes;

//! Map should show restaurants
@property (nonatomic) BOOL showrestaurants;

//! Map should show accommodation
@property (nonatomic) BOOL showaccommodation;

+ (CClanMapSettings *) instance;

- (NSSet *)showable;

- (void) addPermanent:(const CClanAnnotation * const)location;

- (void) removePermanent:(const CClanAnnotation * const)location;

- (BOOL) isPermanent:(const CClanAnnotation *const)location;
@end
