//
//  NSArray+Plist.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSArray (Plist)

+ (NSArray *) arrayFromPlistAtURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval;

@end
