//
//  NHCalendarActivity+Cleanup.h
//  ConnachtClan
//
//  Created by Warren Gavin on 16/02/13.
//
//

#import "NHCalendarActivity.h"

@interface NHCalendarActivity (Cleanup)

- (void) removeDuplicateEvent:(EKEvent *)event inEventStore:(EKEventStore *)eventStore;

@end
