//
//  CClanSharing.m
//  ConnachtClan
//
//  Created by Warren Gavin on 05/01/13.
//
//

#import "CClanSharing.h"
#import <Social/Social.h>

#import <MessageUI/MessageUI.h>

static const CClanSharing *g_instance;

@interface CClanSharing ()

@property (nonatomic) BOOL enabled;
@property (nonatomic) BOOL email;
@property (nonatomic) BOOL tweet;

//! When providing a mail composition view controller we maintain a pointer
//! to the parent view controller, so we can dismiss the mail VC in the
//! delegate method mailComposeController:didFinishWithResult:error:
@property (nonatomic, copy) CClanSharingCompletionHandler handler;

@end

@implementation CClanSharing

+ (const CClanSharing * const) instance
{
    if ( !g_instance ) {
        g_instance = [[CClanSharing alloc] init];
    }
    
    return g_instance;
}

- (UINavigationController *) mailControllerWithSubject:(NSString * const)subject
                                            recipients:(NSArray  * const)recipients
                                           messageBody:(NSString * const)body
                                           attachement:(NSData   * const)attachment
                                    attachmentMimeType:(NSString * const)mime
                                    attachmentFilename:(NSString * const)filename
                                          onCompletion:(CClanSharingCompletionHandler)handler;
{
    self.handler = handler;
    
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    [mail setSubject:subject];
    [mail setMessageBody:body isHTML:YES];  // Always set to HTML in case of links in body, no harm if not
    [mail setToRecipients:recipients];
    
    if ( attachment && mime && filename ) {
        [mail addAttachmentData:attachment
                       mimeType:mime
                       fileName:filename];
    }
    
    return mail;
}

- (UIViewController *) twitterControllerWithText:(NSString * const)text
                                             url:(NSURL * const)url
                                           image:(UIImage * const)image
                                    onCompletion:(CClanSharingCompletionHandler)handler;
{
    SLComposeViewController *tweet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    if ( text ) {
        [tweet setInitialText:text];
    }
    
    if ( url ) {
        [tweet addURL:url];
    }
    
    if ( image ) {
        [tweet addImage:image];
    }

    [tweet setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
        handler(nil);
    }];
    
    return tweet;
}

- (id) init
{
    self = [super init];
    
    if ( self ) {
        dispatch_queue_t get_sharing_details = dispatch_queue_create("Sharing Details", NULL );
        
        dispatch_async( get_sharing_details, ^{
            self.email = [MFMailComposeViewController canSendMail];
            self.tweet = [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
        });
    }
    
    return self;
}

- (BOOL) enabled
{
    // Sharing is enabled if any of the possible methods are available
    return (self.email || self.tweet);
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    self.handler(error);
}

@end
