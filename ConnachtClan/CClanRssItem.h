//
//  CClanRssItem.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CClanRssItem : NSObject

@property (nonatomic, strong) NSString * const title;
@property (nonatomic, strong) NSString * const link;
@property (nonatomic, strong) NSString * const description;
@property (nonatomic, strong) NSDate   * const date;
@property (nonatomic, strong) UIImage  * const icon;

@end
