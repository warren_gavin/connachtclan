//
//  CClanMatchDetailsCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CClanMatchDetailsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *homeTeam;
@property (nonatomic, weak) IBOutlet UIImageView *awayTeam;
@property (nonatomic, weak) IBOutlet UILabel     *location;
@property (nonatomic, weak) IBOutlet UILabel     *details;

@end
