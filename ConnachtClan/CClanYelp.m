//
//  CClanYelp.m
//  ConnachtClan
//
//  Created by Warren Gavin on 05/10/13.
//
//

#import "CClanYelp.h"
#import "CClanYelpAPIv2_0.h"
#import "CClanYelpResult.h"
#import "CClanAnnotation.h"
#import "CClanLocation.h"

#import "NSArray+Blocks.h"
#import <MapKit/MapKit.h>

typedef void (^YelpSearchCompletionHandler)(NSArray *hits);

@implementation CClanYelp

+ (void) notifyResults:(NSArray *)hits forLocation:(NSString *)location inRegion:(CClanMapViewRegion)region
{
    dispatch_async( dispatch_get_main_queue(), ^{
        NSDictionary *results = @{ @"location" : location, @"results" : hits };
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_NEW_MAP_SEARCH_HITS object:results];
    });
}

+ (void) performSearchForCategory:(NSString *)category
                       atLatitude:(CGFloat)latitude
                     andLongitude:(CGFloat)longitude
            withCompletionHandler:(YelpSearchCompletionHandler)onCompletion
{
    dispatch_queue_t category_search = dispatch_queue_create("Yelp Category Search", NULL );
    
    dispatch_async( category_search, ^{
        @synchronized(self) {
            onCompletion( [CClanYelpAPIv2_0 performSearchForCategory:category
                                                          atLatitude:latitude
                                                        andLongitude:longitude] );
        }
    });
}

+ (void) refineSearchResults:(NSArray *)results forLocation:(NSString *)location inRegion:(CClanMapViewRegion)region
{
    __block NSUInteger  numAnnotations   = results.count;
    NSMutableArray     *foundAnnotations = [NSMutableArray array];
    
    for ( CClanAnnotation *annotation in results ) {
        CLLocationCoordinate2D centre   = CLLocationCoordinate2DMake(region.latitude, region.longitude);
        MKCoordinateSpan       span     = MKCoordinateSpanMake(region.latitudeDelta, region.longitudeDelta);
        
        MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
        request.naturalLanguageQuery  = [NSString stringWithFormat:@"%@", annotation.name];
        request.region                = MKCoordinateRegionMake(centre, span);
        
        MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
        [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
            dispatch_queue_t process_search = dispatch_queue_create("Apple Search response", NULL );
            
            dispatch_async( process_search, ^{
                for ( MKMapItem *item in response.mapItems ) {
                    if ( [CClanLocation coordinate:item.placemark.coordinate isInRegion:region] ) {
                        annotation.location = item.placemark.coordinate;
                        @synchronized(self) {
                            [foundAnnotations addObject:annotation];
                        }
                        break;
                    }
                }
                
                @synchronized(self) {
                    if ( --numAnnotations == 0 && foundAnnotations.count ) {
                        [CClanYelp notifyResults:foundAnnotations forLocation:location inRegion:region];
                    }
                }
            });
        }];
    }
}

+ (void) performSearchForLocation:(NSString *)location inRegion:(CClanMapViewRegion)region
{
    dispatch_queue_t yelp_search = dispatch_queue_create("Yelp Search", NULL );
    
    dispatch_async( yelp_search, ^{
        __block NSMutableArray *allHits = [NSMutableArray array];
        __block NSArray        *allCategories = @[ @"Bar", @"Restaurant", @"Cafe", @"Accommodation" ];
        __block NSUInteger      searchCount = 0;
        for ( NSString * category in allCategories ) {
            [self performSearchForCategory:category
                                atLatitude:region.latitude
                              andLongitude:region.longitude
                     withCompletionHandler:^(NSArray *hits) {
                         ++searchCount;
                         [allHits addObjectsFromArray:hits];

                         if ( searchCount == allCategories.count ) {
                             NSArray *mergedHits = [CClanYelp mergeSearchResults:allHits];
                             
                             if ( mergedHits.count ) {
                                 NSArray *hits = [mergedHits convertWithModifierBlock:^id(id element) {
                                     CClanYelpResult *result = element;
                                     return [result annotation];
                                 }];
                                 
                                 [CClanYelp refineSearchResults:hits forLocation:location inRegion:region];
                             }
                         }
                     }];
        }
    });
}

+ (NSArray *) mergeSearchResults:(NSMutableArray *)results
{
    NSMutableArray *mergedResults = [NSMutableArray arrayWithCapacity:results.count];

    for (CClanYelpResult *result in results) {
        if ( ![mergedResults containsObject:result] ) {
            [mergedResults addObject:result];
        }
        else {
            CClanYelpResult *resultToMerge = mergedResults[[mergedResults indexOfObject:result]];
            [resultToMerge addCategories:result.categories];
        }        
    }
    
    return mergedResults;
}

@end
