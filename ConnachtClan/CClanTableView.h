//
//  CClanTableView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 15/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CClanTableViewCell : UITableViewCell

@property (nonatomic, readonly) CGFloat height;

@end

@interface CClanTableView : UITableView

@end
