//
//  CClanPicturesViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 27/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanLoadingView.h"
#import "AQGridView.h"

//! Display view of the Clan photos collection
@interface CClanPicturesViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource>

//! The display containing the clan photos
@property (weak, nonatomic) IBOutlet AQGridView  *grid;
@property (weak, nonatomic) IBOutlet UILabel     *description;
@property (weak, nonatomic) IBOutlet UIImageView *descriptionbg;

//! Activity indicator, used when reading & writing the photo album and
//! reloading the table
@property (weak, nonatomic) IBOutlet CClanLoadingView *loading;

@end
