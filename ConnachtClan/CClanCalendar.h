//
//  CClanCalendar.h
//  ConnachtClan
//
//  Created by Warren Gavin on 20/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CClanCalendar;

@protocol CClanCalendarDelegate

//! Called when the calendar has completed event loading. Events are 
//! downloaded and have an associated lag, so the delegate has to wait
//! until everything is finished.
- (void) calendarDidFinishLoadingEvents:(CClanCalendar *)calendar;

@end

@interface CClanCalendar : NSObject

@property (nonatomic, weak) id<CClanCalendarDelegate> delegate;

//! Chronologically ordered list of all events as CClanCalendarEvent objects
@property (nonatomic, strong, readonly) NSArray *allEvents;

//! Singleton instance
+ (const CClanCalendar * const) instance;

//! Get all events for calendar
- (void) loadEvents;

//! Number of individual months that have calendar events
- (NSUInteger) numberOfMonthsWithEvents;

//! Number of events in a given month. The index is the nth month,
//! in chronological order
- (NSUInteger) numberOfEventsForMonthAtIndex:(NSUInteger)index;

//! Array of events for the nth month. The events are in chronological order
- (NSArray *) eventsForMonth:(NSUInteger)index;

//! The name & year of the nth month
- (NSString *) titleForMonthAtIndex:(NSUInteger)index;

//! Index of the current month
- (NSUInteger) currentMonthIndex;

//! Indicates if a certain date is day or night time depending on the sunrise and 
//! sunset information for a given astronomy information set
+ (BOOL) isDaytimeForDate:(const NSDate *)date withAstronomy:(const NSDictionary *)astronomy;

@end
