//
//  CClanHtmlDownloader.h
//  ConnachtClan
//
//  Created by Warren Gavin on 24/09/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CClanHtmlDownloader;
@class CClanArticle;

@protocol CClanHtmlDownloaderDelegate <NSObject>
- (void) htmlDownloader:(const CClanHtmlDownloader *)downloader didFinishDownloadingArticle:(const CClanArticle *)article;
@end

@interface CClanHtmlDownloader : NSObject

@property (nonatomic, weak) id <CClanHtmlDownloaderDelegate> delegate;
@property (nonatomic, weak) NSString *url;

- (id) initWithUrl:(NSString * const)url;
- (void) download;

@end
