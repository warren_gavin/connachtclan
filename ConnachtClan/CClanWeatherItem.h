//
//  CClanWeatherItem.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CClanWeatherItem : NSObject

@property (nonatomic, strong) NSString *day;
@property (nonatomic, strong) NSString *temperature;
@property (nonatomic, strong) NSString *highlow;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *details;
@property (nonatomic, strong) UIImage  *primary_icon;
@property (nonatomic, strong) UIImage  *secondary_icon;

@end
