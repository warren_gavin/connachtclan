//
//  CClanLocation.m
//  ConnachtClan
//
//  Created by Warren Gavin on 31/10/13.
//
//

#import "CClanLocation.h"

@implementation CClanLocation

+ (BOOL) coordinate:(CLLocationCoordinate2D)coordinates isInRegion:(CClanMapViewRegion)region
{
    return ((region.latitude  - region.latitudeDelta  < coordinates.latitude)  &&
            (region.latitude  + region.latitudeDelta  > coordinates.latitude)  &&
            (region.longitude - region.longitudeDelta < coordinates.longitude) &&
            (region.longitude + region.longitudeDelta > coordinates.longitude));
}

@end
