//
//  CClanCalendarMonthView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

//! An individual month as shown in a calendar view controller
@interface CClanCalendarMonthView : UITableViewCell <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSDate *month;

@end
