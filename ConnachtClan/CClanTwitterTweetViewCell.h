//
//  CClanTwitterTweetViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 30/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanTableView.h"

@class CClanTweet;

@interface CClanTwitterTweetTitle : CClanTableViewCell

@property (nonatomic, weak) IBOutlet UILabel     *user;
@property (nonatomic, weak) IBOutlet UILabel     *interval;

@end

@interface CClanTwitterTweetBody : CClanTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UITextView  *tweetTextView;

- (void) setTweet:(const CClanTweet * const)tweet;

@end

@interface CClanTwitterTweetRetweetDetails : CClanTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *retweetedBy;

@end

@interface CClanTwitterTweetViewCell : CClanTableViewCell <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void) displayTweet:(const CClanTweet * const)tweet;
- (void) resetDisplayRelativeTo:(NSDate * const)time;
- (CGFloat) cellHeightForTweet:(const CClanTweet * const)tweet;

@end
