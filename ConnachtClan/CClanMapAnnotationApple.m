//
//  CClanMapAnnotationiOS.m
//  ConnachtClan
//
//  Created by Warren Gavin on 10/11/13.
//
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanMapAnnotationApple.h"

@implementation CClanMapAnnotationApple

@synthesize annotation = _annotation;

- (CLLocationCoordinate2D)coordinate
{
    return _annotation.location;
}

- (NSString *)title
{
    return _annotation.name;
}

- (NSString *)subtitle
{
    return [_annotation details];
}

- (BOOL) isEqual:(id)object
{
    CClanMapAnnotationApple *mapAnnotation = object;
    return [_annotation.description isEqualToString:mapAnnotation.annotation.description];
}

@end

#endif