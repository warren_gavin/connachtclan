//
//  CClanPictureInspectorViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 27/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanPictureInspectorViewController.h"
#import "CClanPix.h"
#import "CClanBundle.h"
#import "CClanErrorHandler.h"
#import "CClanConstants.h"

static NSString *L_ALERT_DELETE_TITLE   = @"Delete?";
static NSString *L_ALERT_DELETE_MESSAGE = @"Do you want to delete this photo? Photos saved to the camera roll will still be available there.";

@interface CClanPictureInspectorViewController () <UIAlertViewDelegate>

//! The view in which the photo is displayed
@property (strong, nonatomic) UIImageView *imageView;
@property (weak,   nonatomic) CClanPix    *asset;

@end

@implementation CClanPictureInspectorViewController

- (UIImageView *)imageView
{
    if ( !_imageView ) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    
    return _imageView;
}

- (void) setImage
{
    if ( _scrollView ) {
        _scrollView.contentSize = CGSizeZero;
        _imageView.image = nil;
        
        // set the image to display
        UIImage *image = [UIImage imageWithData:self.asset.imageData];
        if ( image ) {
            CGRect  top    = self.navigationController.navigationBar.frame;

            // Scroll view content size is the image plus the tab bar and navigation bars, so allow the image
            // to go under the bars.
            _scrollView.contentSize = CGSizeMake(image.size.width,
                                                 image.size.height + top.origin.y + top.size.height);
            _imageView.image = image;
            _imageView.frame = CGRectMake(0, top.origin.y + top.size.height, image.size.width, image.size.height);
        }
    }
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    [self setImage];
    
    // Set the bar buttons
    UIBarButtonItem *delete = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                            target:self
                                                                            action:@selector(deletePhoto:)];
    
    self.navigationItem.rightBarButtonItems = @[ self.navigationItem.rightBarButtonItem, delete ];
}

- (void) viewWillAppear:(BOOL)animated
{
    // If the user swipes right the view controller will
    // disappear. But if the user doesn't swipe all the way
    // then the view controller will slide back into place
    //
    // When this happens viewWillAppear: is called again,
    // even though the viewController never fully disappeared
    //
    // This causes a problem with zoomScale and minimumZoomScale
    // being recalculated, throwing off the presentation. We
    // protect against this happending by checking to see if we're
    // already visible.
    if ( !(self.isViewLoaded && self.view.window) ) {
        [super viewWillAppear:animated];
        
        CGFloat topHeight    = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        
        _scrollView.minimumZoomScale = MIN(_scrollView.frame.size.height - topHeight/_scrollView.contentSize.height,
                                           _scrollView.frame.size.width/_scrollView.contentSize.width);
        _scrollView.zoomScale = _scrollView.minimumZoomScale;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
         UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        return NO;
    }
    
    return YES;
}

- (void) setImageToDisplay:(CClanPix *)asset;
{
    self.asset = asset;
    [self setImage];
}

- (void)didReceiveMemoryWarning
{
    self.asset = nil;

    [self setImageView:nil];
    [self setScrollView:nil];
    [super didReceiveMemoryWarning];
}

- (void) deletePhoto:(UIBarButtonItem *)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L_ALERT_DELETE_TITLE
                                                    message:L_ALERT_DELETE_MESSAGE
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok", nil];
    
    [alert show];
}

- (IBAction)share:(id)sender
{
    NSArray * const items = @[ self.imageView.image, @"#ClanPix @ConnachtClan " ];
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];

    // The added text for the tweet will mean text copying is enabled as an activity
    activity.excludedActivityTypes = @[ UIActivityTypeCopyToPasteboard ];
    /*
     @@TODO Find out if saving to a new album is possible
    activity.completionHandler = ^(NSString *activityType, BOOL completed) {
        if ( completed && [activityType isEqualToString:UIActivityTypeSaveToCameraRoll] ) {
            [self.delegate saveElement:self.asset];
        }
    };
    */
    [self presentViewController:activity animated:YES completion:nil];
}

#pragma make - UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];

    if ( alertView.cancelButtonIndex != buttonIndex ) {
        if ( [alertView.title isEqualToString:L_ALERT_DELETE_TITLE] ) {
            [self.delegate elementWasSelectedForDeletion:self.asset];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

@end
