//
//  CClanMatchPreviewViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import "CClanTableViewController.h"
#import "CClanWeatherReport.h"
#import "CClanCalendarEvent.h"
#import "CClanMatchDetails.h"

@interface CClanMatchPreviewViewController : CClanTableViewController

@property (nonatomic, strong) const CClanCalendarEvent *event;

@end
