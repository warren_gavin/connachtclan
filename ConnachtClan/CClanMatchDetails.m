//
//  CClanMatchDetails.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import "CClanMatchDetails.h"

@implementation CClanMatchDetails

- (NSString *) description
{
    if ( self.homeScore ) {
        return [NSString stringWithFormat:@"%@ %@ - %@ %@", self.homeTeam, self.homeScore, self.awayScore, self.awayTeam];
    }

    return [NSString stringWithFormat:@"%@ v %@", self.homeTeam, self.awayTeam];
}

@end
