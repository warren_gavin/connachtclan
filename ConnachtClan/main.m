//
//  main.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CClanAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CClanAppDelegate class]));
    }
}
