//
//  CClanMatchPreviewViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import "CClanMatchPreviewViewController.h"
#import "CClanWeatherReportCache.h"
#import "CClanMatchDetailsCell.h"
#import "CClanMatchPreviousCell.h"
#import "CClanMatchWeatherCell.h"

#import "NHCalendarActivity.h"
#import "EKEventStore+CClanEvent.h"

static NSString * const L_ALERT_ADD_TO_CALENDAR  = @"Adding to Calendar";
static NSString * const L_ALERT_CHOICE_YES       = @"Yes";

@interface CClanMatchPreviewViewController () <UIAlertViewDelegate>

@property (nonatomic, weak) const CClanWeatherReport * const weather;
@property (nonatomic, strong) EKEventStore * const store;

@end

@implementation CClanMatchPreviewViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        self.store = [[EKEventStore alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setupWithWeatherReport:)
                                                     name:CCLAN_WEATHER_LOADED
                                                   object:nil];
    }
    
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([self.event.coordinates[0] floatValue],
                                                                    [self.event.coordinates[1] floatValue]);
    [CClanWeatherReportCache loadWeatherForLocation:self.event.location atCoordinates:coordinates];
}

- (void) setupWithWeatherReport:(NSNotification *)notification;
{
    self.weather = notification.object;
    [self.tableView reloadData];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = 8; /* Details pane, weather header & 6 weather cells */
    
    if ( self.event.matchDetails.previousMeetings.count ) {
        rowCount += 1 + self.event.matchDetails.previousMeetings.count;
    }
    
    return rowCount;
}

- (UITableViewCell *) tableView:(UITableView *)tableView previousMatchesCellAtIndex:(NSInteger) index
{
    if ( 0 == index ) {
        return [tableView dequeueReusableCellWithIdentifier:@"previous banner"];
    }
    
    return [CClanMatchPreviousCell cellForTable:tableView withDetails:self.event.matchDetails.previousMeetings[index - 1]];
}

- (UITableViewCell *) tableView:(UITableView *)tableView matchWeatherCellAtIndex:(NSInteger) index
{
    if ( 0 == index ) {
        return [tableView dequeueReusableCellWithIdentifier:@"forecast banner"];
    }

    return [CClanMatchWeatherCell cellForTable:tableView withWeather:self.weather atIndex:index - 1];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( 0 == indexPath.row ) {
        CClanMatchDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"match details"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterNoStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        cell.homeTeam.image = [UIImage imageNamed:[self.event.matchDetails.homeTeam lowercaseString]];
        cell.awayTeam.image = [UIImage imageNamed:[self.event.matchDetails.awayTeam lowercaseString]];
        cell.location.text  = self.event.location;
        cell.details.text   = [@"Kick Off " stringByAppendingString:[formatter stringFromDate:self.event.date]];
        
        return cell;
    }
    
    NSInteger offset = 1;

    if ( self.event.matchDetails.previousMeetings.count ) {
        if ( indexPath.row - 2 < (NSInteger)self.event.matchDetails.previousMeetings.count ) {
            return [self tableView:tableView previousMatchesCellAtIndex:indexPath.row - 1];
        }

        offset += 1 + self.event.matchDetails.previousMeetings.count;
    }
    
    return [self tableView:tableView matchWeatherCellAtIndex:indexPath.row - offset];
}

#pragma mark - UITableViewDelegate
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

#pragma mark - Activity Action
-(NHCalendarEvent *)createCalendarEvent
{
    NHCalendarEvent *calendarEvent = [[NHCalendarEvent alloc] init];
    
    calendarEvent.title = self.event.headline;
    calendarEvent.location = self.event.location;
    calendarEvent.startDate = self.event.date;
    calendarEvent.endDate = [self.event.date dateByAddingTimeInterval:2 * 60 * 60];
    calendarEvent.allDay = NO;
    
    return calendarEvent;
}

- (IBAction)action:(UIBarButtonItem *)sender
{
    NHCalendarActivity *calendar_activity = [[NHCalendarActivity alloc] init];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:@[ [self createCalendarEvent] ]
                                                                           applicationActivities:@[ calendar_activity ]];

    activity.excludedActivityTypes = @[ UIActivityTypePostToFacebook,
                                        UIActivityTypePostToTwitter,
                                        UIActivityTypePostToWeibo,
                                        UIActivityTypeMessage,
                                        UIActivityTypeMail,
                                        UIActivityTypePrint,
                                        UIActivityTypeCopyToPasteboard,
                                        UIActivityTypeAssignToContact,
                                        UIActivityTypeSaveToCameraRoll,
                                        UIActivityTypeAddToReadingList,
                                        UIActivityTypePostToFlickr,
                                        UIActivityTypePostToVimeo,
                                        UIActivityTypePostToTencentWeibo,
                                        UIActivityTypeAirDrop ];

    [activity setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if ( completed && [activityType isEqualToString:[calendar_activity activityType]] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Event added"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Done", nil];
            
            [alert show];
        }
    }];
    
    [self presentViewController:activity animated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.cancelButtonIndex != buttonIndex ) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if ( [[alertView title] isEqualToString:L_ALERT_ADD_TO_CALENDAR] ) {
            [self.store addClanEvent:self.event
                           withAlert:[[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:L_ALERT_CHOICE_YES]
                          controller:self];
        }
    }
}

@end
