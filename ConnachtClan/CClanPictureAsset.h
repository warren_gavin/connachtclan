//
//  CClanPictureAsset.h
//  ConnachtClan
//
//  Created by Warren Gavin on 05/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Clan Photo information
@interface CClanPictureAsset : NSObject

//! Filepath for the photo stored in the Apps documents directory
@property (strong, nonatomic) NSString *path;

//! The clan photo
@property (strong, nonatomic) UIImage  *image;

//! Default initialiser
- (id) initWithPath:(NSString *)path
              image:(UIImage  *)image;

@end
