//
//  NSData+Hash.m
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import "NSData+Hash.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSData (Hash)

- (NSData *) sha1
{
    unsigned char hash[CC_SHA1_DIGEST_LENGTH];

    if ( CC_SHA1([self bytes], (CC_LONG)[self length], hash) ) {
        return [NSData dataWithBytes:hash length:sizeof hash];
    }

    return nil;
}

@end
