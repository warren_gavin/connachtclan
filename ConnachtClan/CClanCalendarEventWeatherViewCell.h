//
//  CClanEventWeatherViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Individual table cell for a weather forecast
@interface CClanCalendarEventWeatherViewCell : UITableViewCell

//! Graphical representation of the forecast
@property (nonatomic, weak) IBOutlet UIImageView *image;

//! An additional image for the forecast to lay over the 
//! main image, such as an indicator of an extra detail. E.g. Wind
@property (weak, nonatomic) IBOutlet UIImageView *detail;

//! The day of the forecast
@property (nonatomic, weak) IBOutlet UILabel     *day;

//! The forecast text
@property (nonatomic, weak) IBOutlet UILabel     *conditions;

@end
