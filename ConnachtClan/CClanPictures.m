//
//  CClanPictures.m
//  ConnachtClan
//
//  Created by Warren Gavin on 23/10/12.
//
//

#import "CClanPictures.h"
#import "CClanPictureAsset.h"
#import "CClanErrorHandler.h"

#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAssetRepresentation.h>

NSString * const CCLAN_PIX_PHOTO_COUNT_CHANGED = @"CClanPix.NumberOfPhotosChanged";
NSString * const CCLAN_PIX_PHOTO_LOADED        = @"CClanPix.NewPhoto";

static NSString         *L_CLAN_PIX_PREFIX        = @"ClanPix";
static NSString         *L_CLAN_ALBUM             = @"ConnachtClan";
static const NSUInteger  L_DEFAULT_PHOTO_CAPACITY = 12;

static CClanPictures *g_instance;

@interface CClanPictures ()

//! Internal photo album
@property (strong, nonatomic) NSMutableArray *photos;

//! Path name for the clan album
@property (strong, nonatomic) NSString *clan_photo_album;

//! Stored images contain an index value. This keeps track of the value
//! Needed when adding new photos
@property (nonatomic) NSInteger image_index;

@property (nonatomic) BOOL loading;

@end

@implementation CClanPictures

- (id) init
{
    self = [super init];
    
    if ( self ) {
        self.photos = [NSMutableArray arrayWithCapacity:L_DEFAULT_PHOTO_CAPACITY];
        self.clan_photo_album = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:L_CLAN_PIX_PREFIX];
    }
    
    return self;
}

+ (CClanPictures * const) instance
{
    if ( !g_instance ) {
        g_instance = [[CClanPictures alloc] init];
        g_instance.loading = NO;
    }
    
    return g_instance;
}

//! @returns Name of an image to store. Of the form: "ClanPixNNNNNNNN.png"
- (NSString *)newImageFileName
{
    NSString *filename = [L_CLAN_PIX_PREFIX stringByAppendingFormat:@"%08d", ++self.image_index];
    return [[self.clan_photo_album stringByAppendingPathComponent:filename] stringByAppendingPathExtension:@"png"];
}

//! Adds an image to the applications photo album.
//! If there is no existing album one is created.
- (void) addPhotoToAlbum:(UIImage *)photo photoIsFinal:(BOOL)final
{
    dispatch_queue_t photo_adding = dispatch_queue_create("adding photos", NULL );
    
    dispatch_async( photo_adding, ^{
        NSFileManager *filemanager = [NSFileManager defaultManager];
        if ( ![filemanager fileExistsAtPath:self.clan_photo_album] ) {
            [filemanager createDirectoryAtPath:self.clan_photo_album
                   withIntermediateDirectories:YES
                                    attributes:nil
                                         error:nil];
        }
        
        NSString *path = [self newImageFileName];
        
        // Saving the photo data takes some time, use another thread
        dispatch_queue_t photo_saving = dispatch_queue_create("saving photos", NULL );
        dispatch_async( photo_saving, ^{
            [filemanager createFileAtPath:path
                                 contents:UIImagePNGRepresentation(photo)
                               attributes:nil];
        });
        
        CClanPictureAsset *asset = [[CClanPictureAsset alloc] initWithPath:path image:photo];
        [self.photos addObject:asset];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSDictionary *info = @{ @"asset" : asset, @"index" : [NSNumber numberWithInteger:self.photos.count - 1] };
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_PHOTO_LOADED object:self userInfo:info];

            if ( final ) {
                self.loading = NO;
            }
        });
    });
}

- (void) addPhotoToAlbum:(UIImage *)photo
{
    [self addPhotoToAlbum:photo photoIsFinal:YES];
}

//! Export an image to the devices photo album
- (void) addPhotoToCameraRoll:(const UIImage *)photo
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    void (^handleFailure)(NSError *error) = ^(NSError *error) {
        dispatch_async( dispatch_get_main_queue(), ^{
            self.loading = NO;
            //            [self.delegate clanPicturesDidFail:self withError:error];
        });
    };
    
    void (^addToClanAlbum)(NSURL *assetURL, NSError *error) = ^(NSURL *assetURL, NSError *error) {
        if ( error ) {
            handleFailure( error );
        }
        else {
            [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                   usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                       if ( [L_CLAN_ALBUM isEqualToString:[group valueForProperty:ALAssetsGroupPropertyName]] ) {
                                           [library assetForURL:assetURL
                                                    resultBlock:^(ALAsset *asset) {
                                                        [group addAsset:asset];
                                                        dispatch_async( dispatch_get_main_queue(), ^{
                                                            self.loading = NO;
                                                            //                                                            [self.delegate clanPicturesDidFinishLoading:self];
                                                        });
                                                        *stop = YES;
                                                    }
                                                   failureBlock:handleFailure];
                                       }
                                   }
                                 failureBlock:handleFailure];
        }
    };
    
    void (^writeImageToSavedPhotos)(ALAssetsGroup *group) = ^(ALAssetsGroup *group) {
        [library writeImageToSavedPhotosAlbum:photo.CGImage
                                  orientation:(ALAssetOrientation)photo.imageOrientation
                              completionBlock:addToClanAlbum];
    };

    [library addAssetsGroupAlbumWithName:L_CLAN_ALBUM
                             resultBlock:writeImageToSavedPhotos
                            failureBlock:handleFailure];
}

//! Remove a photo from the App's photo album. This does not
//! remove the photo from the devices photo album. Users
//! will have to do that separately using the "Photos" App.
- (void) removePhotoFromAlbum:(CClanPictureAsset *)asset
{
    [self.photos removeObject:asset];
    [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_PHOTO_COUNT_CHANGED object:self];
    
    dispatch_queue_t photo_removing = dispatch_queue_create("removing photos", NULL );
    
    dispatch_async( photo_removing, ^{
        NSFileManager *filemanager = [NSFileManager defaultManager];
        if ( [filemanager fileExistsAtPath:asset.path] ) {
            [filemanager removeItemAtPath:asset.path error:nil];
        }
    });
}

//! Load the photos stored in the App's photo album
- (void) loadFromAppPhotoAlbum:(NSFileManager *)filemanager
{
    dispatch_queue_t photo_loading = dispatch_queue_create("loading photos", NULL );
    
    dispatch_async( photo_loading, ^{
        NSError *error;
        NSArray *photos = [filemanager contentsOfDirectoryAtPath:self.clan_photo_album
                                                           error:&error];
        
        if ( error ) {
            self.loading = NO;
            [CClanErrorHandler displayAlert:@"Could not load photo album."];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSDictionary *count = @{ @"count" : [NSNumber numberWithUnsignedInteger:photos.count + self.photos.count] };
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_PHOTO_COUNT_CHANGED object:self userInfo:count];
        });

        for ( NSInteger i = 0; i < photos.count; ++i ) {
            NSString *path  = [photos objectAtIndex:i];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            NSNumber *index = [formatter numberFromString:[[path stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:L_CLAN_PIX_PREFIX
                                                                                                                          withString:@""]];
            
            self.image_index = [index integerValue];
            
            NSString *imgpath = [self.clan_photo_album stringByAppendingPathComponent:path];
            CClanPictureAsset *asset = [[CClanPictureAsset alloc] initWithPath:imgpath
                                                                         image:[UIImage imageWithContentsOfFile:imgpath]];
            [self.photos addObject:asset];

            dispatch_async( dispatch_get_main_queue(), ^{
                NSDictionary *info = @{ @"asset" : asset, @"index" : [NSNumber numberWithInteger:i] };
                [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_PHOTO_LOADED object:self userInfo:info];
            });
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            self.loading = NO;
        });
    });
}

//! Load the photos stored in the Device's photo album
- (void) loadFromAssetLibrary
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    __block NSInteger num_results = 0;
    
    // Block that converts an asset to an image and stores it in
    // this instance's album
    void (^AddPhotoFromAsset)(ALAsset *result, NSUInteger index, BOOL *stop) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if ( result ) {
            ALAssetRepresentation *rep = result.defaultRepresentation;
            UIImage *photo = [UIImage imageWithCGImage:rep.fullResolutionImage
                                                 scale:rep.scale
                                           orientation:(UIImageOrientation)rep.orientation];
            
            [self addPhotoToAlbum:photo photoIsFinal:(num_results == index + 1)];
        }
    };
    
    // Block that adds photos if it is reading from the ConnachtClan album
    // on the device
    void (^AddPhotosFromClanAlbum)(ALAssetsGroup *group, BOOL *stop) = ^(ALAssetsGroup *group, BOOL *stop) {
        if ( [L_CLAN_ALBUM isEqualToString:[group valueForProperty:ALAssetsGroupPropertyName]] ) {
            num_results = group.numberOfAssets;
            
            NSDictionary *count = @{ @"count" : [NSNumber numberWithUnsignedInteger:num_results + self.photos.count] };
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_PHOTO_COUNT_CHANGED object:self userInfo:count];
            [group enumerateAssetsUsingBlock:AddPhotoFromAsset];
            *stop = YES;
        }
        else if ( nil == group ) {
            dispatch_async( dispatch_get_main_queue(), ^{
                self.loading = NO;
                //                [self.delegate clanPicturesDidFinishLoading:self];
            });
        }
    };
    
    void (^handleFailure)(NSError *error) = ^(NSError *error) {
        dispatch_async( dispatch_get_main_queue(), ^{
            self.loading = NO;
            //            [self.delegate clanPicturesDidFail:self withError:error];
        });
    };

    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                           usingBlock:AddPhotosFromClanAlbum
                         failureBlock:handleFailure];
}

// Initialise the internal representation of the photo album
- (void) loadPhotoAlbum
{
    if ( g_instance.loading ) {
        return;
    }
    
    if ( !self.photos.count ) {
        g_instance.loading = YES;
        NSFileManager *filemanager = [NSFileManager defaultManager];
        
        if ( [filemanager fileExistsAtPath:self.clan_photo_album] ) {
            [self loadFromAppPhotoAlbum:filemanager];
        }
        else {
            // If there are no photos stored locally this might be a re-install or reset.
            // In which case check for pics saved in a previous Clan album that has not been cleaned up.
            [self loadFromAssetLibrary];
        }
    }
}

- (NSUInteger) albumSize
{
    return self.photos.count;
}

- (CClanPictureAsset * const) pictureInAlbumAtIndex:(NSUInteger)index
{
    if ( self.photos.count <= index ) {
        return nil;
    }

    return [self.photos objectAtIndex:index];
}

@end
