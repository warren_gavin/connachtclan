//
//  EKEventEditViewController+CClanEvent.m
//  ConnachtClan
//
//  Created by Warren Gavin on 04/01/13.
//
//

#import "EKEventEditViewController+CClanEvent.h"

@implementation EKEventEditViewController (CClanEvent)

+ (id) editorForEvent:(EKEvent * const)event store:(EKEventStore * const)store delegate:(id<EKEventEditViewDelegate>)delegate
{
    EKEventEditViewController *editcontroller = [[EKEventEditViewController alloc] init];
    
    editcontroller.eventStore = store;
    editcontroller.event = event;
    editcontroller.editViewDelegate = delegate;
    
    return editcontroller;
}

@end
