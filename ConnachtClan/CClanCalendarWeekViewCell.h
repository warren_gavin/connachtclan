//
//  CClanCalendarWeekViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

//! An individual week as shown in a calendar month
@interface CClanCalendarWeekViewCell : UITableViewCell

//! Sets the 7 days for a given month, including days either side of the month boundary if necessary
- (void) setDaysInWeek:(NSInteger)index forMonth:(NSDate *)month inCalendar:(NSCalendar *)calendar;

@end
