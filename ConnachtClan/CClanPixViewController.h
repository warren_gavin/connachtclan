//
//  CClanPixViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/01/13.
//
//

#import <UIKit/UIKit.h>
#import "CClanLoadingView.h"
#import "CClanViewController.h"

@interface CClanPixViewController : CClanViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet CClanLoadingView *activity;


@end
