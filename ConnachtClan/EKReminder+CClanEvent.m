//
//  EKReminder+CClanEvent.m
//  ConnachtClan
//
//  Created by Warren Gavin on 18/11/12.
//
//

#import "EKReminder+CClanEvent.h"
#import "CClanCalendarEvent.h"

@implementation EKReminder (CClanEvent)

- (void) setClanEvent:(const CClanCalendarEvent *const)event calendar:(EKCalendar *const)calendar alert:(BOOL)alert
{
    self.title     = event.headline;
    self.location  = event.location;
    self.calendar  = calendar;
    self.URL       = [NSURL URLWithString:event.url];
    
    NSCalendarUnit unit = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    self.dueDateComponents = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] components:unit
                                                                                                    fromDate:event.date];
    
    if ( alert ) {
        self.alarms = [NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:-2 * 60 * 60]];
    }
}

@end
