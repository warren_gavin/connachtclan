//
//  CClanNewsItemView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/12/13.
//
//

#import "CClanTableView.h"
#import "CClanNewsItemHeadlineViewCell.h"
#import "CClanNewsItemImageViewCell.h"
#import "CClanNewsItemBodyViewCell.h"

@interface CClanNewsItemView : CClanTableView

@end
