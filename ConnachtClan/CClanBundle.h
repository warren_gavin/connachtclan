//
//  CClanBundle.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Generic helper class for bundle releated operations
@interface CClanBundle : NSObject

//! Changes a filename to it's thumbnail equivalent. This is not the 
//! file in the bundle, just the filename. Can be used when downloading
//! thumbnails from the Clan App website
//!
//! foo.bar becomes foo_th.bar
+ (NSString *)convertFilenameToThumbnail:(const NSString * const)filename;

//! Path to a given file in the bundle
+ (NSString *) fileInBundle:(const NSString *)filename;

//! Returns the the path to a thumbnail of a given file in the bundle
+ (NSString *) thumbnailInBundle:(const NSString *)filename;

@end
