//
//  CClanWeatherReport.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanWeatherItem.h"

@protocol CClanWeatherParser

//! Parser for a given data set of weather conditions
//! Any class implementing this parser will interpret 
//! weather data in its own way
- (void) parseWeather:(NSData * const) weather;

@end

@interface CClanWeatherReport : NSObject

@property (nonatomic, copy)   NSString * const location;

//! Current weather conditions for a location
@property (nonatomic, strong) const CClanWeatherItem *current;

//! Array of CClanWeatherItems
@property (nonatomic, strong) const NSArray *forecast;

@end
