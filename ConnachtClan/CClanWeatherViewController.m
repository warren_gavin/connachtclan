//
//  CClanWeatherViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 19/01/13.
//
//

#import "CClanWeatherViewController.h"
#import "CClanCalendarEventWeatherViewCell.h"
#import "CClanWeatherDetailsCell.h"
#import "CClanWeatherReportCache.h"

@interface CClanWeatherViewController ()

@property (nonatomic, weak) const CClanWeatherReport * const weather;

@end

@implementation CClanWeatherViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setupWithWeatherReport:)
                                                     name:CCLAN_WEATHER_LOADED
                                                   object:nil];
    }
    
    return self;
}

- (void) setupWithWeatherReport:(NSNotification *)notification;
{
    [self.loading hideLoadingDisplay];

    self.weather = notification.object;
    [self.table reloadData];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [CClanWeatherReportCache loadWeatherForLocation:self.location atCoordinates:self.coordinates];
}

- (void) didReceiveMemoryWarning
{
    self.table = nil;
    self.loading = nil;
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    // One section is the details cell, the other the forecast
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( 0 == section ) {
        // Only one details cell
        return 1;
    }

    // Number of forecast days
    return self.weather.forecast.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( 0 == indexPath.section ) {
        static NSString * const identifier = @"details";
        CClanWeatherDetailsCell * const cell = [self.table dequeueReusableCellWithIdentifier:identifier];
        return cell.frame.size.height;
    }

    static NSString *identifier = @"forecast";
    CClanCalendarEventWeatherViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    return cell.frame.size.height;
}

- (UITableViewCell *) detailsTableViewCell
{
    static NSString * const identifier = @"details";
    CClanWeatherDetailsCell * const cell = [self.table dequeueReusableCellWithIdentifier:identifier];
    
    cell.location.text       = self.weather.location;
    cell.details.text        = self.weather.current.details;
    cell.temperature.text    = self.weather.current.temperature;
    cell.description.text    = self.weather.current.description;
    cell.condition.image     = self.weather.current.primary_icon;
    cell.sub_condition.image = self.weather.current.secondary_icon;
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( 0 == indexPath.section ) {
        return [self detailsTableViewCell];
    }
    
    static NSString *identifier = @"forecast";
    CClanCalendarEventWeatherViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ( !cell ) {
        cell = [[CClanCalendarEventWeatherViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                        reuseIdentifier:identifier];
    }
    
    const CClanWeatherItem *weather_item = self.weather.forecast[indexPath.row];
    cell.day.text        = weather_item.day;
    cell.conditions.text = [NSString stringWithFormat:@"%@, %@", weather_item.description, weather_item.highlow];
    cell.image.image     = weather_item.primary_icon;
    cell.detail.image    = weather_item.secondary_icon;
    
    return cell;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
