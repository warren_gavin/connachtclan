#import <UIKit/UIKit.h>

@protocol CClanMapSearchActivityDelegate <NSObject>

- (void) displaySearch;

@end

@interface CClanMapSearchActivity : UIActivity

@property (nonatomic, weak) id<CClanMapSearchActivityDelegate>  delegate;

+ (CClanMapSearchActivity *) activityWithDelegate:(id<CClanMapSearchActivityDelegate>) delegate;

@end
