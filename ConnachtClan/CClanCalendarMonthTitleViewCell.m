//
//  CClanCalendarMonthTitleViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarMonthTitleViewCell.h"

@interface CClanCalendarMonthTitleViewCell()

@property (weak, nonatomic) IBOutlet UILabel *month;

@end

@implementation CClanCalendarMonthTitleViewCell

@synthesize month = _month;

- (void) setTitleForDate:(NSDate *)month
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMMM, YYYY"
                                                             options:0
                                                              locale:[NSLocale currentLocale]]];
    self.month.text = [formatter stringFromDate:month];
}

@end
