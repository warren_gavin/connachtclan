//
//  CClanMatchPreviousCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CClanMatchPreviousCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel     *previousResult;
@property (nonatomic, weak) IBOutlet UIImageView *competition;

+(CClanMatchPreviousCell *) cellForTable:(UITableView *)tableView withDetails:(NSDictionary *)previous;

@end
