//
//  CClanPix+Date.h
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import "CClanPix.h"

@interface CClanPix (Date)

- (NSComparisonResult) compare:(CClanPix *)toCompare;

@end
