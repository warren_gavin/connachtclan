//
//  CClanMapLocationCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/07/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanMapLocationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
