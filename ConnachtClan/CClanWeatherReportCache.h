//
//  CClanWeatherReportCache.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "CClanMacros.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_WEATHER_LOADED );
CCLAN_DECLARE_NOTIFICATION( CCLAN_WEATHER_FAILED );

@interface CClanWeatherReportCache : NSObject

+ (const CClanWeatherReportCache *) instance;
+ (void) loadWeatherForLocation:(NSString const *)location atCoordinates:(const CLLocationCoordinate2D)coordinates;

@end
