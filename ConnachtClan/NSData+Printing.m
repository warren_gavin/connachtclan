//
//  NSData+Printing.m
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import "NSData+Printing.h"

@implementation NSData (Printing)

- (NSString *) hexString
{
    const unsigned char *data = [self bytes];
    
    if (!data) {
        return nil;
    }
    
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(self.length * 2)];
    
    for (int i = 0; i < self.length; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02x", data[i]]];
    
    return [NSString stringWithString:hexString];
}

@end
