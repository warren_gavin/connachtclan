//
//  NSArray+Blocks.h
//  ConnachtClan
//
//  Created by Warren Gavin on 04/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id (^ElementModifierOperation)(id element);

@interface NSArray (Blocks)

- (NSArray *) convertWithModifierBlock:(ElementModifierOperation)block;

@end
