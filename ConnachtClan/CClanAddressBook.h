//
//  CClanAddressBook.h
//  ConnachtClan
//
//  Created by Warren Gavin on 24/11/12.
//
//

#import <Foundation/Foundation.h>
#import <AddressBookUI/AddressBookUI.h>

#import "CClanAnnotation.h"

@class CClanAddressBook;

//! delegate class - handles saving annotation details to the address book
@protocol CClanAddressBookDelegate <NSObject>

- (void) addressBookDidFinishSavingRecord:(const CClanAddressBook * const)addressBook;

- (void) addressBookDidFailSavingRecord:(const CClanAddressBook * const)addressBook;

@end

//! Address book wrapper
@interface CClanAddressBook : NSObject

@property (nonatomic, strong) id<CClanAddressBookDelegate, ABPersonViewControllerDelegate> delegate;

//! Add a clan annotation as an address book record
- (void) addAddressBookRecord:(const CClanAnnotation * const) annotation;

//! Indicates if an annotation already exists in the address book
- (BOOL) addressRecordExists:(const CClanAnnotation * const) annotation;

@end
