#import <UIKit/UIKit.h>

@protocol CClanMapSettingsActivityDelegate <NSObject>

- (void) displaySettings;

@end

@interface CClanMapSettingsActivity : UIActivity

@property (nonatomic, weak) id<CClanMapSettingsActivityDelegate>  delegate;

+ (CClanMapSettingsActivity *) activityWithDelegate:(id<CClanMapSettingsActivityDelegate>) delegate;

@end
