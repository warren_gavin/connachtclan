//
//  CClanMacros.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#ifndef ConnachtClan_CClanMacros_h
#define ConnachtClan_CClanMacros_h

#define CCLAN_DECLARE_NOTIFICATION(notificationString) extern NSString * const notificationString

#endif
