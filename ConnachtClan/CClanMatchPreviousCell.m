//
//  CClanMatchPreviousCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import "CClanMatchPreviousCell.h"
#import "CClanBundle.h"

@implementation CClanMatchPreviousCell

+(CClanMatchPreviousCell *) cellForTable:(UITableView *)tableView withDetails:(NSDictionary *)previous
{
    CClanMatchPreviousCell *cell = [tableView dequeueReusableCellWithIdentifier:@"match previous"];
    
    cell.previousResult.text = previous[@"result"];
    cell.competition.image = [UIImage imageWithContentsOfFile:[CClanBundle thumbnailInBundle:previous[@"competition"]]];
    
    return cell;
}

@end
