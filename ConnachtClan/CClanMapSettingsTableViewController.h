//
//  CClanMapSettingsTableViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 02/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanMapSettings.h"
#import "CClanTableViewController.h"

//! Map settings view
@interface CClanMapSettingsTableViewController : CClanTableViewController

//! Table view displaying the settings
@property (nonatomic, weak) IBOutlet UITableView *table;

//! On/Off switch outlet for turning on satellite view
@property (weak, nonatomic) IBOutlet UISwitch *satellitecontroller;

//! On/Off switch outlet for turning on labelled satellite view
@property (weak, nonatomic) IBOutlet UISwitch *hybridcontroller;

//! The delegate that acts on changes to the map settings
@property (nonatomic, weak) id <CClanMapViewSettingsDelegate> delegate;

//! The settings that are displayed in the view
@property (nonatomic, weak) CClanMapSettings *settings;

@end
