//
//  CClanNavigationViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 23/09/13.
//
//

#import "CClanNavigationViewController.h"
#import "CClanConstants.h"

@implementation CClanNavigationViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    self.navigationBar.barTintColor = CCLAN_COLOUR;
    self.navigationBar.tintColor = [UIColor blackColor];
}

@end
