//
//  CClanCalendarEventDetails.m
//  ConnachtClan
//
//  Created by Warren Gavin on 22/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarEventDetailsView.h"
#import "CClanCalendarEvent.h"
#import "CClanErrorHandler.h"
#import "CClanDownloadedHtmlCache.h"
#import "CClanArticle.h"

#import "UIImage+RoundedCorner.h"

static NSString *L_WEBVIEW_BASE_URL = @"http://www.connachtclan.com/";

//! Calendar event details uses XML parsing, set as delegate
@interface CClanCalendarEventDetailsView() <NSXMLParserDelegate, UIWebViewDelegate, CClanDownloadedHtmlCacheDelegate>

@end

@implementation CClanCalendarEventDetailsView

- (void) setupWithDetails: (NSString * const)url;
{
    [CClanDownloadedHtmlCache downloadHtmlAtUrl:url withDelegate:self];
}

#pragma mark - CClanHtmlDownloaderDelegate
- (void) htmlCache:(const CClanDownloadedHtmlCache *)cache didDownloadArticle:(const CClanArticle *)article
{
    self.headline.text = article.headline;
    self.imageview.image = article.image;
    
    self.webview.delegate = self;
    [self.webview loadHTMLString:article.body baseURL:[NSURL URLWithString:L_WEBVIEW_BASE_URL]];
}

#pragma mark - UIWebViewDelegate
- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // If this is the baseURL continue, it is called when loading the HTML string
    if ( [L_WEBVIEW_BASE_URL isEqualToString:[[request URL] absoluteString]] ) {
        return YES;
    }

    // Links on displayed view should launch in Safari
    [[UIApplication sharedApplication] openURL:[request URL]];
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    // Resize the display for the scroll view, the web view (to remove internal scrollbars)
    // and the background image
    [self setContentSize:CGSizeMake(self.contentSize.width,
                                    (self.webview.frame.origin.y - self.frame.origin.y) +            // New scroll height is distance from webview to scrollview
                                    (self.eventBackground.frame.origin.y - self.frame.origin.y) +    // plus padding, which is distance from background to scrollview
                                    self.webview.scrollView.contentSize.height)];                    // plus new web view size

    [self.webview setFrame:CGRectMake(self.webview.frame.origin.x,
                                      self.webview.frame.origin.y,
                                      self.webview.frame.size.width,
                                      self.webview.scrollView.contentSize.height)];

    [self.eventBackground setFrame:CGRectMake(self.eventBackground.frame.origin.x,
                                              self.eventBackground.frame.origin.y,
                                              self.eventBackground.frame.size.width,
                                              self.webview.frame.origin.y - self.eventBackground.frame.origin.y +  // New event background size is distance from webview
                                              self.webview.scrollView.contentSize.height)];                        // plus webview size
     
    // Stop the activity spinner once everything is loaded
    [self.loading hideLoadingDisplay];
}

@end
