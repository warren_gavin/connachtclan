//
//  CClanNewsViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 11/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanNewsViewController.h"
#import "CClanNewsViewCell.h"
#import "CClanNews.h"
#import "CClanNewsItemViewController.h"
#import "CClanRssItem.h"

#import "UIImage+Resize.h"

@interface CClanNewsViewController ()

@property (strong, nonatomic) const CClanNews * const news;
@property (copy,   nonatomic) const NSArray   *items;

@end

@implementation CClanNewsViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(newNewsArticlesAvailable:)
                                                     name:CCLAN_NEWS_NOTIFY_NEW_ITEMS
                                                   object:nil];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.news = [CClanNews instance];
    self.items = [self.news connachtClanNews];
}

- (void) setItems:(const NSArray *)items
{
    if ( _items != items ) {
        _items = items;
    }
    
    if ( !_items ) {
        [self.loading showLoadingDisplay];
    }
    else {
        [self.loading hideLoadingDisplay];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    const CClanRssItem * const item = sender;
    [segue.destinationViewController setUrl:item.link];
}

#pragma mark - News notification
- (void) newNewsArticlesAvailable:(const NSNotificationCenter * const) notification
{
    self.items = [self.news connachtClanNews];
    [self.table reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    const CClanRssItem * const item = self.items[indexPath.row];
    CClanNewsViewCell * const cell = [self.table dequeueReusableCellWithIdentifier:@"News Item"];
    
    cell.description.text = item.description;
    cell.title.text       = item.title;
    cell.imageview.image  = [item.icon thumbnailImage:cell.imageview.frame.size.height
                                    transparentBorder:0
                                         cornerRadius:0
                                 interpolationQuality:kCGInterpolationHigh];


    return cell;
}

#pragma  mark - UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    const CClanRssItem * const item = [self.news connachtClanNews][indexPath.row];
    [self performSegueWithIdentifier:@"News Item" sender:item];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
