//
//  CClanNews.m
//  ConnachtClan
//
//  Created by Warren Gavin on 08/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanNews.h"
#import "CClanNewsClanItems.h"
#import "CClanRssReader.h"
#import "CClanRssItem.h"
#import "CClanDownloadedHtmlCache.h"

NSString * const CCLAN_NEWS_NOTIFY_NEW_ITEMS = @"be.apokrupto.ClanNews.NewItems";

//static NSString *L_CONNACHT_RUGBY_FEED = @"http://www.connachtrugby.ie/component/option,com_ninjarsssyndicator/feed_id,2";
static NSString *L_CONNACHT_CLAN_FEED  = @"http://connachtclan.com/news?format=feed&type=rss";
static const NSTimeInterval L_NEWS_CACHE_EXPIRATION = 60 * 60;

static const CClanNews *g_instance;

@interface CClanNews () <CClanRssReaderDelegate>
@property (nonatomic, strong) NSArray * const clannews;
@property (nonatomic, strong) NSTimer * const timer;
@property (nonatomic)         BOOL            loading;
@property (nonatomic)         NSUInteger      processed;
@end

@implementation CClanNews

+ (const CClanNews * const) instance
{
    if ( !g_instance ) {
        g_instance = [[CClanNews alloc] init];
        g_instance.loading = NO;
        g_instance.timer = [NSTimer scheduledTimerWithTimeInterval:L_NEWS_CACHE_EXPIRATION
                                                            target:g_instance
                                                          selector:@selector(loadNews)
                                                          userInfo:nil
                                                           repeats:YES];
    }

    return g_instance;
}

- (void) loadNews
{
    if ( self.loading ) {
        return;
    }

    self.loading = YES;
    dispatch_queue_t loading_newsfeeds = dispatch_queue_create("loading newsfeeds", NULL );
    
    dispatch_async( loading_newsfeeds, ^{
        //        [CClanRssReader loadFeedFromLocation:L_CONNACHT_RUGBY_FEED withDelegate:self];
        [CClanRssReader loadFeedFromLocation:L_CONNACHT_CLAN_FEED  withDelegate:self];
    });
}

- (const NSArray * const) connachtClanNews
{
    return self.clannews;
}

- (void) clanItemProcessed
{
    @synchronized(self) {
        if ( ++self.processed == self.clannews.count ) {
            dispatch_async( dispatch_get_main_queue(), ^{
                self.loading = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_NEWS_NOTIFY_NEW_ITEMS object:self];
            });
        }
    }
}

- (const NSArray * const) newNewsItemsFromRssFeed:(const NSArray * const)feedItems
{
    if ( !self.clannews ) {
        return feedItems;
    }
    
    const CClanRssItem * const latest = self.clannews[0];
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        CClanRssItem * const item = evaluatedObject;
        return (NSOrderedDescending == [item.date compare:latest.date]);
    }];
    
    return [feedItems filteredArrayUsingPredicate:filter];
}

#pragma mark - CClanRssReaderDelegate
- (void) reader:(CClanRssReader *)reader didFinishLoadingFeed:(NSArray *)items fromURL:(const NSString *const)url
{
    //    if ( [url isEqualToString:L_CONNACHT_RUGBY_FEED] ) {
    //        self.connachtnews = items;
    //    } else 
    if ( [url isEqualToString:L_CONNACHT_CLAN_FEED] ) {
        const NSArray *  newItems = [self newNewsItemsFromRssFeed:items];
        
        if ( newItems.count ) {
            self.processed = self.clannews.count;
            self.clannews = [newItems arrayByAddingObjectsFromArray:self.clannews];

            for ( CClanRssItem *item in newItems ) {
                dispatch_queue_t process_item = dispatch_queue_create("process news item", NULL);
                
                dispatch_async( process_item, ^{
                    CClanNewsClanItems *newsitem = [[CClanNewsClanItems alloc] init];

                    [newsitem parseDescription:item.description];
                    item.description = newsitem.description;
                    item.icon = newsitem.image;

                    dispatch_queue_t preload_articles = dispatch_queue_create("preload articles", NULL);
                    
                    dispatch_async( preload_articles, ^{
                        // Preload the HTML. No delegate is passed as we don't care
                        // about when the page is finished loading
                        [CClanDownloadedHtmlCache downloadHtmlAtUrl:item.link withDelegate:nil];
                    });
                    
                    [self clanItemProcessed];
                });
            }
        }
        else {
            self.loading = NO;
        }
    }
}

@end
