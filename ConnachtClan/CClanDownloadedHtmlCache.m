//
//  CClanDownloadedHtmlCache.m
//  ConnachtClan
//
//  Created by Warren Gavin on 24/09/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanDownloadedHtmlCache.h"
#import "CClanHtmlDownloader.h"
#import "CClanArticle.h"

static CClanDownloadedHtmlCache *g_instance;

@interface CClanDownloadedHtmlCache () <CClanHtmlDownloaderDelegate>

//! Cache of articles. This object is a dictionary that maps URL strings
//! to the downloaded article.
@property (nonatomic, strong) const NSMutableDictionary *cache;

//! Collection of articles that are currently being downloaded. This is
//! a dictionary of URL strings to the delegate that should be called
//! when the loading is finished.
@property (nonatomic, strong) const NSMutableDictionary *loading;

@end

@implementation CClanDownloadedHtmlCache

- (id) init
{
    self = [super init];

    if ( self ) {
        self.cache   = [NSMutableDictionary dictionary];
        self.loading = [NSMutableDictionary dictionary];
    }

    return self;
}

+ (void) downloadHtmlAtUrl:(NSString * const)url withDelegate:(id<CClanDownloadedHtmlCacheDelegate>)delegate
{
    // @@TODO Leave this here to see if it will fix slow network failures
    if ( !url ) {
        return;
    }

    if ( !g_instance ) {
        g_instance = [[CClanDownloadedHtmlCache alloc] init];
    }

    id key = (url ? url : [NSNull null]);

    if ( !g_instance.loading[key] ) {
        const CClanArticle *article = g_instance.cache[key];
        if ( !article ) {
            // Add this URL to the list of URLs currently being downloaded and their delegates
            [g_instance.loading setValue:delegate forKey:key];
            
            // Download the URL data
            const CClanHtmlDownloader *downloader = [[CClanHtmlDownloader alloc] initWithUrl:url];
            downloader.delegate = g_instance;
            [downloader download];
        }
        else {
            [delegate htmlCache:g_instance didDownloadArticle:article];
        }
    }
    else {
        // If the URL is currently loading make sure we set the delegate to be sure
        // we get the callback
        [g_instance.loading setValue:delegate forKey:key];
    }
}

- (void) htmlDownloader:(const CClanHtmlDownloader *)downloader didFinishDownloadingArticle:(const CClanArticle *)article
{
    if ( nil != article ) {
        id key = (downloader.url ? downloader.url : [NSNull null]);

        self.cache[key] = article;

        id <CClanDownloadedHtmlCacheDelegate> delegate = self.loading[key];
        [self.loading removeObjectForKey:key];
        [delegate htmlCache:self didDownloadArticle:article];
    }
}

@end
