//
//  CClanRevealViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 03/01/14.
//
//

#import "SWRevealViewController.h"

//! Extended SWRevealViewController that acts as its own
//! delegate. This is the initial view controller of the
//! application.
@interface CClanRevealViewController : SWRevealViewController <SWRevealViewControllerDelegate>

@end
