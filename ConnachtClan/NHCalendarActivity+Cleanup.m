//
//  NHCalendarActivity+Cleanup.m
//  ConnachtClan
//
//  Created by Warren Gavin on 16/02/13.
//
//

#import "NHCalendarActivity+Cleanup.h"

@implementation NHCalendarActivity (Cleanup)

- (void) removeDuplicateEvent:(EKEvent *)event inEventStore:(EKEventStore *)eventStore
{
    NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:event.startDate
                                                                 endDate:event.endDate
                                                               calendars:nil];
    
    // Get the array of matching entries and drop the last one, it's the
    // event we have just added
    NSMutableArray *matchedEvents = [[eventStore eventsMatchingPredicate:predicate] mutableCopy];
    [matchedEvents removeLastObject];

    if ( matchedEvents.count ) {
        for ( EKEvent * const match in matchedEvents ) {
            if ( [match.title isEqualToString:event.title] ) {
                [eventStore removeEvent:match span:EKSpanThisEvent commit:YES error:nil];
            }
        }
    }
}

@end
