//
//  CClanCalendar.m
//  ConnachtClan
//
//  Created by Warren Gavin on 20/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanCalendar.h"
#import "CClanCalendarEvent.h"
#import "CClanDownloadedHtmlCache.h"

#import "NSArray+Plist.h"
#import "NSArray+Blocks.h"

// Uncomment this when testing a new event list file, add the event.plist to the data dir
//#define L_USE_LOCAL_EVENT_LIST

static NSString * const L_CALENDAR_DATE_MONTH_FORMAT = @"MMMM yyyy";

// Singleton
static const CClanCalendar *g_instance;

// Comparator for dates that are specified in a string format
static NSComparisonResult
l_stringDateSort( NSString *lhs, NSString *rhs, void *context )
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:L_CALENDAR_DATE_MONTH_FORMAT
                                                             options:0
                                                              locale:[NSLocale currentLocale]]];
    
    NSDate *lhs_date = [formatter dateFromString:lhs];
    NSDate *rhs_date = [formatter dateFromString:rhs];
    
    return [lhs_date compare:rhs_date];
}

// Comparator for dictionaries that are ordered based on a date object
// within the dictionary
static NSComparisonResult
l_dictionaryDateSort( NSDictionary *lhs, NSDictionary *rhs, void *context )
{
    NSDate *lhs_date = lhs[@"date"];
    NSDate *rhs_date = rhs[@"date"];
    
    return [lhs_date compare:rhs_date];
}

@interface CClanCalendar()

@property (atomic) BOOL loading;

//! All events in a dictionary, keyed on individual months, unordered
@property (nonatomic, strong) NSDictionary *events;

//! Chronologically ordered list of all events as CClanCalendarEvent objects
@property (nonatomic, strong) NSArray *allEvents;

//! The separate months that contain events, ordered chronologically
@property (nonatomic, strong) NSArray      *months;

@end


@implementation CClanCalendar

+ (const CClanCalendar * const) instance
{
    if ( !g_instance ) {
        g_instance = [[CClanCalendar alloc] init];
        g_instance.loading = NO;
    }

    return g_instance;
}

- (void) preloadEvent:(NSString * const)url
{
    dispatch_queue_t preload_html = dispatch_queue_create("preload html", NULL );
    
    dispatch_async( preload_html, ^{
        // Preload the event details page - no delegate needed
        [CClanDownloadedHtmlCache downloadHtmlAtUrl:url withDelegate:nil];
    });
}

- (void) setEventNotification:(NSDictionary *)event
{
    NSDate *date = event[@"date"];
    
    if ( [date compare:[NSDate date]] == NSOrderedDescending ) {
        NSInteger offset = 3;
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [date dateByAddingTimeInterval:offset * -60];
        notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"%@ starts in %d minutes", nil), event[@"headline"], offset];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

- (void) loadEvents
{
    if ( self.events && !self.loading ) {
        [self.delegate calendarDidFinishLoadingEvents:self];
    }
    else if ( !self.loading ) {
        self.loading = YES;

#if defined( L_USE_LOCAL_EVENT_LIST )
        NSURL * const events_url = [NSURL URLWithString:@"events.plist" relativeToURL:[[NSBundle mainBundle] bundleURL]];
#else
        NSURL * const events_url = [NSURL URLWithString:@"events.plist" relativeToURL:[NSURL URLWithString:CCLAN_APP_URL]];
#endif
        dispatch_queue_t download_event_list = dispatch_queue_create("download events", NULL );
        
        dispatch_async( download_event_list, ^{
            NSMutableDictionary * const eventlist = [NSMutableDictionary dictionary];
            NSArray             * const events    = [NSArray arrayFromPlistAtURL:events_url
                                                                     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                                 timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];
            
            if ( events ) {
                // remove all the old notifications that may have been superceded by a new event list
                [[UIApplication sharedApplication] cancelAllLocalNotifications];

                for ( NSDictionary *event in events ) {
                    [self preloadEvent:event[@"url"]];
                    [self setEventNotification:event];
                    
                    // Create a dictionary of events keyed on a month
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:L_CALENDAR_DATE_MONTH_FORMAT
                                                                             options:0
                                                                              locale:[NSLocale currentLocale]]];
                    
                    NSString       *key = [formatter stringFromDate:event[@"date"]];
                    NSMutableArray *events_by_month = eventlist[key];
                    
                    if ( !events_by_month ) {
                        events_by_month = [NSMutableArray array];
                    }
                    
                    [events_by_month addObject:event];
                    eventlist[key] = events_by_month;
                }
                
                self.events = [NSDictionary dictionaryWithDictionary:eventlist];
                self.months = [[self.events allKeys] sortedArrayUsingFunction:l_stringDateSort context:NULL];
                self.allEvents = [[events sortedArrayUsingFunction:l_dictionaryDateSort context:NULL] convertWithModifierBlock:^id(id element) {
                    return [CClanCalendarEvent calendarEventFromData:element];
                }];
            }
            else {
                dispatch_async( dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"There was a problem downloading the calendar, try again later"
                                                                   delegate:nil
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"Ok", nil];
                    [alert show];
                });
            }
            
            dispatch_async( dispatch_get_main_queue(), ^{
                self.loading = NO;
                [self.delegate calendarDidFinishLoadingEvents:self];
            });
        });
    }
}

- (NSUInteger) numberOfMonthsWithEvents
{
    return self.events.count;
}

- (NSUInteger) numberOfEventsForMonthAtIndex:(NSUInteger)index
{
    NSArray *monthevents = self.events[self.months[index]];
    return monthevents.count;
}

- (NSArray *) eventsForMonth:(NSUInteger)index
{
    // Array of NSDictionary events
    NSArray *monthevents = self.events[self.months[index]];
    
    // The events in a particular month might not be ordered chronologically
    // so we do that here, predicated on the date key in the dictionary
    return [monthevents sortedArrayUsingFunction:l_dictionaryDateSort context:NULL];
}

- (NSString *) titleForMonthAtIndex:(NSUInteger)index
{
    return self.months[index];
}

- (NSUInteger) currentMonthIndex
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:L_CALENDAR_DATE_MONTH_FORMAT
                                                             options:0
                                                              locale:[NSLocale currentLocale]]];

    NSString *currentMonth = [formatter stringFromDate:[NSDate date]];
    
    // If the month isn't in the calendar then return the last month in the calendar
    if ( ![self.months containsObject:currentMonth] ) {
        return self.months.count - 1;
    }
    
    return [self.months indexOfObject:currentMonth];
}

+ (BOOL) isDaytimeForDate:(const NSDate *)date withAstronomy:(const NSDictionary *)astronomy;
{
    // Default to daytime
    if ( !astronomy || !date )
        return YES;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"hh:mm"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *sunrise = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit
                                            fromDate:[format dateFromString:astronomy[@"sunrise"]]];
    
    NSDateComponents *sunset  = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit
                                            fromDate:[format dateFromString:astronomy[@"sunset"]]];
    
    NSDateComponents *time    = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit
                                            fromDate:(NSDate *)date];
    
    return (((60 * sunrise.hour + sunrise.minute) <= (60 * time.hour   + time.minute)) &&
            ((60 * time.hour    + time.minute   ) <  (60 * sunset.hour + sunset.minute)));
}

@end
