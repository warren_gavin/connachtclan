//
//  CClanAnnotationDetailsTableViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 04/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanAnnotationDetailsTableViewController.h"
#import "CClanAnnotationCollection.h"
#import "CClanWebPageController.h"
#import "CClanAnnotation.h"
#import "CClanBundle.h"
#import "CClanAddressBook.h"
#import "CClanMapSettings.h"

#import "UIImage+RoundedCorner.h"
#import <AddressBookUI/AddressBookUI.h>

static NSString * const L_ACTION_ADD              = @"Add to Address Book";
static NSString * const L_ACTION_CANCEL           = @"Cancel";
static NSString * const L_ALERT_TITLE_ADDRESS_ADD = @"Added to Address Book";
static NSString * const L_ALERT_OPTION_OK         = @"Done";

@interface CClanAnnotationDetailsTableViewController () <CClanAddressBookDelegate, UIActionSheetDelegate, UIAlertViewDelegate, ABPersonViewControllerDelegate>

@property (nonatomic, strong) UIActionSheet    * const actions;
@property (nonatomic, strong) CClanAddressBook * addressbook;

@end

@implementation CClanAnnotationDetailsTableViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        self.addressbook = [[CClanAddressBook alloc] init];
        self.addressbook.delegate = self;
    }
    
    return self;
}

- (void) setAttribution
{
    self.attributionText.text         = @"";
    self.infoText.text                = @"";
    self.attributionBackground.hidden = YES;
    self.attributionImage.hidden      = YES;
    self.infoBackground.hidden        = YES;
    self.infoImage.hidden             = YES;
    
    if ( !self.annotation.fixed ) {
        self.attributionBackground.hidden = NO;
        self.attributionImage.hidden = NO;
        return;
    }

    NSDictionary *info = self.annotation.info;
    if ( info ) {
        self.infoBackground.hidden = NO;
        self.infoImage.hidden      = NO;
        self.infoText.text         = info[@"text"];
        
        NSString *attribution = info[@"attribution"];
        if ( attribution ) {
            self.attributionText.text = attribution;
            self.attributionBackground.hidden = NO;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.activity startAnimating];

    dispatch_queue_t annotation_image = dispatch_queue_create("download annotation detail image", NULL );
    
    dispatch_async( annotation_image, ^{
        UIImage *image = [self.annotation image];
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.activity stopAnimating];
            self.imageview.image = image;
        });
    });

    self.title        = [self.annotation name];
    self.name.text    = [self.annotation name];
    self.address.text = [self.annotation address];
    self.phone.text   = [self.annotation phone];
    self.website.text = [self.annotation website];

    [self setAttribution];
    
    if ( [_annotation isKindOfClass:[CClanExtendedAnnotation class]] && _annotation.fixed ) {
        self.permanent.enabled = NO;
    }
    else {
        self.permanent.on = [[CClanMapSettings instance] isPermanent:self.annotation];
    }
    
    // Don't allow action button if the annotation already exists in the address book
    if ( [self.addressbook addressRecordExists:self.annotation] ) {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)viewDidUnload
{
    [self setImageview:nil];
    [self setAddress:nil];
    [self setPhone:nil];
    [self setWebsite:nil];
    [self setActivity:nil];
    [self setActions:nil];
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
        UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        return NO;
    }

    return YES;
}

- (void)displayActions
{
    if ( !self.actions ) {
        // User should be able to add a contact only, no editing
        self.actions = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:L_ACTION_CANCEL
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:L_ACTION_ADD, nil];

        [self.actions showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
}

- (IBAction)share:(id)sender
{
    /*
    if ( [UIActivityViewController class] ) {
        NSArray * const items = @[ self.imageview.image ];

        UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
        activity.excludedActivityTypes = @[ UIActivityTypeSaveToCameraRoll,
                                            UIActivityTypePrint,
                                            UIActivityTypePostToWeibo,
                                            UIActivityTypePostToTwitter,
                                            UIActivityTypePostToFacebook,
                                            UIActivityTypeMessage,
                                            UIActivityTypeMail,
                                            UIActivityTypeCopyToPasteboard ];
        
        [self presentViewController:activity animated:YES completion:nil];
    }
    else {
        // Hand crafted sharing for older version that don't have the
        // UIActivityViewController
        [self displayActions];
    }
     */
    // Do not use the activity view controller for sharing this annotation
    // because there is no way to associate the data with the address book
    // except for adding the image as an attribute of an already existing
    // contact.
    //
    // Maybe this will change, keep an eye on http://uiactivities.com
    [self displayActions];
}

#pragma mark - UIActionSheetDelegate
- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    if ( [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:L_ACTION_ADD] ) {
        [self.addressbook addAddressBookRecord:self.annotation];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    self.actions = nil;
}

#pragma mark - CClanAddressBookDelegate
- (void) addressBookDidFinishSavingRecord:(const CClanAddressBook *const)addressBook
{
    self.navigationItem.rightBarButtonItem.enabled = NO;

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L_ALERT_TITLE_ADDRESS_ADD
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:L_ALERT_OPTION_OK, nil];
    
    [alert show];
}

- (void) addressBookDidFailSavingRecord:(const CClanAddressBook *const)addressBook
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Whoops!"
                                                    message:@"That didn't work, don't know why. Sorry!"
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok", nil];
    
    [alert show];
}

#pragma mark - ABPersonViewControllerDelegate
- (BOOL) personViewController:(ABPersonViewController *)personViewController
shouldPerformDefaultActionForPerson:(ABRecordRef)person
                     property:(ABPropertyID)property
                   identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}

#pragma mark - Table view delegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *url = nil;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if ( [cell.reuseIdentifier isEqualToString:@"phone number"] ) {
        NSMutableString *phoneno = [self.phone.text mutableCopy];
        [phoneno replaceOccurrencesOfString:@" "
                                 withString:@""
                                    options:NSLiteralSearch
                                      range:NSMakeRange(0, phoneno.length)];

        url = [NSURL URLWithString:[@"tel:" stringByAppendingString:phoneno]];
    }
    else if ( [cell.reuseIdentifier isEqualToString:@"website"] ) {
        url = [NSURL URLWithString:[@"http://" stringByAppendingString:self.website.text]];
    }
    else if ( [cell.reuseIdentifier isEqualToString:@"info"] ) {
        url = [NSURL URLWithString:self.annotation.info[@"link"]];
    }

    [[UIApplication sharedApplication] openURL:url];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (IBAction)markPermanent:(UISwitch *)sender
{
    [self.delegate markLocation:self.annotation permanent:sender.on];
}

@end
