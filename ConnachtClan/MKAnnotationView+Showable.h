//
//  MKAnnotationView+Showable.h
//  ConnachtClan
//
//  Created by Warren Gavin on 20/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//
#import <MapKit/MapKit.h>

@interface MKAnnotationView (Showable)

- (void) setStateFromAnnotation:(id<MKAnnotation>) annotation andShowable:(NSSet *)showable;

@end

