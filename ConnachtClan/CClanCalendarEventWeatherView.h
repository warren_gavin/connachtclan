//
//  CClanCalendarEventWeatherView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanLoadingView.h"
#import "CClanWeatherReport.h"

//! Modal view of the weather forecast for an event
@interface CClanCalendarEventWeatherView : UIScrollView

@property (weak, nonatomic) IBOutlet UIImageView             *condition;
@property (weak, nonatomic) IBOutlet UIImageView             *sub_condition;
@property (weak, nonatomic) IBOutlet UILabel                 *temperature;
@property (weak, nonatomic) IBOutlet UILabel                 *description;
@property (weak, nonatomic) IBOutlet UILabel                 *details;
@property (weak, nonatomic) IBOutlet UITableView             *forecast;
@property (weak, nonatomic) IBOutlet CClanLoadingView        *loading;
@property (weak, nonatomic) IBOutlet UILabel                 *location;

//! Parses the weather as a CClanWeatherReport and sets the outlets
- (void) setupWithWeatherReport: (const CClanWeatherReport *) weather;

@end
