//
//  CClanGoogleSearch.m
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import "CClanGoogleSearch.h"
#import "CClanGoogleSearchAPI.h"
#import "CClanMapView.h"

#import "NSArray+Blocks.h"

@implementation CClanGoogleSearch

+(void) performSearchForLocation:(NSString *)location inRegion:(CClanMapViewRegion)region
{
    dispatch_queue_t google_search = dispatch_queue_create("Google Search", NULL );
    
    dispatch_async( google_search, ^{
        CClanGoogleSearchAPI *apiSearch = [[CClanGoogleSearchAPI alloc] initWithLocation:location inRegion:region];
        [apiSearch performSearch];
    });
}

@end
