//
//  CClanNewsItemImageViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/12/13.
//
//

#import "CClanTableView.h"

@interface CClanNewsItemImageViewCell : CClanTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;

@end
