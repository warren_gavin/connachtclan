//
//  CClanMapViewiOS.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/11/13.
//
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanMapView.h"
#import <MapKit/MapKit.h>

@interface CClanMapViewApple : NSObject <MKMapViewDelegate, CClanMapView>

@end
#endif
