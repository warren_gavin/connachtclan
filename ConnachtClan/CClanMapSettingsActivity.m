#import "CClanMapActivity.h"
#import "CClanMapSettingsActivity.h"

@implementation CClanMapSettingsActivity

+ (CClanMapSettingsActivity *) activityWithDelegate:(id<CClanMapSettingsActivityDelegate>) delegate
{
    CClanMapSettingsActivity *activity = [[CClanMapSettingsActivity alloc] init];
    activity.delegate = delegate;

    return activity;
}

- (NSString *) activityType
{
    return @"be.apokrupto.CClanMapSettingsActivity";
}

- (NSString *) activityTitle
{
    return NSLocalizedString(@"Settings", nil);
}

- (UIImage *) activityImage
{
    return [UIImage imageNamed:@"CClanMapSettingsActivity"];
}

-(BOOL) canPerformWithActivityItems:(NSArray *)activityItems
{
    for (id item in activityItems) {
        if ( [item isKindOfClass:[CClanMapActivity class]] ) { 
            return YES;
        }
    }
    
    return NO;
}

- (void) prepareWithActivityItems:(NSArray *)activityItems
{
}

- (void) performActivity
{
    [self.delegate displaySettings];
}

@end
