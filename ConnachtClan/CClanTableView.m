//
//  CClanTableView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 15/09/13.
//
//

#import "CClanTableView.h"
#import "CClanBundle.h"

@implementation CClanTableViewCell

- (CGFloat) height
{
    return self.frame.size.height;
}

@end

@implementation CClanTableView

- (void) setBackgroundImage
{
    UIImage *background = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"background.png"]];
    self.backgroundView = [[UIImageView alloc] initWithImage:background];
    self.backgroundView.contentMode = UIViewContentModeBottom;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setBackgroundImage];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        [self setBackgroundImage];
    }

    return self;
}

@end
