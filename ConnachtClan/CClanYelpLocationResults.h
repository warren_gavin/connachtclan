//
//  CClanYelpLocationResults.h
//  ConnachtClan
//
//  Created by Warren Gavin on 05/10/13.
//
//

#import <Foundation/Foundation.h>

@interface CClanYelpLocationResults : NSObject

@property (nonatomic, strong) const NSString * const location;
@property (nonatomic, strong) const NSArray  * const results;

@end
