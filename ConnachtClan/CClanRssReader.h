//
//  CClanRssReader.h
//  ConnachtClan
//
//  Created by Warren Gavin on 10/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CClanRssReader;

@protocol CClanRssReaderDelegate <NSObject>

- (void) reader:(CClanRssReader *)reader didFinishLoadingFeed:(NSArray *)items fromURL:(const NSString * const)url;

@end

@interface CClanRssReader : NSObject

@property (nonatomic, weak) id<CClanRssReaderDelegate> delegate;

+ (void) loadFeedFromLocation:(NSString *)url withDelegate:(id < CClanRssReaderDelegate >)delegate;

@end
