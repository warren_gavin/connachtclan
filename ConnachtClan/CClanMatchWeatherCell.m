//
//  CClanMatchWeatherCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import "CClanMatchWeatherCell.h"

@implementation CClanMatchWeatherCell

+(CClanMatchWeatherCell *) cellForTable:(UITableView *)tableView withWeather:(const CClanWeatherReport * const)weather atIndex:(NSInteger)index
{
    CClanMatchWeatherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"match weather"];
    
    cell.conditions.text = nil;
    cell.image.image = nil;
    cell.detail.image = nil;
    cell.day.text = nil;
    [cell.activity startAnimating];
    
    if ( weather ) {
        const CClanWeatherItem *weatherItem = weather.current;
        cell.conditions.text = [NSString stringWithFormat:@"%@, %@", weatherItem.description, weatherItem.temperature];
        
        if ( index > 0 ) {
            weatherItem = weather.forecast[index - 1];
            cell.conditions.text = [NSString stringWithFormat:@"%@, %@", weatherItem.description, weatherItem.highlow];
        }
        
        cell.image.image     = weatherItem.primary_icon;
        cell.detail.image    = weatherItem.secondary_icon;
        cell.day.text        = weatherItem.day;
        
        [cell.activity stopAnimating];
    }
    
    return cell;
}

@end
