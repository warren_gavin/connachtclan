//
//  CClanRssReader.m
//  ConnachtClan
//
//  Created by Warren Gavin on 10/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanRssReader.h"
#import "CClanRssItem.h"

#import "NSXMLParser+URL.h"

@interface CClanRssReader()  <NSXMLParserDelegate>

@property (nonatomic, strong) const CClanRssItem * const currentitem;
@property (nonatomic, strong) NSString * const           currenttext;
@property (nonatomic, strong) NSMutableArray * const     items;
@property (nonatomic, weak)   const NSString * const     url;
@property (nonatomic, strong) const NSXMLParser * const  parser;

@end

@implementation CClanRssReader

- (id) initWithUrl:(NSString *)url andDelegate:(id<CClanRssReaderDelegate>)delegate
{
    self = [super init];

    if ( self ) {
        self.url = url;
        self.delegate = delegate;
        self.items = [NSMutableArray array];
        self.parser = [NSXMLParser xmlParserWithContentsOfURL:[NSURL URLWithString:url]
                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                              timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];
        self.parser.delegate = self;
    }

    return self;
}

+ (void) loadFeedFromLocation:(NSString *)url withDelegate:(id<CClanRssReaderDelegate>)delegate
{
    CClanRssReader *reader = [[CClanRssReader alloc] initWithUrl:url andDelegate:delegate];
    
    [reader.parser parse];
}

#pragma mark - NSXMLParserDelegate

- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
     attributes:(NSDictionary *)attributeDict
{
    self.currenttext = @"";

    if ( [elementName isEqualToString:@"item"] ) {
        self.currentitem = [[CClanRssItem alloc] init];
    }
}

- (void) parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    self.currenttext = [self.currenttext stringByAppendingString:[[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding]];
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.currenttext = [self.currenttext stringByAppendingString:string];
}

- (void) parser:(NSXMLParser *)parser
  didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    self.currenttext = [self.currenttext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if ( [elementName isEqualToString:@"item"] ) {
        [self.items addObject:self.currentitem];
        self.currentitem = nil;
    }
    else if ( [elementName isEqualToString:@"title"] ) {
        self.currentitem.title = self.currenttext;
    }
    else if ( [elementName isEqualToString:@"link"] ) {
        self.currentitem.link = self.currenttext;
    }
    else if ( [elementName isEqualToString:@"description"] ) {
        self.currentitem.description = self.currenttext;
    }
    else if ( [elementName isEqualToString:@"pubDate"] ) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
        
        self.currentitem.date = [formatter dateFromString:self.currenttext];
    }
    
    self.currenttext = nil;
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
    NSComparisonResult (^dateCompare)(CClanRssItem *lhs, CClanRssItem *rhs) = ^(CClanRssItem *lhs, CClanRssItem *rhs) {
        return [rhs.date compare:lhs.date];
    };
    
    [self.delegate reader:self didFinishLoadingFeed:[self.items sortedArrayUsingComparator:dateCompare] fromURL:self.url];
}

@end
