//
//  NSXMLParser+URL.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSXMLParser (URL)

+ (NSXMLParser *) xmlParserWithContentsOfURL:(NSURL *)url
                                 cachePolicy:(NSURLRequestCachePolicy)cachePolicy
                             timeoutInterval:(NSTimeInterval)timeoutInterval;

@end
