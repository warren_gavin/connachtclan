//
//  NSData+URL.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSData+URL.h"

@implementation NSData (URL)

+ (NSData *) dataWithContentsOfURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval
{
    NSURLRequest  * const request = [NSURLRequest requestWithURL:url
                                                     cachePolicy:cachePolicy
                                                 timeoutInterval:timeoutInterval];
    
    NSURLResponse  *response;
    NSError        *error;
    NSData * const data = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&response
                                                            error:&error];
    
    if ( error ) {
        return nil;
    }
    
    return data;
}

@end
