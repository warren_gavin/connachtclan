#import "CClanMapActivity.h"
#import "CClanMapSearchActivity.h"

@implementation CClanMapSearchActivity

+ (CClanMapSearchActivity *) activityWithDelegate:(id<CClanMapSearchActivityDelegate>) delegate
{
    CClanMapSearchActivity *activity = [[CClanMapSearchActivity alloc] init];
    activity.delegate = delegate;

    return activity;
}

- (NSString *) activityType
{
    return @"be.apokrupto.CClanMapSearchActivity";
}

- (NSString *) activityTitle
{
    return NSLocalizedString(@"search", nil);
}

- (UIImage *) activityImage
{
    return [UIImage imageNamed:@"CClanMapSearchActivity"];
}

-(BOOL) canPerformWithActivityItems:(NSArray *)activityItems
{
    for (id item in activityItems) {
        if ( [item isKindOfClass:[CClanMapActivity class]] ) { 
            return YES;
        }
    }
    
    return NO;
}

- (void) prepareWithActivityItems:(NSArray *)activityItems
{
}

- (void) performActivity
{
    [self.delegate displaySearch];
}

@end
