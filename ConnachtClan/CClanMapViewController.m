//
//  CCLanMapViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 25/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CCLanMapViewController.h"
#import "CClanMapView.h"
#import "CClanAnnotationCollection.h"
#import "CClanAnnotation.h"
#import "CClanAnnotationDetailsTableViewController.h"
#import "CClanMapActivity.h"
#import "CClanMapSettings.h"
#import "CClanMapSettingsActivity.h"
#import "CClanMapSearchActivity.h"
#import "CClanMapLocationSelector.h"
#import "CClanMapChangeLocationActivity.h"
#import "CClanBundle.h"
#import "NSArray+Blocks.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface CClanMapViewController () <UISearchBarDelegate,
                                      CClanMapDelegate,
                                      CClanMapViewSettingsDelegate,
                                      CClanMapLocationSelectorDelegate,
                                      CClanMapSettingsActivityDelegate,
                                      CClanMapSearchActivityDelegate,
                                      CClanMapChangeLocationActivityDelegate>

@property (strong, nonatomic) CClanMapSettings *settings;
@property (strong, nonatomic) NSArray          *annotations;

@end

@implementation CClanMapViewController

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(haveNewAnnotations:)
                                                     name:CCLAN_ANNOTATION_COLLECTION_NEW_ANNOTATIONS
                                                   object:nil];
    }
    
    return self;
}

- (void) haveNewAnnotations:(NSNotification *)notification
{
    NSString *location = notification.object;
    
    if ( NSOrderedSame == [self.settings.location caseInsensitiveCompare:location] ) {
        self.annotations = [[CClanAnnotationCollection instance] annotationsForLocation:location];
    }
}

- (void) loadAnnotationsAnimated:(BOOL)animated
{
    [self.map setAnimatesDrop:animated];
    
    @synchronized(self.map) {
        if ( !animated ) {
            [self.map removeAllMapAnnotations];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            for ( id<CClanMapAnnotation> annotatation in self.annotations ) {
                if ( [annotatation.annotation shouldDisplay:self.settings.showable] ) {
                    [self.map addMapAnnotation:annotatation];
                }
            }
        });
    }
}

- (void) findAnnotations:(NSString *)substring
{
    [self.map setAnimatesDrop:NO];

    @synchronized(self.map) {
        [self.map removeAllMapAnnotations];
        
        if ( substring.length ) {
            for ( id<CClanMapAnnotation> annotation in self.annotations ) {
                NSRange range = [annotation.annotation.name rangeOfString:substring options:NSCaseInsensitiveSearch];
                
                if ( range.length ) {
                    if ( [annotation.annotation shouldDisplay:self.settings.showable] ) {
                        [self.map addMapAnnotation:annotation];
                    }
                }
            }
        }
        else {
            [self loadAnnotationsAnimated:NO];
        }
    }
}

-(void) setAnnotations:(NSArray *)annotations
{
    dispatch_async( dispatch_get_main_queue(), ^{
        if ( annotations != _annotations ) {
            _annotations = [annotations convertWithModifierBlock:^id(id element) {
                return [CClanMapViewInstance mapAnnotation:element];
            }];
            [self loadAnnotationsAnimated:YES];
        }
    });
}

// Setting the map location means setting its long & lat,
// the displayed span on screen and the name of the location
// on the toolbar
- (void) setMapLocation:(NSString *)location
{
    const CClanAnnotationCollection *annotationCollection = [CClanAnnotationCollection instance];

    self.annotations = [annotationCollection annotationsForLocation:location];
    self.settings.location = location;

    const NSDictionary *mapdetails = [annotationCollection coordinatesForLocation:location];

    // Navigation bar     
    self.navigationItem.title = mapdetails[@"location"];

    CClanMapViewRegion region = CClanMapViewRegionMake([mapdetails[@"latitude"] floatValue],
                                                       [mapdetails[@"longitude"] floatValue],
                                                       [mapdetails[@"latitudeDelta"] floatValue],
                                                       [mapdetails[@"longitudeDelta"] floatValue]);

    [self.map setMapLocationRegion:region animated:YES];
}

- (void) initialiseMap: (NSString *)location
{
    if ( !_map ) {
        _map = [CClanMapViewInstance map];
        [_map.view setFrame:self.view.frame];
        [self.view addSubview:_map.view];
        [self.view sendSubviewToBack:_map.view];
    }
    
    [self setMapLocation:location];
    [self.map setMapDisplayType:self.settings.maptype];
    self.map.delegate = self;
    [self.map showUserLocation:YES];

    dispatch_queue_t init_map = dispatch_queue_create("initialise map", NULL );

    dispatch_async( init_map, ^{
        self.annotations = [NSArray arrayWithArray:[[CClanAnnotationCollection instance] annotationsForLocation:location]];
    });
}

- (void) hideSearchBar
{
    self.search.hidden = YES;
    [self.search resignFirstResponder];
}

- (void) showSearchBar
{
    self.search.hidden = NO;
    [self.search becomeFirstResponder];
}

- (void) clearSearchBar
{
    if ( self.search.text ) {
        [self hideSearchBar];
        self.search.text = nil;
        
        [self loadAnnotationsAnimated:NO];
        [self.search endEditing:YES];
    }
}

-(void)viewDidLoad
{
    [super viewDidLoad];

    self.settings = [CClanMapSettings instance];
    [self initialiseMap:self.settings.location];

    self.search.delegate = self;
}

- (void)viewDidUnload
{
    self.map = nil;
    self.settings = nil;

    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self hideSearchBar];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // When a segue is called remove all search state, this is
    // equivalent to clicking cancel in the search bar
    [self searchBarCancelButtonClicked:self.search];
    
    if ( [segue.identifier isEqualToString:@"map settings"] ) {
        [segue.destinationViewController setDelegate:self];
        [segue.destinationViewController setSettings:self.settings];
    }
    else if ( [segue.identifier isEqualToString:@"annotation details"] ) {
        [segue.destinationViewController setAnnotation:sender];
        [segue.destinationViewController setDelegate:self];
    }
    else if ( [segue.identifier isEqualToString:@"change map"] ) {
        [segue.destinationViewController setDelegate:self];
        [segue.destinationViewController setLocations:[[CClanAnnotationCollection instance] locations]];
    }
}

- (IBAction)displayActions:(UIBarButtonItem *)sender
{
    CClanMapActivity * const activity = [[CClanMapActivity alloc] init];
    
    CClanMapSettingsActivity       * const settings = [CClanMapSettingsActivity activityWithDelegate:self];
    CClanMapSearchActivity         * const search   = [CClanMapSearchActivity activityWithDelegate:self];
    CClanMapChangeLocationActivity * const change   = [CClanMapChangeLocationActivity activityWithDelegate:self];
    
    NSMutableArray * const activities = [NSMutableArray arrayWithObjects:settings, change, nil];
    if ( self.search.hidden ) {
        [activities addObject:search];
    }
    UIActivityViewController * const activityController = [[UIActivityViewController alloc] initWithActivityItems:@[ activity ]
                                                                                            applicationActivities:activities];

    activityController.excludedActivityTypes = @[ UIActivityTypePostToFacebook,
                                                  UIActivityTypePostToTwitter,
                                                  UIActivityTypePostToWeibo,
                                                  UIActivityTypeMessage,
                                                  UIActivityTypeMail,
                                                  UIActivityTypePrint,
                                                  UIActivityTypeCopyToPasteboard,
                                                  UIActivityTypeAssignToContact,
                                                  UIActivityTypeSaveToCameraRoll,
                                                  UIActivityTypeAddToReadingList,
                                                  UIActivityTypePostToFlickr,
                                                  UIActivityTypePostToVimeo,
                                                  UIActivityTypePostToTencentWeibo,
                                                  UIActivityTypeAirDrop ];

    [self presentViewController:activityController animated:YES completion:nil];
}

#pragma mark - CClanMapLocationSelectorDelegate
- (void) mapLocationSelector:(CClanMapLocationSelector *)locator didSelectLocation:(NSString *)location
{
    [self setMapLocation:location];
}

#pragma mark - Popover delegate
- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

#pragma mark - CClan Map View Delegate
- (void) mapView:(id<CClanMapView>)mapView annotationTapped:(id<CClanMapAnnotation>)annotation
{
    [self performSegueWithIdentifier:@"annotation details" sender:annotation.annotation];
}

#pragma mark - Settings Delegate methods
- (void)setMapViewStandard
{
    [self.map setMapDisplayType:CClanMapViewTypeStandard];
    self.settings.maptype = CClanMapViewTypeStandard;
}

- (void)setMapViewSatellite
{
    [self.map setMapDisplayType:CClanMapViewTypeSatelite];
    self.settings.maptype = CClanMapViewTypeSatelite;
}

- (void)setMapViewHybrid
{
    [self.map setMapDisplayType:CClanMapViewTypeHybrid];
    self.settings.maptype = CClanMapViewTypeHybrid;
}

- (void) showBars:(BOOL) selection
{
    self.settings.showbars = selection;
    [self loadAnnotationsAnimated:NO];
}

- (void) showCafes:(BOOL) selection
{
    self.settings.showcafes = selection;
    [self loadAnnotationsAnimated:NO];
}

- (void) showRestaurants:(BOOL) selection
{
    self.settings.showrestaurants = selection;
    [self loadAnnotationsAnimated:NO];
}

- (void) showAccommodation:(BOOL) selection
{
    self.settings.showaccommodation = selection;
    [self loadAnnotationsAnimated:NO];
}

- (void) markLocation:(const CClanAnnotation *const)location permanent:(BOOL)selection
{
    if ( selection ) {
        [self.settings addPermanent:location];
    }
    else {
        [self.settings removePermanent:location];
    }
    
    [self.map removeAllMapAnnotations];
    [self loadAnnotationsAnimated:NO];
}

# pragma mark - Search bar delegate
- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self findAnnotations:searchText];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.search resignFirstResponder];
    [self.view endEditing:YES];
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.search.text = @"";
    [self.search resignFirstResponder];
    [self loadAnnotationsAnimated:NO];
    [self hideSearchBar];
}

#pragma mark - CClanSearchActivityDelegate
- (void) displaySearch
{
    [self showSearchBar];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CClanSettingsActivityDelegate
- (void) displaySettings
{
    [self performSegueWithIdentifier:@"map settings" sender:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CClanChangeLocationActivityDelegate
- (void) displayChangeLocation
{
    [self performSegueWithIdentifier:@"change map" sender:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
