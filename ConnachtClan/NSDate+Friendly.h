//
//  NSDate+Friendly.h
//  ConnachtClan
//
//  Created by Warren Gavin on 09/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Friendly)

- (NSString * const)friendlyDayOfWeek;

@end
