//
//  UIImage+Array.m
//  ConnachtClan
//
//  Created by Warren Gavin on 02/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "UIImage+Array.h"
#import "CClanBundle.h"

@implementation UIImage (Array)

+ (UIImage *) imageFromArray:(NSArray *)array separatedBy:(CGFloat)distance
{
    if ( !array.count ) {
        return nil;
    }
    
    CGFloat width  = array.count * distance;
    CGFloat height = 0.0;

    // Calculate the total width of the image and max height
    for ( const UIImage *image in array ) {
        width += image.size.width;
        height = MAX( height, image.size.height );
    }

    // Create an image context to add the images in to
    UIGraphicsBeginImageContextWithOptions( CGSizeMake( width, height ), NO, [[UIScreen mainScreen] scale] );

    // If we have images, add them one by one
    CGFloat offset = 0.0;
    for ( const UIImage *image in array ) {
        [image drawInRect:CGRectMake(offset, 0.0, image.size.width, image.size.height)];
        offset += image.size.width + distance;
    }

    UIImage *final_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return final_image;
}

@end
