//
//  CClanMapView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/11/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef struct {
	CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    CLLocationDegrees latitudeDelta;
    CLLocationDegrees longitudeDelta;
} CClanMapViewRegion;

#define CClanMapViewRegionMake(lat,long,latd,longd) {lat, long, latd, longd}

typedef enum {
    CClanMapViewTypeStandard,
    CClanMapViewTypeSatelite,
    CClanMapViewTypeHybrid
} CClanMapViewType;

@protocol CClanMapView;
@class    CClanAnnotation;

@protocol CClanMapAnnotation <NSObject>

@property (nonatomic, strong) CClanAnnotation *annotation;

@end

@protocol CClanMapDelegate

- (void) mapView:(id<CClanMapView>)mapView annotationTapped:(id<CClanMapAnnotation>)annotation;

@end

//! An abstract interface of a map view, can be implemented using
//! any map library such as MapKit or Google Maps
@protocol CClanMapView <NSObject>

@property (nonatomic, weak)   UIView                *view;
@property (nonatomic, strong) id <CClanMapDelegate>  delegate;
@property (nonatomic)         BOOL                   animatesDrop;

//! Clear the map view of annotations
- (void) removeAllMapAnnotations;

//! Add an id<CClanMapViewAnnotation> element
- (void) addMapAnnotation:(id<CClanMapAnnotation>)annotation;

//! Center the map at a given location
//! For implementations that set a zoom value they will have to
//! calculate the zoom based on the span value in the region supplied
- (void) setMapLocationRegion:(CClanMapViewRegion)region animated:(BOOL)animated;

//! Set map to standard, hybrid, satelite
- (void) setMapDisplayType:(CClanMapViewType)mapType;

//! Set whether the map should show the current location of the user
- (void) showUserLocation:(BOOL)showLocation;

@end

@interface CClanMapViewInstance : UIView

+ (id<CClanMapView>) map;

+ (id<CClanMapAnnotation>) mapAnnotation:(CClanAnnotation *)annotation;

@end