//
//  CClanCalendarEventCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarEventCell.h"
#import "CClanCalendarEvent.h"
#import "CClanBundle.h"

#import "UIImage+RoundedCorner.h"
#import "UIImage+Array.h"
#import "NSArray+Blocks.h"

static const CGFloat L_BROADCASTER_LOGO_WHITESPACE = 10.0;
static NSDate *lastdate;

@interface CClanCalendarEventCell()

@property (nonatomic, strong) const CClanCalendarEvent *event;

@end

@implementation CClanCalendarEventCell

- (void) setDate
{
    self.day.text = nil;
    self.date.text = nil;
    self.datebg.hidden = YES;

    if ( !lastdate || NSOrderedSame != [self.event.date compare:lastdate] )  {
        const NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"d"
                                                                 options:0
                                                                  locale:[NSLocale currentLocale]]];
        
        self.date.text = [formatter stringFromDate:self.event.date];
        
        [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEEE"
                                                                 options:0
                                                                  locale:[NSLocale currentLocale]]];
        
        self.day.text = [formatter stringFromDate:self.event.date];
        
        lastdate = [self.event.date copy];
        self.datebg.hidden = NO;
    }
}

- (void) setType
{
    self.type.image = nil;
    
    if ( self.event.type ) {
        UIImage *celltypeimage = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:self.event.type]];
        self.type.image = [celltypeimage roundedCornerImage:6 borderSize:1];
    }
}

- (void) setHeadline
{
    self.headline.text = self.event.headline;
}

- (void) setBroadcasting
{
    if ( !self.event.broadcasters.count ) {
        self.broadcasting.image = nil;
        return;
    }
    
    id (^createImagesFromPaths)( id element ) = ^( id element ) {
        return [UIImage imageWithContentsOfFile:[CClanBundle thumbnailInBundle:element]];
    };
    
    self.broadcasting.image = [UIImage imageFromArray:[self.event.broadcasters convertWithModifierBlock:createImagesFromPaths]
                                          separatedBy:L_BROADCASTER_LOGO_WHITESPACE];
}

- (void) setCompetition
{
    self.competition.image = nil;
    
    if ( self.event.competition ) {
        self.competition.image = [UIImage imageWithContentsOfFile:[CClanBundle thumbnailInBundle:self.event.competition]];
    }
}

- (void) setDetail
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];

    self.detail.text = [self.event.location stringByAppendingFormat:@", %@", [formatter stringFromDate:self.event.date]];
}

- (void) setEventData:(NSDictionary *const)data
{
    self.event = [CClanCalendarEvent calendarEventFromData:data];
    
    [self setDate];
    [self setType];
    [self setHeadline];
    [self setBroadcasting];
    [self setCompetition];
    [self setDetail];
}

@end
