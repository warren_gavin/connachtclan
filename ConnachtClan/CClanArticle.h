//
//  CClanArticle.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/09/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CClanArticle : NSObject

@property (nonatomic, strong) NSString * const headline;
@property (nonatomic, strong) UIImage  * const image;
@property (nonatomic, strong) NSString * const body;

@end
