//
//  CClanNewsViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 11/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanViewController.h"
#import "CClanLoadingView.h"

@interface CClanNewsViewController : CClanViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView      *table;
@property (weak, nonatomic) IBOutlet CClanLoadingView *loading;

@end
