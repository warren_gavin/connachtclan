//
//  CClanButton.m
//  ConnachtClan
//
//  Created by Warren Gavin on 10/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanPictureAssetButton.h"

@implementation CClanPictureAssetButton

@synthesize asset = _asset;

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if ( self ) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }

    return self;
}

@end
