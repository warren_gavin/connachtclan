//
//  CClanSidebarViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/01/14.
//
//

#import "CClanSidebarViewController.h"
#import "CClanConstants.h"
#import "CClanTwitter.h"

#import "SWRevealViewController.h"
#import "UIImage+Mask.h"

@interface CClanSidebarViewController ()

@property (nonatomic, strong) NSDictionary *sectionImages;
@property (nonatomic)         BOOL          twitterEnabled;

@end

@implementation CClanSidebarViewController

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        _sectionImages = @{
            @"News"     : @"newspaper",
            @"Calendar" : @"calendar",
            @"Maps"     : @"map",
            @"ClanPix"  : @"photo",
            @"Twitter"  : @"twitter"
        };
        

        // Observe changes in twitter access and load the timeline
        const CClanTwitter *twitter = [CClanTwitter instance];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(twitterDisabled:)
                                                     name:CCLAN_TWITTER_DISABLED
                                                   object:twitter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(twitterEnabled:)
                                                     name:CCLAN_TWITTER_ENABLED
                                                   object:twitter];
        
        [twitter pollTwitterStatus];
    }
    
    return self;
}

#pragma mark - Twitter notifications
- (void) twitterDisabled:(NSNotification *)notification
{
    _twitterEnabled = NO;
}

- (void) twitterEnabled:(NSNotification *)notification
{
    _twitterEnabled = YES;
}

//! The sidebar tableview's background is one of two embossed
//! backgrounds, either the Clan logo with the sponsor underneath
//! or Con the warrior which isn't displayed very often
- (NSString *) backgroundImageName
{
    NSString *imageName = @"sidebar_background";
    
    // Ratio is 9 Clan logos to each Con
    if ( 0 == arc4random() % 10 ) {
        imageName = [imageName stringByAppendingString:@"-con"];
    }

    return imageName;
}

//! Set the tableView's background
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIImage *background = [UIImage imageNamed:[self backgroundImageName]];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:background];
    self.tableView.backgroundView.contentMode = UIViewContentModeBottomLeft;
}

//! The segue is the shifting of the front view controller to full screen
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *revealSegue = (SWRevealViewControllerSegue*) segue;
        
        revealSegue.performBlock = ^(SWRevealViewControllerSegue *revealViewControllerSegue,
                                     UIViewController            *sourceViewController,
                                     UIViewController            *destinationViewController) {
            // Replace the reveal's front view controller with the destination view
            // controller of the segue, and reset the view controller's position to slide
            // it back to full screen
            self.revealViewController.frontViewController = destinationViewController;
            [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        };
    }
}

//! Sets a cell's image, depending on the text of the cell.
//!
//! A cell's image is a normal filled image, masked to the clan colour
//! A selected cell's image is an unfilled version masked to the clan colour
- (void) imageForTableViewCell:(UITableViewCell *)cell selected:(BOOL)selected
{
    NSString *imageName = _sectionImages[cell.textLabel.text];
    if ( !selected ) {
        imageName = [imageName stringByAppendingString:@"-fill"];
    }
    
    cell.imageView.image = [[UIImage imageNamed:imageName] maskedImageForColour:CCLAN_COLOUR];
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_twitterEnabled ? _sectionImages.count : _sectionImages.count - 1);
}

#pragma mark - UITableViewDelegate
// Set the image for the cell, unselected
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self imageForTableViewCell:cell selected:NO];
}

// Set the image for the cell, with selected set
- (void) tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self imageForTableViewCell:[tableView cellForRowAtIndexPath:indexPath] selected:YES];
}

// Set the image for the cell, unselected
- (void) tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self imageForTableViewCell:[tableView cellForRowAtIndexPath:indexPath] selected:NO];
}



@end
