//
//  CClanMapViewAnnotationCollection.h
//  ConnachtClan
//
//  Created by Warren Gavin on 29/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanMacros.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_ANNOTATION_COLLECTION_NEW_ANNOTATIONS );

@class CClanAnnotation;

//! Collection of annotations to display on a map view
@interface CClanAnnotationCollection : NSObject

//! The colleciton is a singleton class
+ (const CClanAnnotationCollection *) instance;

//! Map coordinates for a given location
- (const NSDictionary *) coordinatesForLocation:(NSString *)location;

//! All known locations
- (NSArray *) locations;

- (NSArray *) annotationsForLocation:(NSString *)location;

@end

