//
//  CClanWeatherWWO.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanWeatherReport.h"

@interface CClanWeatherWWO : CClanWeatherReport <CClanWeatherParser>

@end
