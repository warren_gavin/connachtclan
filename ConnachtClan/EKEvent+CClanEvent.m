//
//  EKEvent+CClanEvent.m
//  ConnachtClan
//
//  Created by Warren Gavin on 18/11/12.
//
//

#import "EKEvent+CClanEvent.h"
#import "CClanCalendarEvent.h"

@implementation EKEvent (CClanEvent)

- (void) setClanEvent:(const CClanCalendarEvent * const)event calendar:(EKCalendar * const)calendar alert:(BOOL)alert
{
    self.title     = event.headline;
    self.location  = event.location;
    self.calendar  = calendar;
    self.URL       = [NSURL URLWithString:event.url];
    self.startDate = event.date;
    self.endDate   = [event.date dateByAddingTimeInterval:2 * 60 * 60];
    
    if ( alert ) {
        self.alarms = @[[EKAlarm alarmWithRelativeOffset:0], [EKAlarm alarmWithRelativeOffset:-2 * 60 * 60]];
    }
    else {
        // Can't just set alarms to nil, defaults will be used.
        // Remove all alarms that may already be set
        for ( EKAlarm *alarm in self.alarms ) {
            [self removeAlarm:alarm];
        }
    }
}
@end
