//
//  CClanCalendarMonthTitleViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Header cell for each month displayed in a calendar view
@interface CClanCalendarMonthTitleViewCell : UITableViewCell

//! Formats the title of the month
- (void) setTitleForDate:(NSDate *)month;

@end
