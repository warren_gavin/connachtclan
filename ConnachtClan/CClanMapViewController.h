//
//  CCLanMapViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 25/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanMapView.h"
#import "CClanViewController.h"

//! 
@interface CClanMapViewController : CClanViewController

@property (strong, nonatomic) id<CClanMapView>      map;
@property (strong, nonatomic) IBOutlet UISearchBar *search;

@end
