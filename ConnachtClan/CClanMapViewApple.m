//
//  CClanMapViewiOS.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/11/13.
//
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanMapViewApple.h"
#import "CClanMapAnnotationApple.h"
#import "CClanAnnotationView.h"
#import "CClanAnnotation.h"
#import "CClanMapSettings.h"
#import "MKAnnotationView+Showable.h"

@interface CClanMapViewApple ()

@property (nonatomic, strong) CClanMapSettings *settings;
@property (nonatomic, strong) MKMapView        *mapView;

@end

@implementation CClanMapViewApple

@synthesize view           = _view;
@synthesize delegate       = _delegate;
@synthesize animatesDrop   = _animatesDrop;

- (instancetype) init
{
    self = [super init];
    
    if ( self ) {
        _mapView = [[MKMapView alloc] initWithFrame:CGRectZero];
        _view = _mapView;
        _mapView.delegate = self;
    }
    
    return self;
}

- (CClanMapSettings *)settings
{
    if ( !_settings ) {
        _settings = [CClanMapSettings instance];
    }
    
    return _settings;
}

- (void) removeAllMapAnnotations
{
    [_mapView removeAnnotations:_mapView.annotations];
}

- (void) addMapAnnotations:(NSArray *)annotations
{
    for ( id<CClanMapAnnotation> annotation in annotations ) {
        [self addMapAnnotation:annotation];
    }
}

- (void) addMapAnnotation:(id<CClanMapAnnotation>)annotation
{
    if ( ![_mapView.annotations containsObject:annotation] ) {
        [_mapView addAnnotation:(CClanMapAnnotationApple *)annotation];
    }
}

- (void) setMapLocationRegion:(CClanMapViewRegion)region animated:(BOOL)animated
{
    CLLocationCoordinate2D centre   = CLLocationCoordinate2DMake(region.latitude, region.longitude);
    MKCoordinateSpan       span     = MKCoordinateSpanMake(region.latitudeDelta, region.longitudeDelta);
    MKCoordinateRegion     mkregion = MKCoordinateRegionMake(centre, span);

    [_mapView setRegion:mkregion animated:animated];
}

- (void) setMapDisplayType:(CClanMapViewType)mapType
{
    switch ( mapType ) {
        case CClanMapViewTypeSatelite:
            _mapView.mapType = MKMapTypeSatelite;
            break;
        case CClanMapViewTypeHybrid:
            _mapView.mapType = MKMapTypeHybrid;
            break;
        case CClanMapViewTypeStandard:
        default:
            _mapView.mapType = MKMapTypeStandard;
            break;
    }
}

- (void) showUserLocation:(BOOL)showLocation
{
    _mapView.showsUserLocation = showLocation;
}

#pragma mark - Map View Delegate methods
#if 0
- (void) mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    NSLog(@"Region: (%f, %f)  [%f    %f]", mapView.region.center.latitude,
          mapView.region.center.longitude,
          mapView.region.span.latitudeDelta,
          mapView.region.span.longitudeDelta);
}
#endif

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ( annotation == mapView.userLocation ) {
        return nil;
    }
    
    CClanMapAnnotationApple *iOSAnnotation = (CClanMapAnnotationApple *)annotation;
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:[[iOSAnnotation.annotation class] identifier]];
    
    if ( !view ) {
        view = [CClanAnnotationViewFactory viewForAnnotation:iOSAnnotation];
    }
    
    [view setStateFromAnnotation:annotation andShowable:self.settings.showable];
    view.annotation = annotation;
    if ( [view isKindOfClass:[MKPinAnnotationView class]] ) {
        MKPinAnnotationView *pin = (MKPinAnnotationView *)view;
        pin.animatesDrop = self.animatesDrop;
    }
    
    return view;
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [view setStateFromAnnotation:view.annotation andShowable:self.settings.showable];
}

- (void) mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    [mapView deselectAnnotation:view.annotation animated:YES];
    [self.delegate mapView:self annotationTapped:(CClanMapAnnotationApple *)view.annotation];
}

@end

#endif
