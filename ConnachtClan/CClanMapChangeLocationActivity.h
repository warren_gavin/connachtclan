#import <UIKit/UIKit.h>

@protocol CClanMapChangeLocationActivityDelegate <NSObject>

- (void) displayChangeLocation;

@end

@interface CClanMapChangeLocationActivity : UIActivity

@property (nonatomic, weak) id<CClanMapChangeLocationActivityDelegate>  delegate;

+ (CClanMapChangeLocationActivity *) activityWithDelegate:(id<CClanMapChangeLocationActivityDelegate>) delegate;

@end
