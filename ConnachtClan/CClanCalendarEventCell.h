//
//  CClanCalendarEventCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  CClanCalendarEvent;

@interface CClanCalendarEventCell : UITableViewCell

@property (nonatomic, weak) IBOutlet  UILabel     *day;
@property (nonatomic, weak) IBOutlet  UILabel     *date;
@property (nonatomic, weak) IBOutlet  UIImageView *datebg;
@property (nonatomic, weak) IBOutlet  UIImageView *type;
@property (nonatomic, weak) IBOutlet  UILabel     *headline;
@property (nonatomic, weak) IBOutlet  UIImageView *broadcasting;
@property (nonatomic, weak) IBOutlet  UIImageView *competition;
@property (nonatomic, weak) IBOutlet  UILabel     *detail;

@property (nonatomic, strong, readonly) const CClanCalendarEvent *event;

- (void) setEventData:(NSDictionary * const)data;

@end
