//
//  CClanLocation.h
//  ConnachtClan
//
//  Created by Warren Gavin on 31/10/13.
//
//

#import <Foundation/Foundation.h>
#import "CClanMapView.h"

@interface CClanLocation : NSObject

+ (BOOL) coordinate:(CLLocationCoordinate2D)coordinates isInRegion:(CClanMapViewRegion)region;

@end
