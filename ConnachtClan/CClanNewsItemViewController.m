//
//  CClanNewsItemViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 14/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanNewsItemViewController.h"
#import "CClanCalendarEventDetailsView.h"
#import "CClanErrorHandler.h"
#import "CClanDownloadedHtmlCache.h"
#import "CClanArticle.h"
#import "CClanNewsItemView.h"

#import "UIImage+Resize.h"

static NSString *L_WEBVIEW_BASE_URL = @"http://www.connachtclan.com/";

@interface CClanNewsItemViewController () <CClanDownloadedHtmlCacheDelegate, UIWebViewDelegate>

@property (nonatomic, strong) const CClanArticle                  *article;
@property (nonatomic, strong) const CClanNewsItemHeadlineViewCell *headlineCell;
@property (nonatomic, strong) const CClanNewsItemImageViewCell    *imageCell;
@property (nonatomic, strong) const CClanNewsItemBodyViewCell     *bodyCell;
@property (nonatomic) BOOL loaded;

@property (nonatomic, strong) UIImage *articleImage;

@end

@implementation CClanNewsItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_loadingView showLoadingDisplay];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [CClanDownloadedHtmlCache downloadHtmlAtUrl:_url withDelegate:self];
}

- (IBAction)share:(UIBarButtonItem *)sender
{
    NSArray * const items = @[ [NSURL URLWithString:self.url] ];
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    activity.excludedActivityTypes = @[ UIActivityTypeSaveToCameraRoll ];
    
    [self presentViewController:activity animated:YES completion:nil];
}

- (void) setArticle:(const CClanArticle *)article
{
    @synchronized(self) {
        if ( _article != article ) {
            _article = article;
            _loaded  = NO;
            [_tableView reloadData];
        }
    }
}

- (const CClanNewsItemHeadlineViewCell *)headlineCell
{
    if ( !_headlineCell && _article ) {
        _headlineCell = [_tableView dequeueReusableCellWithIdentifier:@"ArticleHeadline"];
        _headlineCell.headline.text = _article.headline;
    }
    
    return _headlineCell;
}

- (const CClanNewsItemImageViewCell *)imageCell
{
    if ( _article ) {
        _imageCell = [_tableView dequeueReusableCellWithIdentifier:@"ArticleImage"];
        //        _imageCell.imageView.image = _article.image;
        _imageCell.cellImageView.image = [_article.image resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                                          bounds:_imageCell.imageView.frame.size
                                                            interpolationQuality:kCGInterpolationHigh];
    }

    return _imageCell;
}

- (const CClanNewsItemBodyViewCell *)bodyCell
{
    @synchronized(self) {
        if ( !_bodyCell ) {
            _bodyCell = [_tableView dequeueReusableCellWithIdentifier:@"ArticleBody"];
        }

        if ( _article.body && !_bodyCell.webView.loading && !_loaded ) {
            _bodyCell.webView.delegate = self;
            [_bodyCell.webView loadHTMLString:_article.body baseURL:[NSURL URLWithString:L_WEBVIEW_BASE_URL]];
        }
    }

    return _bodyCell;
}

- (CClanTableViewCell *) cellForRow:(NSUInteger)row
{
    SEL cellSelector[] = { @selector(headlineCell), @selector(imageCell), @selector(bodyCell) };
    
    CClanTableViewCell *cell;
    SuppressPerformSelectorLeakWarning( cell = [self performSelector:cellSelector[row]] );
    
    return cell;
}

#pragma mark - CClanHtmlDownloaderDelegate
- (void) htmlCache:(const CClanDownloadedHtmlCache *)cache didDownloadArticle:(const CClanArticle *)article
{
    [_loadingView hideLoadingDisplay];
    self.article = article;
}

#pragma mark - UIWebViewDelegate
- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // If this is the baseURL continue, it is called when loading the HTML string
    if ( [L_WEBVIEW_BASE_URL isEqualToString:[[request URL] absoluteString]] ) {
        return YES;
    }
    
    // Links on displayed view should launch in Safari
    [[UIApplication sharedApplication] openURL:[request URL]];
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    @synchronized(self) {
        _loaded = YES;
        webView.scrollView.scrollEnabled = NO;
    }

    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @synchronized(self) {
        // Don't do any table stuff until we have an article
        return (_article ? 3 : 0);
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self cellForRow:indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CClanTableViewCell *cell = [self cellForRow:indexPath.row];
    return [cell height];
}

@end
