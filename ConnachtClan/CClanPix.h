//
//  CClanPix.h
//  ConnachtClan
//
//  Created by Warren Gavin on 19/08/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CClanPix : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSData * imageData;
@property (nonatomic, retain) NSData * thumbnail;

@end
