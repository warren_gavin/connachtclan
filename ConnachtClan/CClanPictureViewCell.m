//
//  CClanPictureViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 14/07/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanPictureViewCell.h"
#import "CClanPictureAssetButton.h"

@implementation CClanPictureViewCell

@synthesize button = _button;

- (id) initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];

    if ( self ) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        self.button = [[CClanPictureAssetButton alloc] initWithFrame:frame];
        self.button.opaque = NO;
        [self.contentView addSubview:self.button];
    }

    return self;
}

@end
