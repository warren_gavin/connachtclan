//
//  CClanYelpResult.h
//  ConnachtClan
//
//  Created by Warren Gavin on 05/10/13.
//
//

#import <Foundation/Foundation.h>
#import "CClanMapSearchResult.h"

@interface CClanYelpResult : NSObject <CClanMapSearchResult>

@property (nonatomic, strong) NSString * const name;
@property (nonatomic, strong) NSArray  * const categories;
@property (nonatomic, strong) NSString * const phone;
@property (nonatomic, strong) NSString * const address;
@property (nonatomic, strong) NSString * const city;
@property (nonatomic, strong) NSString * const country;
@property (nonatomic, strong) NSString * const imageUrl;
@property (nonatomic, strong) NSString * const ratingImageUrl;
@property (nonatomic, strong) NSString * const moreUrl;

- (void) addCategories:(NSArray *)categories;

@end
