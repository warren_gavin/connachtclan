//
//  CClanNewsItemViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 14/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanLoadingView.h"
#import "CClanNewsItemView.h"

@class CClanCalendarEventDetailsView;

@interface CClanNewsItemViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString * const url;
@property (weak, nonatomic) IBOutlet CClanLoadingView *loadingView;
@property (weak, nonatomic) IBOutlet CClanNewsItemView *tableView;

@end
