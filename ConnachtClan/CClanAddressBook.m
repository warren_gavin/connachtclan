//
//  CClanAddressBook.m
//  ConnachtClan
//
//  Created by Warren Gavin on 24/11/12.
//
//

#import "CClanAddressBook.h"

#import <AddressBook/ABAddressBook.h>

static void
l_SetName(ABRecordRef                   io_entry,
          const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABRecordSetValue(io_entry, kABPersonFirstNameProperty, (__bridge CFStringRef)in_annotation.name, &error);
}

static void
l_SetPhoneNumber(ABRecordRef                   io_entry,
                 const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef phones = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(phones, (__bridge CFStringRef)in_annotation.phone, kABPersonPhoneMainLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonPhoneProperty, phones, &error);
    
    CFRelease(phones);
}

static void
l_SetAddress(ABRecordRef                   io_entry,
             const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef address = ABMultiValueCreateMutable(kABDictionaryPropertyType);
    
    CFStringRef keys[] = { kABPersonAddressStreetKey, kABPersonAddressCityKey, kABPersonAddressCountryKey };
    CFStringRef values[] = { (__bridge CFStringRef)in_annotation.address,
        (__bridge CFStringRef)in_annotation.city,
        (__bridge CFStringRef)in_annotation.country };
    
    CFDictionaryRef aDict = CFDictionaryCreate(kCFAllocatorDefault,
                                               (void *)keys,
                                               (void *)values,
                                               sizeof keys / sizeof keys[0],
                                               &kCFCopyStringDictionaryKeyCallBacks,
                                               &kCFTypeDictionaryValueCallBacks);
    
    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(address, aDict, kABHomeLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonAddressProperty, address, &error);
    
    CFRelease(aDict);
    CFRelease(address);
}

static void
l_SetPhoto(ABRecordRef                   io_entry,
           const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABPersonSetImageData(io_entry, (__bridge CFDataRef)UIImagePNGRepresentation(in_annotation.image), &error);
}

static void
l_SetHomepage(ABRecordRef                   io_entry,
              const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef urls = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(urls, (__bridge CFStringRef)in_annotation.website, kABPersonHomePageLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonURLProperty, urls, &error);
    
    CFRelease(urls);
}

@implementation CClanAddressBook

- (ABRecordRef) recordMatchingAnnotation:(const CClanAnnotation * const) annotation
{
    id               match;
    CFErrorRef       error = NULL;
    ABAddressBookRef address = ABAddressBookCreateWithOptions(NULL, &error);
    
    if ( address ) {
        NSArray *records = (__bridge  NSArray *)ABAddressBookCopyPeopleWithName(address, (__bridge CFStringRef)annotation.name);
        match = [records lastObject];
        
        CFRelease( address );
    }
    
    if ( error ) {
        CFRelease(error);
    }

    return (__bridge ABRecordRef)match;
}

- (ABRecordRef) convertRecord:(const CClanAnnotation * const) annotation
{
    ABRecordRef entry = ABPersonCreate();
    
    if ( entry ) {
        l_SetName       ( entry, annotation );
        l_SetPhoneNumber( entry, annotation );
        l_SetAddress    ( entry, annotation );
        l_SetPhoto      ( entry, annotation );
        l_SetHomepage   ( entry, annotation );
    }
    
    return entry;
}

- (void) addAddressBookRecord:(const CClanAnnotation * const)annotation
{
    dispatch_queue_t add_to_address_book = dispatch_queue_create("Add to Address Book", NULL );
    
    dispatch_async( add_to_address_book, ^{
        bool       result = false;
        CFErrorRef error  = NULL;
        
        ABAddressBookRef address = ABAddressBookCreateWithOptions(NULL, &error);
        ABRecordRef entry = [self convertRecord:annotation];
        
        if ( entry && address ) {
            result = ABAddressBookAddRecord(address, entry, &error);

            if ( result ) {
                result = ABAddressBookSave(address, &error);
            }
        }

        dispatch_async( dispatch_get_main_queue(), ^{
            if ( result ) {
                [self.delegate addressBookDidFinishSavingRecord:self];
            }
            else {
                [self.delegate addressBookDidFailSavingRecord:self];
            }
        });
        
        if ( address ) {
            CFRelease(address);
        }
        if ( entry ) {
            CFRelease(entry);
        }
        if ( error ) {
            CFRelease(error);
        }
    });
}

- (BOOL) addressRecordExists:(const CClanAnnotation * const) annotation
{
    ABRecordRef entry  = [self recordMatchingAnnotation:annotation];
    BOOL        result = (NULL != entry);
    
    if ( entry ) {
        CFRelease(entry);
    }

    return result;
}

@end
