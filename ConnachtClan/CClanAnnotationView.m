//
//  CClanAnnotationView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 31/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanAnnotationView.h"
#import "CClanAnnotation.h"
#import "CClanMapAnnotationApple.h"
#import "CClanMapSettings.h"

@interface CClanAnnotationView() <CClanAnnotationDelegate>

- (id) initWithAnnotation:(id<MKAnnotation>)annotation;

@end

@interface CClanExtendedAnnotationView() <CClanAnnotationDelegate>

- (id) initWithAnnotation:(id<MKAnnotation>)annotation;

@end

@implementation CClanAnnotationViewFactory

+ (CClanAnnotationView *) viewWithAnnotation:(CClanMapAnnotationApple *)annotation
{
    return [[CClanAnnotationView alloc] initWithAnnotation:annotation];
}

+ (CClanExtendedAnnotationView *) extendedViewWithAnnotation:(CClanMapAnnotationApple *)annotation
{
    return [[CClanExtendedAnnotationView alloc] initWithAnnotation:annotation];
}

+ (MKAnnotationView  *) viewForAnnotation:(CClanMapAnnotationApple *)annotation
{
    if ( [annotation.annotation isKindOfClass:[CClanExtendedAnnotation class]] )
        return [CClanAnnotationViewFactory extendedViewWithAnnotation:annotation];

    return [CClanAnnotationViewFactory viewWithAnnotation:annotation];
}

@end

@implementation CClanAnnotationView

- (void) setStateFromAnnotation:(id<MKAnnotation>) annotation andShowable:(NSSet *)showable
{
    CClanMapAnnotationApple *iOSAnnotation   = (CClanMapAnnotationApple *)annotation;
    NSArray               *annotationTypes = [iOSAnnotation.annotation types];
    
    if ( [annotationTypes containsObject:@"Bar"] && [showable containsObject:@"Bar"] ) {
        self.pinColor = MKPinAnnotationColorGreen;
    }
    else if ( [annotationTypes containsObject:@"Restaurant"] && [showable containsObject:@"Restaurant"] ) {
        self.pinColor = MKPinAnnotationColorPurple;
    }
    else if ( [annotationTypes containsObject:@"Cafe"] && [showable containsObject:@"Cafe"] ) {
        self.pinColor = MKPinAnnotationColorPurple;
    }
    else {
        self.pinColor = MKPinAnnotationColorRed;
    }

    iOSAnnotation.annotation.delegate = self;
}

- (CClanAnnotationView *) initWithAnnotation:(id<MKAnnotation>)annotation
{
    self = [super initWithAnnotation:annotation reuseIdentifier:[CClanAnnotation identifier]];
    
    if ( self ) {
        self.canShowCallout = YES;
        self.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    
    return self;
}

#pragma mark - CClanAnnotationDelegate
- (void) annotation:(CClanAnnotation * const)annotation didDownloadCalloutImage:(UIImage * const)image
{
    self.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:image];
}

@end

@implementation CClanExtendedAnnotationView

- (void) setStateFromAnnotation:(id<MKAnnotation>) annotation andShowable:(NSSet *)showable
{
    CClanMapAnnotationApple *iOSAnnotation = (CClanMapAnnotationApple *)annotation;
    
    iOSAnnotation.annotation.delegate = self;

    CClanExtendedAnnotation *exannotation = (CClanExtendedAnnotation *)iOSAnnotation.annotation;
    [exannotation getAnnotationExtensions];
}

- (CClanExtendedAnnotationView *) initWithAnnotation:(id<MKAnnotation>)annotation
{
    self = [super initWithAnnotation:annotation reuseIdentifier:[CClanExtendedAnnotation identifier]];
    
    if ( self ) {
        self.canShowCallout = YES;
        self.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    
    return self;
}

#pragma mark - CClanAnnotationDelegate
- (void) annotation:(CClanAnnotation * const)annotation didDownloadCalloutImage:(UIImage * const)image
{
    self.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:image];
}

- (void) annotation:(CClanExtendedAnnotation * const)annotation didDownloadAnnotationImage:(UIImage * const)image
{
    self.image = image;
}

@end

#endif