//
//  CClanBundle.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanBundle.h"

@implementation CClanBundle

+ (NSString *)convertFilenameToThumbnail:(const NSString * const)filename
{
    if ( !filename ) {
        return nil;
    }
    
    return [[[filename stringByDeletingPathExtension] stringByAppendingString:@"_th"] stringByAppendingPathExtension:[filename pathExtension]];
}


+ (NSString *) fileInBundle:(const NSString *)filename
{
    if ( !filename ) {
        return nil;
    }

    return [[NSBundle mainBundle] pathForResource:[filename stringByDeletingPathExtension]
                                           ofType:[filename pathExtension]];
}

+ (NSString *) thumbnailInBundle:(const NSString *)filename
{
    return [CClanBundle fileInBundle:[CClanBundle convertFilenameToThumbnail:filename]];
}

@end
