//
//  CClanLoadingView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 21/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanLoadingView.h"
#import "CClanBundle.h"

@interface CClanLoadingView ()

@property (nonatomic, strong) UIImageView * const background;
@property (nonatomic, strong) UIActivityIndicatorView * const activity;
@property (nonatomic, strong) UILabel * const loading;

@end

@implementation CClanLoadingView

- (void) setText:(NSString * const)text
{
    self.loading.text = text;
}

- (void) showLoadingDisplay
{
    self.hidden = NO;
    self.background.hidden = NO;
    self.loading.hidden = NO;
    [self.activity startAnimating];
}

- (void) hideLoadingDisplay
{
    self.hidden = YES;
    self.background.hidden = YES;
    self.loading.hidden = YES;
    //    [self.activity stopAnimating];
}

- (void) layoutSubviews
{
    UIImage *bgimage = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"loading_bg.png"]];
    self.background = [[UIImageView alloc] initWithImage:bgimage];
    self.background.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
    
    self.loading = [[UILabel alloc] init];
    self.loading.textColor = [UIColor whiteColor];
    self.loading.backgroundColor = [UIColor clearColor];
    self.loading.textAlignment = NSTextAlignmentCenter;
    self.loading.text = @"Loading...";
    [self.loading sizeToFit];
    self.loading.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 1.33);
    
    self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activity.hidesWhenStopped = YES;
    self.activity.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 3.0);
    [self.activity startAnimating];
    
    [self addSubview:self.background];
    [self addSubview:self.loading];
    [self addSubview:self.activity];
}

@end
