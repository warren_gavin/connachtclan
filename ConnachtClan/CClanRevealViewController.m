//
//  CClanRevealViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 03/01/14.
//
//

#import "CClanRevealViewController.h"

static NSString * const SWSegueRearIdentifier = @"sw_rear";
static NSString * const SWSegueFrontIdentifier = @"sw_front";

@interface CClanRevealViewController ()

@end

@implementation CClanRevealViewController

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        self.delegate = self;
    }
    
    return self;
}

- (void)loadStoryboardControllers
{
    if ( self.storyboard && self.rearViewController == nil )
    {
        [self performSegueWithIdentifier:SWSegueRearIdentifier sender:nil];
        [self performSegueWithIdentifier:SWSegueFrontIdentifier sender:nil];
    }
}

//! This class will set the application's status bar
//! colour to light or dark depending on the position of the
//! front view controller.
//!
//! If the side view controller is visible then the status bar
//! should be light, otherwise it's the default dark colour
- (void) revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    UIApplication *clanApp = [UIApplication sharedApplication];
    
    switch (position) {
        case FrontViewPositionLeft:
            [clanApp setStatusBarStyle:UIStatusBarStyleDefault];
            break;
        default:
            [clanApp setStatusBarStyle:UIStatusBarStyleLightContent];
            break;
    }
}

@end
