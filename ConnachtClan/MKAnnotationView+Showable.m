//
//  MKAnnotationView+Showable.m
//  ConnachtClan
//
//  Created by Warren Gavin on 20/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "MKAnnotationView+Showable.h"

@implementation MKAnnotationView (Showable)

- (void) setStateFromAnnotation:(id<MKAnnotation>) annotation andShowable:(NSSet *)showable
{
    @throw [NSException exceptionWithName:@"AccessingAbstractInterface"
                                   reason:@"MKAnnotationView does not implement this method"
                                 userInfo:nil];
}

@end
