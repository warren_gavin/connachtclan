//
//  NSDictionary+Plist.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSDictionary+Plist.h"
#import "NSPropertyListSerialization+URL.h"

@implementation NSDictionary (Plist)

+ (NSDictionary *) dictionaryFromPlistAtURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval
{
    id plist = [NSPropertyListSerialization plistWithContentsOfURL:url cachePolicy:cachePolicy timeoutInterval:timeoutInterval];
    
    if ( ![plist isKindOfClass:[NSDictionary class]] ) {
        plist = nil;
    }
    
    return plist;
}

@end
