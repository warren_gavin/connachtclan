//
//  CClanTweet.h
//  ConnachtClan
//
//  Created by Warren Gavin on 25/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CClanTweet : NSObject

@property (nonatomic, strong) NSString           * const twitterName;
@property (nonatomic, strong) NSString           * const screenName;
@property (nonatomic, strong) NSAttributedString * const tweet;
@property (nonatomic, strong) NSString           * const identifier;
@property (nonatomic, strong) NSDate             * const time;
@property (nonatomic, strong) NSString           * const retweetedBy;
@property (nonatomic, strong) UIImage            *logo;

- (id) initWithJSONEntry:(NSDictionary * const)details;

- (NSString * const) tweetTimeRelativeTo:(NSDate * const)time;
- (void) setLogoThumbnailSize:(CGFloat)size;

@end
