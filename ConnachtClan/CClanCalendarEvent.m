//
//  CClanCalendarEvent.m
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarEvent.h"
#import "CClanBundle.h"
#import "CClanWeatherReportCache.h"

@implementation CClanCalendarEvent

+ (const CClanCalendarEvent *) calendarEventFromData: (NSDictionary *)data
{
    CClanMatchDetails *details = [[CClanMatchDetails alloc] init];
    details.homeTeam  = data[@"home"];
    details.homeScore = data[@"homeScore"];
    details.awayTeam  = data[@"away"];
    details.awayScore = data[@"awayScore"];
    details.previousMeetings = data[@"previous"];
    
    const CClanCalendarEvent *event = [[CClanCalendarEvent alloc] init];
    event.date         = data[@"date"];
    event.type         = data[@"type"];
    event.headline     = data[@"headline"];
    event.url          = data[@"url"];
    event.broadcasters = data[@"broadcasters"];
    event.competition  = data[@"competition"];
    event.location     = data[@"location"];
    event.coordinates  = data[@"coordinates"];
    event.matchDetails = details;
    
    return event;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\tdate: %@\n\theadine: %@\n\tmatch: %@", self.date, self.headline, self.matchDetails];
}

@end
