//
//  CClanPictures.h
//  ConnachtClan
//
//  Created by Warren Gavin on 23/10/12.
//
//

#import <Foundation/Foundation.h>
#import "CClanPictureAsset.h"

@class CClanPictures;

extern NSString * const CCLAN_PIX_PHOTO_COUNT_CHANGED;
extern NSString * const CCLAN_PIX_PHOTO_LOADED;
extern NSString * const CCLAN_PIX_PHOTO;

@interface CClanPictures : NSObject

+ (CClanPictures * const) instance;

- (void) loadPhotoAlbum;

//! Remove a photo from the App's photo album. This does not
//! remove the photo from the devices photo album. Users
//! will have to do that separately using the "Photos" App.
- (void) removePhotoFromAlbum:(CClanPictureAsset *)asset;

//! Adds an image to the applications photo album.
//! If there is no existing album one is created.
- (void) addPhotoToAlbum:(UIImage *)photo;

//! Export an image to the devices photo album
- (void) addPhotoToCameraRoll:(const UIImage *)photo;

- (NSUInteger) albumSize;

- (CClanPictureAsset * const) pictureInAlbumAtIndex:(NSUInteger)index;

@end
