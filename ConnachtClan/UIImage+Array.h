//
//  UIImage+Array.h
//  ConnachtClan
//
//  Created by Warren Gavin on 02/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

//! Helper methods for handling arays of images
@interface UIImage (Array)

//! Compose an array of images together in a row
+ (UIImage *) imageFromArray:(NSArray *)array separatedBy:(CGFloat)distance;

@end
