//
//  CClanPictureAsset.m
//  ConnachtClan
//
//  Created by Warren Gavin on 05/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanPictureAsset.h"
#import "UIImage+Resize.h"

@implementation CClanPictureAsset

- (id) initWithPath:(NSString *)path
              image:(UIImage  *)image
{
    self = [super init];

    self.path = path;
    self.image = image;

    return self;
}

@end
