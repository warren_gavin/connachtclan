//
//  CClanYelpAPIv2_0.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/10/13.
//
//

#import "CClanYelpAPIv2_0.h"
#import "CClanYelpResult.h"
#import "OAConsumer.h"
#import "OAToken.h"
#import "OAMutableURLRequest.h"

#import "NSArray+Blocks.h"

static NSString * const L_CLAN_OAUTH_CONSUMER_KEY    = @"DAi7f-3LLIVCPE-mCjPNrQ";
static NSString * const L_CLAN_OAUTH_CONSUMER_SECRET = @"shmQdO3XzY2Bse_rn-gCzCqFl1Y";
static NSString * const L_CLAN_OAUTH_TOKEN           = @"Uhv5L8K-sk1d6jVa42XA6a4-XG0j1noC";
static NSString * const L_CLAN_OAUTH_TOKEN_SECRET    = @"R9BdZ8v2_QLEENjqx1yWdjoa-iw";

@implementation CClanYelpAPIv2_0

+ (NSString * const)oauthConsumerKey
{
    return L_CLAN_OAUTH_CONSUMER_KEY;
}

+ (NSString * const)oauthConsumerSecret
{
    return L_CLAN_OAUTH_CONSUMER_SECRET;
}

+ (NSString * const)oauthTokenKey
{
    return L_CLAN_OAUTH_TOKEN;
}

+ (NSString * const)oauthTokenSecret
{
    return L_CLAN_OAUTH_TOKEN_SECRET;
}

+ (NSDictionary *) performSearch:(NSString *)requestUrl
{
    OAConsumer * const consumer  = [[OAConsumer alloc] initWithKey:[CClanYelpAPIv2_0 oauthConsumerKey]
                                                            secret:[CClanYelpAPIv2_0 oauthConsumerSecret]];
    OAToken    * const token     = [[OAToken alloc] initWithKey:[CClanYelpAPIv2_0 oauthTokenKey]
                                                         secret:[CClanYelpAPIv2_0 oauthTokenSecret]];
    
    OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl]
                                                                   consumer:consumer
                                                                      token:token
                                                                      realm:nil
                                                          signatureProvider:nil];
    
    [request prepare];
    
    id             result;
    NSURLResponse *response;
    NSData        *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    if ( data ) {
        result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    }
    
    if ( ![result isKindOfClass:[NSDictionary class]] ) {
        return nil;
    }

    return result;
}

+ (NSArray *) performSearchForCategory:(NSString *)category atLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude
{
    NSString     * const searchApi = @"http://api.yelp.com/v2/search?term=%@&radius_filter=2000&limit=10&ll=%f,%f";
    NSDictionary * const result    = [CClanYelpAPIv2_0 performSearch:[NSString stringWithFormat:searchApi, category, latitude, longitude]];
    
    return [result[@"businesses"] convertWithModifierBlock:^id(id element) {
        NSDictionary *userInfo = @{ @"category" : category };
        return [CClanYelpResult resultFromSearchResult:element userInfo:userInfo];
    }];
}

+ (NSDictionary *) performSearchForBusiness:(NSString *)id
{
    return [CClanYelpAPIv2_0 performSearch:[NSString stringWithFormat:@"http://api.yelp.com/v2/business/%@", id]];
}

@end
