//
//  CClanAnnotationDetailsTableViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 04/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanTableViewController.h"
#import "CClanMapSettings.h"

@class CClanAnnotation;

//! Detailed annotation information, displayed when
//! the left callout is selected on an annotation
@interface CClanAnnotationDetailsTableViewController : CClanTableViewController

//! The annoation that corresponds to the view displayed
@property (nonatomic, weak) CClanAnnotation *annotation;

@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UIImageView *attributionBackground;

@property (weak, nonatomic) IBOutlet UIImageView *attributionImage;

@property (weak, nonatomic) IBOutlet UILabel *attributionText;

//! The annotation's image
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

//! The annotation's address
@property (weak, nonatomic) IBOutlet UILabel *address;

//! The annotation's phone nubmer
@property (weak, nonatomic) IBOutlet UILabel *phone;

//! The annotation's site
@property (weak, nonatomic) IBOutlet UILabel *website;

@property (weak, nonatomic) IBOutlet UIImageView *infoImage;

@property (weak, nonatomic) IBOutlet UILabel *infoText;

@property (weak, nonatomic) IBOutlet UIImageView *infoBackground;

//! Activity indicator to show while waiting for annotation's image to download
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (weak, nonatomic) IBOutlet UISwitch *permanent;

@property (weak, nonatomic) id<CClanMapViewSettingsDelegate> delegate;
@end
