//
//  CClanGoogleSearchResult.m
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import "CClanGoogleSearchResult.h"
#import "CClanGoogleSearchAPI.h"
#import "CClanAnnotation.h"

const CGFloat L_IMAGE_FRAME_RATIO = 0.5625; // 16:9

@implementation CClanGoogleSearchResult

+(NSString *)imageRef:(NSArray *)photos
{
    NSString *imageRef;
    
    for ( NSDictionary *photoInfo in photos ) {
        CGFloat heigth = [photoInfo[@"height"] floatValue];
        CGFloat width  = [photoInfo[@"width"] floatValue];
        
        UIScreen *mainScreen = [UIScreen mainScreen];
        if ( mainScreen.bounds.size.width * mainScreen.scale <= width && heigth / width > L_IMAGE_FRAME_RATIO ) {
            imageRef = photoInfo[@"photo_reference"];
            break;
        }
    }
    
    return imageRef;
}

+(NSArray *) categories:(NSArray *)searchCategories
{
    NSMutableSet *uniqueCategories = [NSMutableSet setWithCapacity:searchCategories.count];

    for ( NSString *category in searchCategories ) {
        if ( [category isEqualToString:@"bar"] ) {
            [uniqueCategories addObject:@"Bar"];
        } else if ( [category isEqualToString:@"restaurant"] ) {
            [uniqueCategories addObject:@"Restaurant"];
        } else if ( [category isEqualToString:@"cafe"] ) {
            [uniqueCategories addObject:@"Cafe"];
        } else if ( [category isEqualToString:@"lodging"] ) {
            [uniqueCategories addObject:@"Accommodation"];
        }
    }
    
    return [uniqueCategories allObjects];
}

+(id<CClanMapSearchResult>) resultFromSearchResult:(NSDictionary *)searchResult
                                          userInfo:(NSDictionary *)userInfo
{
    NSDictionary *place        = [CClanGoogleSearchAPI performPlaceDetailsLookup:searchResult[@"reference"]];
    NSDictionary *placeDetails = place[@"result"];
    NSString     *imageRef     = [CClanGoogleSearchResult imageRef:placeDetails[@"photos"]];
    NSArray      *categories   = [CClanGoogleSearchResult categories:placeDetails[@"types"]];

    if ( !imageRef || !categories.count || !place ) {
        return nil;
    }

    CClanGoogleSearchResult *result = [[CClanGoogleSearchResult alloc] init];

    [result setAddressDetails:placeDetails[@"address_components"]];
    if ( !result.city || !result.country || !result.address ) {
        return nil;
    }
    
    result.categories = categories;
    result.imageRef   = imageRef;
    result.name       = placeDetails[@"name"];
    result.rating     = placeDetails[@"rating"];
    result.phone      = placeDetails[@"international_phone_number"];
    result.website    = placeDetails[@"website"];

    NSDictionary *coordinates = placeDetails[@"geometry"][@"location"];
    result.location = CLLocationCoordinate2DMake([coordinates[@"lat"] floatValue], [coordinates[@"lng"] floatValue]);

    return result;
}

- (CClanAnnotation *) annotation
{
    CClanAnnotation * annotation = [[CClanAnnotation alloc] init];
    
    annotation.name     = _name;
    annotation.location = _location;
    annotation.types    = _categories;
    annotation.address  = _address;
    annotation.city     = _city;
    annotation.country  = _country;
    annotation.phone    = _phone;
    annotation.website  = _website;
    annotation.fixed    = NO;
    
    dispatch_queue_t download_google_image = dispatch_queue_create("Download Google Place Image", NULL );
    dispatch_async( download_google_image, ^{
        annotation.image = [CClanGoogleSearchAPI performImageLookup:self.imageRef];
    });
    
    return annotation;
}

- (void) setAddressDetails:(NSArray *)addressComponents
{
    if ( addressComponents.count >= 3) {
        NSString *streetNumber;
        NSString *roadName;
        
        for ( NSDictionary *component in addressComponents ) {
            NSString *type = component[@"types"][0];
            
            if ( [type isEqualToString:@"country"] ) {
                self.country = component[@"short_name"];
            }
            else if ( [type isEqualToString:@"locality"] ) {
                self.city = component[@"short_name"];
            }
            else if ( [type isEqualToString:@"street_number"] ) {
                streetNumber = component[@"short_name"];
            }
            else if ( [type isEqualToString:@"route"] ) {
                roadName = component[@"short_name"];
            }
        }
        
        if ( streetNumber && roadName ) {
            self.address = [NSString stringWithFormat:@"%@ %@", streetNumber, roadName];
        }
        else {
            self.address = roadName;
        }
    }
}

@end
