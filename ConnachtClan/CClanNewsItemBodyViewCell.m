//
//  CClanNewsItemBodyViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/12/13.
//
//

#import "CClanNewsItemBodyViewCell.h"

@implementation CClanNewsItemBodyViewCell

- (CGFloat) height
{
    return _webView.scrollView.contentSize.height;
}

@end
