//
//  CClanPixViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/01/13.
//
//

#import <UIKit/UIKit.h>
#import "CClanPix.h"

@interface CClanPixViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView             *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic)          CClanPix                *asset;

@end
