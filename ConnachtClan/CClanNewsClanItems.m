//
//  CClanNewsClanItems.m
//  ConnachtClan
//
//  Created by Warren Gavin on 11/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanNewsClanItems.h"
#import "CClanDownloadedImagesCache.h"

@interface CClanNewsClanItems () <NSXMLParserDelegate>
@end

@implementation CClanNewsClanItems

- (void) parseDescription:(const NSString * const) description
{
    self.description = @"";

    NSString *wrapped_description = [NSString stringWithFormat:@"<description>%@</description>", description];
    wrapped_description = [wrapped_description stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
    wrapped_description = [wrapped_description stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];

    const NSXMLParser *const parser = [[NSXMLParser alloc] initWithData:[wrapped_description dataUsingEncoding:NSUTF8StringEncoding]];
    parser.delegate = self;
    
    [parser parse];
}

#pragma mark - NSXMLParserDelegate
- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
     attributes:(NSDictionary *)attributeDict
{
    if ( [elementName isEqualToString:@"img"] ) {
        self.image = [CClanDownloadedImagesCache imageAtURL:attributeDict[@"src"]];
    }
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.description = [self.description stringByAppendingString:string];
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
    self.description = [self.description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


@end
