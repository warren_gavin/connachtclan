//
//  CClanMapView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 08/11/13.
//
//

#import "CClanMapView.h"

#if defined(CCLAN_MAP_USE_GOOGLE)
# import "CClanMapViewGoogle.h"
# import "CClanMapAnnotationGoogle.h"
# define L_MAP_VIEW CClanMapViewGoogle
# define L_MAP_ANNOTATION CClanMapAnnotationGoogle
#else
# import "CClanMapViewApple.h"
# import "CClanMapAnnotationApple.h"
# define L_MAP_VIEW CClanMapViewApple
# define L_MAP_ANNOTATION CClanMapAnnotationApple
#endif

@implementation CClanMapViewInstance

+ (id<CClanMapView>) map
{
    return [[L_MAP_VIEW alloc] init];
}

+ (id<CClanMapAnnotation>) mapAnnotation:(CClanAnnotation *)annotation
{
    L_MAP_ANNOTATION *mapAnnotation = [[L_MAP_ANNOTATION alloc] init];
    
    mapAnnotation.annotation = annotation;
    
    return mapAnnotation;
}

@end
