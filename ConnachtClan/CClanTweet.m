//
//  CClanTweet.m
//  ConnachtClan
//
//  Created by Warren Gavin on 25/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanTweet.h"
#import "CClanDownloadedImagesCache.h"

#import "UIImage+Resize.h"

@interface CClanTweet ()

@property (nonatomic, strong) NSString * const logo_url;

@end

@implementation CClanTweet

- (id) initWithJSONEntry:(NSDictionary * const)tweet
{
    self = [super init];

    if ( self ) {
        self.identifier  = tweet[@"id_str"];

        NSDictionary *tweetInfo  = tweet[@"retweeted_status"];
        if ( tweetInfo ) {
            self.retweetedBy = tweet[@"user"][@"screen_name"];
        }
        else {
            tweetInfo = tweet;
        }
        
        [self setTweetText:tweetInfo[@"text"] entities:tweetInfo[@"entities"]];
        [self setTweetTime:tweet[@"created_at"]];
        
        NSDictionary *user_info  = tweetInfo[@"user"];
        self.twitterName = user_info[@"name"];
        self.screenName  = user_info[@"screen_name"];
        
        // The profile image logo is the small version, which is not big enough for retina
        // displays.
        //
        // The imge URL has the form  [hexdigits]_normal.[type] and the original logo
        // is the same with the "_normal" text removed.
        self.logo_url = [user_info[@"profile_image_url"] stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
    }
    
    return self;
}

- (NSString *)sanitiseTweetText:(NSString *)text
{
    return [text stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
}

- (void) setTweetText:(NSString *)text entities:(NSDictionary *)entities
{
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:text];
    NSDictionary              *attrFormat = @{ NSForegroundColorAttributeName : [UIColor blueColor] };
    
    for ( NSDictionary *urlInfo in entities[@"urls"] ) {
        NSUInteger start = [urlInfo[@"indices"][0] integerValue];
        NSUInteger end   = [urlInfo[@"indices"][1] integerValue];
        
        [attrString setAttributes:attrFormat range:NSMakeRange(start, end - start)];
    }
    
    NSRange ampersand = [[attrString string] rangeOfString:@"&amp;"];
    if ( ampersand.location != NSNotFound ) {
        [attrString replaceCharactersInRange:ampersand withString:@"&"];
    }

    self.tweet = attrString;
}

- (void) setTweetTime:(NSString *)creationDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
    
    self.time = [formatter dateFromString:creationDate];
}

- (void) setLogoThumbnailSize:(CGFloat)size
{
    if ( !_logo || _logo.size.height != size ) {
        _logo = [[CClanDownloadedImagesCache imageAtURL:self.logo_url] thumbnailImage:size
                                                                    transparentBorder:0
                                                                         cornerRadius:7 * [[UIScreen mainScreen] scale]
                                                                 interpolationQuality:kCGInterpolationHigh];
    }
}

- (NSString * const) tweetTimeRelativeTo:(NSDate * const)time
{
    NSTimeInterval interval = [time timeIntervalSinceDate:self.time];
    
    if ( interval < 60.0 ) {
        int seconds = (int)interval;
        return [NSString stringWithFormat:@"%ds", seconds];
    }
    
    if ( interval < 60.0 * 60.0 ) {
        int minutes = (int)(interval / 60.0);
        return [NSString stringWithFormat:@"%dm", minutes];
    }
    
    if ( interval < (60.0 * 60.0 * 24.0) ) {
        int hours = (int)(interval / (60.0 * 60.0));
        return [NSString stringWithFormat:@"%dh", hours];
    }
    
    NSCalendar        *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit fromDate:time];
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];

    if ( dateComponents.day < 10 ) {
        [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"d MMM"
                                                                 options:0
                                                                  locale:[NSLocale currentLocale]]];
    }
    else {
        [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"dd MMM"
                                                                 options:0
                                                                  locale:[NSLocale currentLocale]]];
    }

    return [formatter stringFromDate:time];
}

@end
