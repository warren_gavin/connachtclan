//
//  CClanYelpAPIv2_0.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/10/13.
//
//

#import <Foundation/Foundation.h>

@interface CClanYelpAPIv2_0 : NSObject

+ (NSArray *) performSearchForCategory:(NSString *)category atLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude;

+ (NSDictionary *) performSearchForBusiness:(NSString *)id;

@end
