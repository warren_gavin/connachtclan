//
//  CClanErrorHandler.h
//  ConnachtClan
//
//  Created by Warren Gavin on 12/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanLoadingView.h"

#define SuppressPerformSelectorLeakWarning(op) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
op; \
_Pragma("clang diagnostic pop") \
} while (0)

typedef enum {
    CCLAN_TWITTER_NO_FEED = -100,
    CCLAN_MAP_NO_DOWNLOAD = -200,
    CCLAN_MAP_NO_LOCATION = -201
} CClanErrorCode;

//! Alert handling factory class
@interface CClanErrorHandler : NSObject

//! Display an error in an alert view and stop an activity spinner if required
+ (void) handleError:(const NSError *)error withIndicator:(const UIActivityIndicatorView *) activity;

//! Display an error in an alert view and hide a loading view if required
+ (void) handleError:(const NSError *)error withLoadingView:(const CClanLoadingView *)loading;

//! Display a general alert view
+ (void) displayAlert:(NSString *)text;

// Domain of app generated errors
+ (NSString * const) domain;

@end
