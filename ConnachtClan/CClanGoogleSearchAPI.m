//
//  CClanGoogleAPI.m
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import "CClanMapSearch.h"
#import "CClanGoogleAPI.h"
#import "CClanGoogleSearchAPI.h"
#import "CClanGoogleSearchResult.h"
#import "CClanConstants.h"
#import "CClanLocation.h"

#import "NSArray+Blocks.h"

// Root API URL
NSString *L_GOOGLE_API_URL = @"https://maps.googleapis.com/maps/api/place/";

// Search API
NSString *L_GOOGLE_SEARCH  = @"nearbysearch/json?sensor=false";
NSString *L_GOOGLE_QUERY   = @"&types=bar%7Crestaurant%7Clodging%7Ccafe&radius=2500";

// Place details API
NSString *L_GOOGLE_DETAILS = @"details/json?sensor=false";

// Image API
NSString *L_GOOGLE_IMAGE   = @"photo?sensor=false";

@interface CClanGoogleSearchAPI()

//! Location name for search, this is not used in the search itself
//! but is required data for the notification to inform observers
//! about the new search hit parameters
@property (nonatomic, strong) NSString *location;

//! Long and lat of the region being searched
@property (nonatomic) CClanMapViewRegion region;

@end

@implementation CClanGoogleSearchAPI

-(instancetype) initWithLocation:(NSString *)location inRegion:(CClanMapViewRegion)region
{
    self = [super init];

    if ( self ) {
        _location = location;
        _region   = region;
    }

    return self;
}

+(NSString *) formattedAPIKey
{
    return [NSString stringWithFormat:@"&key=%@", [CClanGoogleAPI apiKey]];
}

+(NSData *) callAPI:(NSURL *)url
{
    NSURLRequest  * const request = [NSURLRequest requestWithURL:url
                                                     cachePolicy:CCLAN_URL_REQUEST_CACHE_POLICY
                                                 timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];
    
    NSURLResponse  *response;
    NSData * data = [NSURLConnection sendSynchronousRequest:request
                                          returningResponse:&response
                                                      error:nil];
    
    return data;
}

+(id) callJSONAPI:(NSURL *)url
{
    NSData * const data = [CClanGoogleSearchAPI callAPI:url];

    if ( !data ) {
        return nil;
    }
    
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
}

-(void) notifyResults:(NSArray *)hits
{
    if ( hits.count ) {
        dispatch_async( dispatch_get_main_queue(), ^{
            NSDictionary *results = @{ @"location" : _location, @"results" : hits };
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_NEW_MAP_SEARCH_HITS object:results];
        });
    }
}

-(void) processSearchResults:(id)result
{
    if ( ![result isKindOfClass:[NSDictionary class]] ) {
        return;
    }
    
    [self notifyResults:[result[@"results"] convertWithModifierBlock:^id(id element) {
        NSDictionary *details = element;
        NSDictionary *location = details[@"geometry"][@"location"];
        
        CLLocationCoordinate2D centre = CLLocationCoordinate2DMake([location[@"lat"] floatValue], [location[@"lng"] floatValue]);
        
        if ( ![CClanLocation coordinate:centre isInRegion:_region] ) {
            return nil;
        }

        CClanGoogleSearchResult *result = [CClanGoogleSearchResult resultFromSearchResult:details userInfo:nil];
        return [result annotation];
    }]];
    
    if ( result[@"next_page_token"] ) {
        NSString *pageToken = [NSString stringWithFormat:@"&pagetoken=%@", result[@"next_page_token"]];
        NSString *fullURL   = [NSString stringWithFormat:@"%@%@%@%@", L_GOOGLE_API_URL, L_GOOGLE_SEARCH, pageToken, [CClanGoogleSearchAPI formattedAPIKey]];
        
        [self processSearchResults:[CClanGoogleSearchAPI callJSONAPI:[NSURL URLWithString:fullURL]]];
    }
}

-(void) performSearch
{
    NSString *locationParameters = [NSString stringWithFormat:@"&location=%f,%f", _region.latitude, _region.longitude];
    NSString *fullURL            = [NSString stringWithFormat:@"%@%@%@%@%@", L_GOOGLE_API_URL,
                                                                             L_GOOGLE_SEARCH,
                                                                             L_GOOGLE_QUERY,
                                                                             locationParameters,
                                                                             [CClanGoogleSearchAPI formattedAPIKey]];
    
    [self processSearchResults:[CClanGoogleSearchAPI callJSONAPI:[NSURL URLWithString:fullURL]]];
}

+(NSDictionary *) performPlaceDetailsLookup:(NSString *)placeRef
{
    NSString *reference = [NSString stringWithFormat:@"&reference=%@", placeRef];
    NSString *fullURL   = [NSString stringWithFormat:@"%@%@%@%@", L_GOOGLE_API_URL, L_GOOGLE_DETAILS, reference, [CClanGoogleSearchAPI formattedAPIKey]];
    id        result    = [CClanGoogleSearchAPI callJSONAPI:[NSURL URLWithString:fullURL]];
    
    if ( ![result isKindOfClass:[NSDictionary class]] ) {
        return nil;
    }
    
    return result;
}

+(UIImage *) performImageLookup:(NSString *)imageRef
{
    UIScreen   *deviceScreen   = [UIScreen mainScreen];
    CGFloat     maxWidthPixels = deviceScreen.bounds.size.width * deviceScreen.scale;
    NSString   *photoReference = [NSString stringWithFormat:@"&photoreference=%@", imageRef];
    NSString   *maxWidth       = [NSString stringWithFormat:@"&maxwidth=%d", (int)maxWidthPixels];
    NSString   *fullURL        = [NSString stringWithFormat:@"%@%@%@%@%@", L_GOOGLE_API_URL, L_GOOGLE_IMAGE, photoReference, maxWidth, [CClanGoogleSearchAPI formattedAPIKey]];
    
    return [UIImage imageWithData:[CClanGoogleSearchAPI callAPI:[NSURL URLWithString:fullURL]] scale:deviceScreen.scale];
}

@end
