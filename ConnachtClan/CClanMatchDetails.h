//
//  CClanMatchDetails.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import <Foundation/Foundation.h>

@interface CClanMatchDetails : NSObject

@property (nonatomic, strong) NSString *homeTeam;
@property (nonatomic, strong) NSString *awayTeam;
@property (nonatomic, strong) NSString *homeScore;
@property (nonatomic, strong) NSString *awayScore;

//! Array of CClanMatchDetails objects
@property (nonatomic, strong) NSArray  *previousMeetings;

@end
