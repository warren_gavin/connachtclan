//
//  CClanButton.h
//  ConnachtClan
//
//  Created by Warren Gavin on 10/06/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CClanPictureAsset;

//! An extended UIButton that associates a clan photo with the button displayed
@interface CClanPictureAssetButton : UIButton

//! The clan photo
@property (weak, nonatomic) CClanPictureAsset *asset;

@end
