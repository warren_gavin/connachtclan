//
//  CClanNewsItemHeadlineViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/12/13.
//
//

#import "CClanTableView.h"

@interface CClanNewsItemHeadlineViewCell : CClanTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headline;

@end
