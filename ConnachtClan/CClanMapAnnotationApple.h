//
//  CClanMapAnnotationiOS.h
//  ConnachtClan
//
//  Created by Warren Gavin on 10/11/13.
//
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CClanMapView.h"
#import "CClanAnnotation.h"

@interface CClanMapAnnotationApple : NSObject <MKAnnotation, CClanMapAnnotation>

@end

#endif