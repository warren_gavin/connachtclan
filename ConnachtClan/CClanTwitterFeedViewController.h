//
//  CClanTwitterFeedViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 29/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanTableViewController.h"
#import "CClanLoadingView.h"

@interface CClanTwitterFeedViewController : CClanTableViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem  *sidebarButton;

@end
