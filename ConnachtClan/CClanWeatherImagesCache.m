//
//  CClanWeatherImagesCache.m
//  ConnachtClan
//
//  Created by Warren Gavin on 27/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanWeatherImagesCache.h"
#import "CClanCalendar.h"
#import "CClanBundle.h"

static const CClanWeatherImagesCache *g_instance;

@interface CClanWeatherImagesCache()

// Cache of stored weather images
@property (nonatomic, strong) NSMutableDictionary *cache;

// Container of weather image filenames
@property (nonatomic, strong) NSDictionary        *images;

- (CClanWeatherImagesCache *) init;

@end

@implementation CClanWeatherImagesCache

- (CClanWeatherImagesCache *) init
{
    self = [super init];

    if ( self ) {
        self.images = @{ @(CClanWeatherImagesCacheFair):             @[@"fair_d.png", @"fair_n.png"],
                         @(CClanWeatherImagesCacheSunny):            @[@"sun.png",    @"moon.png"],
                         @(CClanWeatherImagesCacheFog):              @[@"fair_d.png", @"fair_n.png"],
                         @(CClanWeatherImagesCacheCloudy):           @[@"cloud.png", @"cloud.png"],
                         @(CClanWeatherImagesCacheMostlyCloudy):     @[@"fair_d.png", @"fair_n.png"],
                         @(CClanWeatherImagesCacheLightShowers):     @[@"rain.png", @"rain.png"],
                         @(CClanWeatherImagesCacheShowers):          @[@"rain.png", @"rain.png"],
                         @(CClanWeatherImagesCacheWind):             @[@"fair_d.png", @"fair_n.png"],
                         @(CClanWeatherImagesCacheRain):             @[@"rain.png", @"rain.png"],
                         @(CClanWeatherImagesCacheLightRain):        @[@"rain.png", @"rain.png"],
                         @(CClanWeatherImagesCacheThunder):          @[@"storm.png", @"storm.png"],
                         @(CClanWeatherImagesCacheScatteredThunder): @[@"storm.png", @"storm.png"],
                         @(CClanWeatherImagesCacheSleetShowers):     @[@"snow.png", @"snow.png"],
                         @(CClanWeatherImagesCacheSleet):            @[@"snow.png", @"snow.png"],
                         @(CClanWeatherImagesCacheLightSnow):        @[@"snow.png", @"snow.png"],
                         @(CClanWeatherImagesCacheSnow):             @[@"snow.png", @"snow.png"] };
        
        self.cache = [[NSMutableDictionary alloc] init];
    }

    return self;
}

+ (UIImage *) imageForCondition:(CClanWeatherImagesCacheWeatherCondition)condition isDaytime:(BOOL)isdaytime
{
    // Cache is contained in a singleton
    if ( !g_instance ) {
        g_instance = [[CClanWeatherImagesCache alloc] init];
    }

    // there are two images for the weather condition, 1 each for daytime/nighttime
    NSArray  *condition_images = g_instance.cache[[NSNumber numberWithInteger:condition]];
    
    UIImage *image;
    if ( 2 == condition_images.count ) {
        image = condition_images[(isdaytime ? 0 : 1)];
    }

    // if there are no images create them and add them to the cache
    if ( !image ) {
        NSArray *condition_images = g_instance.images[[NSNumber numberWithInteger:condition]];
        
        if ( 2 != condition_images.count ) {
            return nil;
        }

        UIImage *dayimage   = [[UIImage alloc] initWithContentsOfFile:[CClanBundle fileInBundle:condition_images[0]]];
        UIImage *nightimage = [[UIImage alloc] initWithContentsOfFile:[CClanBundle fileInBundle:condition_images[1]]];
        
        g_instance.cache[[NSNumber numberWithInteger:condition]] = @[dayimage, nightimage];
        
        image = (isdaytime ? dayimage : nightimage);
    }
    
    return image;
}

@end
