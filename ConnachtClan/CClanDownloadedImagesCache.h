//
//  CClanDownloadedImagesCache.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Caching class for downloaded images
@interface CClanDownloadedImagesCache : NSObject

//! An image, either from the cache or downloaded if not already there
+ (UIImage *) imageAtURL:(NSString *)image_url;

//! An image, either from the cache or downloaded if not already there,
//! with rounded corners and a border
+ (UIImage *) imageAtURL:(NSString *)image_url
        withCornerRadius:(NSInteger)radius
               andBorder:(NSInteger)border;

@end
