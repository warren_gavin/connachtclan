//
//  CClanGoogleSearch.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import <Foundation/Foundation.h>
#import "CClanMapSearch.h"

@interface CClanGoogleSearch : NSObject <CClanMapSearcher>

@end
