//
//  CClanGoogleAPI.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import <Foundation/Foundation.h>
#import "CClanMapView.h"

@interface CClanGoogleSearchAPI : NSObject

-(instancetype) initWithLocation:(NSString *)location inRegion:(CClanMapViewRegion)region;

-(void) performSearch;

+(NSDictionary *) performPlaceDetailsLookup:(NSString *)placeRef;

+(UIImage *) performImageLookup:(NSString *)imageRef;

@end
