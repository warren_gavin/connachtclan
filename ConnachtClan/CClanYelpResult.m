//
//  CClanYelpResult.m
//  ConnachtClan
//
//  Created by Warren Gavin on 05/10/13.
//
//

#import "CClanYelpResult.h"
#import "CClanYelpAPIv2_0.h"
#import "CClanAnnotation.h"

@implementation CClanYelpResult

+(id<CClanMapSearchResult>)resultFromSearchResult:(NSDictionary *)searchResult userInfo:(NSDictionary *)userInfo
{
    if ( [searchResult[@"is_closed"] integerValue] == 1 || [searchResult[@"location"][@"address"] count] == 0 ) {
        return nil;
    }
    
    CClanYelpResult *result = [[CClanYelpResult alloc] init];
    
    result.name           = searchResult[@"name"];
    result.categories     = @[ userInfo[@"category"] ];
    result.phone          = searchResult[@"display_phone"];
    result.address        = searchResult[@"location"][@"address"][0];
    result.city           = searchResult[@"location"][@"city"];
    result.country        = searchResult[@"location"][@"country_code"];
    result.imageUrl       = searchResult[@"image_url"];
    result.ratingImageUrl = searchResult[@"rating_img_url_large"];
    result.moreUrl        = searchResult[@"url"];
    
    return result;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.name, [self.categories componentsJoinedByString:@", "]];
}

- (CClanAnnotation *) annotation
{
    CClanAnnotation *annotation = [[CClanAnnotation alloc] init];

    annotation.fixed   = NO;
    annotation.name    = self.name;
    annotation.types   = self.categories;
    annotation.address = self.address;
    annotation.city    = self.city;
    annotation.country = self.country;
    annotation.phone   = self.phone;
    annotation.website = self.moreUrl;

    dispatch_queue_t download_image = dispatch_queue_create("Yelp image download", NULL );
    dispatch_async( download_image, ^{
        annotation.image   = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageUrl]]];
    });

    return annotation;
}

- (void) addCategories:(NSArray *)categories
{
    NSMutableSet *allCategories = [NSMutableSet setWithArray:categories];
    [allCategories addObjectsFromArray:self.categories];
    
    self.categories = [allCategories allObjects];
}

- (BOOL) isEqual:(id)object
{
    CClanYelpResult *result = object;
    return [self.name isEqualToString:result.name];
}

@end
