//
//  CClanWebPageController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 15/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanWebPageController : UIViewController <UIWebViewDelegate>

- (void) setURLToLoad:(NSString  *)url;
- (void) setHtmlString:(NSString *)html;

@end
