//
//  ABNewPersonViewController+CClanAnnotation.m
//  ConnachtClan
//
//  Created by Warren Gavin on 20/11/12.
//
//

#import "ABNewPersonViewController+CClanAnnotation.h"
#import "CClanAnnotation.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

static void
l_SetName(ABRecordRef                   io_entry,
          const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABRecordSetValue(io_entry, kABPersonFirstNameProperty, (__bridge CFStringRef)in_annotation.name, &error);
}

static void
l_SetPhoneNumber(ABRecordRef                   io_entry,
                 const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef phones = ABMultiValueCreateMutable(kABMultiStringPropertyType);

    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(phones, (__bridge CFStringRef)in_annotation.phone, kABPersonPhoneMainLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonPhoneProperty, phones, &error);
    
    CFRelease(phones);
}

static void
l_SetAddress(ABRecordRef                   io_entry,
             const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef address = ABMultiValueCreateMutable(kABDictionaryPropertyType);

    CFStringRef keys[] = { kABPersonAddressStreetKey, kABPersonAddressCityKey, kABPersonAddressCountryKey };
    CFStringRef values[] = { (__bridge CFStringRef)in_annotation.address,
                             (__bridge CFStringRef)in_annotation.city,
                             (__bridge CFStringRef)in_annotation.country };
    
    CFDictionaryRef aDict = CFDictionaryCreate(kCFAllocatorDefault,
                                               (void *)keys,
                                               (void *)values,
                                               sizeof keys / sizeof keys[0],
                                               &kCFCopyStringDictionaryKeyCallBacks,
                                               &kCFTypeDictionaryValueCallBacks);
    
    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(address, aDict, kABHomeLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonAddressProperty, address, &error);

    CFRelease(aDict);
    CFRelease(address);
}

static void
l_SetPhoto(ABRecordRef                   io_entry,
           const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABPersonSetImageData(io_entry, (__bridge CFDataRef)UIImagePNGRepresentation(in_annotation.image), &error);
}

static void
l_SetHomepage(ABRecordRef                   io_entry,
              const CClanAnnotation * const in_annotation)
{
    CFErrorRef error = NULL;
    ABMutableMultiValueRef urls = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueIdentifier identifier;
    ABMultiValueAddValueAndLabel(urls, (__bridge CFStringRef)in_annotation.website, kABPersonHomePageLabel, &identifier);
    
    ABRecordSetValue(io_entry, kABPersonURLProperty, urls, &error);
    
    CFRelease(urls);
}


@implementation ABNewPersonViewController (CClanAnnotation)

+ (UINavigationController *) controllerForCClanAnnotation:(const CClanAnnotation *const) annotation
                                                 delegate:(id<ABNewPersonViewControllerDelegate>) delegate
{
    ABRecordRef      entry = ABPersonCreate();

    l_SetName       ( entry, annotation );
    l_SetPhoneNumber( entry, annotation );
    l_SetAddress    ( entry, annotation );
    l_SetPhoto      ( entry, annotation );
    l_SetHomepage   ( entry, annotation );

    ABNewPersonViewController *controller = [[ABNewPersonViewController alloc] init];
    controller.newPersonViewDelegate = delegate;
    controller.displayedPerson = entry;

    return [[UINavigationController alloc] initWithRootViewController:controller];
}

@end
