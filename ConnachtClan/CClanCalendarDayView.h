//
//  CClanCalendarDayView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CClanCalendarEvent;

//! View for a given day in a calendar
@interface CClanCalendarDayView : UIView

//! The event that falls on this day, if any
@property (nonatomic, strong) const CClanCalendarEvent const *event;

//! Sets the appearance of the view onscreen
//!
//! The view will be dimmed if it is not in the active month. If there is an event
//! on this date then the date will be highlighted onscreen
- (void) setDetailsForDate:(NSDate *)date
                 ifInMonth:(NSDate *)month
              withCalendar:(NSCalendar *)calendar;

@end
