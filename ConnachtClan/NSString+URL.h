//
//  NSString+URL.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

+ (NSString *)stringWithContentsOfURL:(NSURL *)url
                             encoding:(NSStringEncoding)enc
                          cachePolicy:(NSURLRequestCachePolicy)cachePolicy
                      timeoutInterval:(NSTimeInterval)timeoutInterval;

@end
