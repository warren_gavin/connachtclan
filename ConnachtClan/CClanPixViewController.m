//
//  CClanPixViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/01/13.
//
//

#import "CClanPixViewController.h"
#import "CClanPictures.h"
#import "CClanPixViewCell.h"
#import "CClanPictureAsset.h"
#import "CClanBundle.h"
#import "CClanErrorHandler.h"
#import "CClanPictureInspectorViewController.h"
#import "CClanPix+Extensions.h"
#import "CClanConstants.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+Resize.h"

static const CGFloat     L_MAX_PHOTO_DIMENSION    = 620.0;

@interface CClanPixViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CClanPictureInspectorDelegate>

@property (nonatomic, strong) UIManagedDocument *photoDatabase;
@property (nonatomic, copy)   NSArray           *photos;

@end

@implementation CClanPixViewController

- (void) setBackgroundImage
{
    UIImage *background = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"background.png"]];
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:background];
    self.collectionView.backgroundView.contentMode = UIViewContentModeBottom;
}

- (void) reloadPhotos
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"CClanPix"];
    fetch.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES] ];
    
    NSError *error;
    NSArray *photos = [self.photoDatabase.managedObjectContext executeFetchRequest:fetch error:&error];
    
    if ( photos && !error ) {
        self.photos = photos;
    }
    else {
        [self.activity hideLoadingDisplay];
    }
}

- (void) databaseIsReady
{
    if ( UIDocumentStateNormal == self.photoDatabase.documentState ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(photoDatabaseChanged:)
                                                     name:NSManagedObjectContextObjectsDidChangeNotification
                                                   object:self.photoDatabase.managedObjectContext];
        
        [self reloadPhotos];
    }
}

- (void) useDatabase
{
    if ( ![[NSFileManager defaultManager] fileExistsAtPath:[self.photoDatabase.fileURL path]] ) {
        [self.photoDatabase saveToURL:self.photoDatabase.fileURL
                     forSaveOperation:UIDocumentSaveForCreating
                    completionHandler:^(BOOL success) {
                        if ( success ) {
                            [self databaseIsReady];
                        }
                    }];
    }
    else if ( UIDocumentStateClosed == self.photoDatabase.documentState ) {
        [self.photoDatabase openWithCompletionHandler:^(BOOL success) {
            if ( success ) {
                [self databaseIsReady];
            }
        }];
    }
    else if ( UIDocumentStateNormal == self.photoDatabase.documentState ) {
        [self databaseIsReady];
    }
}

- (void) setPhotoDatabase:(UIManagedDocument *)photoDatabase
{
    if ( _photoDatabase != photoDatabase ) {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter removeObserver:self
                                      name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
                                    object:_photoDatabase.managedObjectContext.persistentStoreCoordinator];
        
        _photoDatabase = photoDatabase;
        
        [notificationCenter addObserver:self
                               selector:@selector(photoDatabaseChanged:)
                                   name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
                                 object:_photoDatabase.managedObjectContext.persistentStoreCoordinator];
        
        [self useDatabase];
    }
}

- (void) setPhotos:(NSArray *)photos
{
    if ( ![self.photos isEqualToArray:photos] ) {
        _photos = photos;
        [self.collectionView reloadData];
    }

    [self.activity hideLoadingDisplay];
}

//! Sets the buttons to display on the view navigation bar.
- (void) loadBarButtons
{
    UIBarButtonItem *camera = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                            target:self
                                                                            action:@selector(launchCamera:)];
    
    UIBarButtonItem *addpic = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                            target:self
                                                                            action:@selector(addPhotosFromCameraRoll:)];
    
    self.navigationItem.rightBarButtonItems = @[camera, addpic];
}

- (void) loadPhotoDatabase
{
    if ( !self.photoDatabase ) {
        NSURL *url = [CClanPix urlForClanPixDatabase];
        self.photoDatabase = [[UIManagedDocument alloc] initWithFileURL:url];
    }
}

- (void) photoDatabaseChanged:(NSNotification *)notification
{
    // Inserted objects aren't set up yet, they're just place holders
    // so we ignore the reload for an insterted object to avoid adding
    // empty data to the display
    if ( notification.userInfo[NSInsertedObjectsKey] ) {
        [self.activity showLoadingDisplay];
        [self reloadPhotos];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadBarButtons];
    [self.activity showLoadingDisplay];
    [self setBackgroundImage];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadPhotoDatabase];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self.photoDatabase saveToURL:self.photoDatabase.fileURL
                 forSaveOperation:UIDocumentSaveForOverwriting
                completionHandler:nil];

    [super viewDidDisappear:animated];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CClanPixViewCell *cell = sender;
    CClanPictureInspectorViewController *controller = segue.destinationViewController;
    
    controller.delegate = self;
    [controller setImageToDisplay:cell.asset];
}

//! Creates an image picker object of the specified type
- (UIImagePickerController *)imagePickerOfType:(UIImagePickerControllerSourceType)type
{
    UIImagePickerController *controller = nil;
    
    if ( [UIImagePickerController isSourceTypeAvailable:type] ) {
        NSString *imagetype = (NSString *)kUTTypeImage;
        NSArray  *available = [UIImagePickerController availableMediaTypesForSourceType:type];
        
        if ( [available containsObject:imagetype] ) {
            controller = [[UIImagePickerController alloc] init];
            
            controller.delegate = self;
            controller.sourceType = type;
            controller.mediaTypes = @[imagetype];
            controller.navigationBar.barTintColor = CCLAN_COLOUR;
        }
    }
    
    return controller;
}

//! Open a camera view
- (void) launchCamera:(UIBarButtonItem *)sender
{
    UIImagePickerController *controller = [self imagePickerOfType:UIImagePickerControllerSourceTypeCamera];
    
    if ( controller ) {
        controller.allowsEditing = NO;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        [CClanErrorHandler displayAlert:@"Can't find a camera."];
    }
}

//! Open a popover to allow the user to select images to add to the photo albu,
- (void) addPhotosFromCameraRoll:(UIBarButtonItem *)sender
{
    UIImagePickerController *controller = [self imagePickerOfType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    if ( controller ) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        [CClanErrorHandler displayAlert:@"Can't find the photo library."];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.photos.count;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CClanPixViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ClanPix Cell" forIndexPath:indexPath];

    CClanPix *asset = self.photos[indexPath.row];
    cell.asset = asset;
    
    return cell;
}

#pragma mark - UIImagePickerController delegate methods
- (UIImage *) taggedClanPic:(UIImage *)image
{
    UIImage *logo = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"clanpics.png"]];
    
    UIImage *pic = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                              bounds:CGSizeMake(L_MAX_PHOTO_DIMENSION, L_MAX_PHOTO_DIMENSION)
                                interpolationQuality:kCGInterpolationHigh];
    
    UIGraphicsBeginImageContextWithOptions(pic.size, NO, [[UIScreen mainScreen] scale]);
    [pic drawInRect:CGRectMake(0, 0, pic.size.width, pic.size.height)];
    [logo drawInRect:CGRectMake(pic.size.width - logo.size.width - 10,
                                pic.size.height - logo.size.height - 10,
                                logo.size.width,
                                logo.size.height)];
    
    UIImage *clanpic = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return clanpic;
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        UIImage *clanpic = [self taggedClanPic:info[UIImagePickerControllerOriginalImage]];
        
        [CClanPix clanPixWithImage:clanpic forManagedObjectContext:self.photoDatabase.managedObjectContext];
        [self.photoDatabase saveToURL:self.photoDatabase.fileURL
                     forSaveOperation:UIDocumentSaveForOverwriting
                    completionHandler:nil];
    });

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CClanPictureInspector delegate methods
- (void) elementWasSelectedForDeletion:(CClanPix *)asset
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self.photoDatabase.managedObjectContext deleteObject:asset];
        [self.photoDatabase saveToURL:self.photoDatabase.fileURL
                     forSaveOperation:UIDocumentSaveForOverwriting
                    completionHandler:nil];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadPhotos];
            [self.navigationController popViewControllerAnimated:YES];
        });
    });
}

#pragma mark - dealloc
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
