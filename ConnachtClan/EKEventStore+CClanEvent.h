//
//  EKEventStore+CClanEvent.h
//  ConnachtClan
//
//  Created by Warren Gavin on 17/11/12.
//
//

#import <EventKit/EventKit.h>

@class CClanCalendarEvent;
@class EKEvent;

@interface EKEventStore (CClanEvent)

- (void) addClanEvent:(const CClanCalendarEvent * const) event withAlert:(BOOL)alert controller:(id<UIAlertViewDelegate>) delegate;

- (EKEvent *) storedClanEvent:(const CClanCalendarEvent * const) event;

@end
