//
//  NSArray+Blocks.m
//  ConnachtClan
//
//  Created by Warren Gavin on 04/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "NSArray+Blocks.h"

@implementation NSArray (Blocks)

- (NSArray *) convertWithModifierBlock:(ElementModifierOperation)block
{
    NSMutableArray *modifiedArray = [NSMutableArray arrayWithCapacity:self.count];
    
    for ( id element in self ) {
        id convertedElement = block( element );
        if ( convertedElement ) {
            [modifiedArray addObject:convertedElement];
        }
    }
    
    if ( modifiedArray.count == 0 ) {
        return nil;
    }
    
    return [NSArray arrayWithArray:modifiedArray];
}

@end
