//
//  CClanMapSearch.m
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import "CClanMapSearch.h"
#import "CClanYelp.h"
#import "CClanGoogleSearch.h"

NSString * const CCLAN_NEW_MAP_SEARCH_HITS = @"be.apokrupto.CClanMapSearch.NewSearchHits";

@implementation CClanMapSearch

+ (Class<CClanMapSearcher>) searcher
{
    return [CClanGoogleSearch class];
}

@end
