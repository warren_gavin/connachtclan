//
//  CClanPictureViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 14/07/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "AQGridViewCell.h"

@class CClanPictureAssetButton;

@interface CClanPictureViewCell : AQGridViewCell

@property (nonatomic, strong) CClanPictureAssetButton *button;

@end
