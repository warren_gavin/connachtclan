//
//  CClanMapSearch.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import <Foundation/Foundation.h>
#import "CClanMacros.h"
#import "CClanMapView.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_NEW_MAP_SEARCH_HITS );

@protocol CClanMapSearcher <NSObject>

+ (void) performSearchForLocation:(NSString *)location inRegion:(CClanMapViewRegion)region;

@end

@interface CClanMapSearch : NSObject

+ (Class<CClanMapSearcher>) searcher;

@end