//
//  CClanTwitter.h
//  ConnachtClan
//
//  Created by Warren Gavin on 18/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanMacros.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_TWITTER_NEW_TWEETS );
CCLAN_DECLARE_NOTIFICATION( CCLAN_TWITTER_LOAD_FAILED );
CCLAN_DECLARE_NOTIFICATION( CCLAN_TWITTER_DISABLED );
CCLAN_DECLARE_NOTIFICATION( CCLAN_TWITTER_ENABLED );

@interface CClanTwitter : NSObject

//! Timeline is an array of CClanTweet objects
@property (strong, nonatomic, readonly) NSArray * const timeline;

//! The twitter feed is a singleton
+ (const CClanTwitter * const) instance;

//! Loads the timeline
- (void) loadTimeline;

//! This is called periodically to see if twitter has been turned on
//! or off by the user
- (void) pollTwitterStatus;

@end
