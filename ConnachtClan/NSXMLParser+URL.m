//
//  NSXMLParser+URL.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSXMLParser+URL.h"
#import "NSData+URL.h"

@implementation NSXMLParser (URL)

+ (NSXMLParser *) xmlParserWithContentsOfURL:(NSURL *)url
                                 cachePolicy:(NSURLRequestCachePolicy)cachePolicy
                             timeoutInterval:(NSTimeInterval)timeoutInterval
{
    NSData * const data = [NSData dataWithContentsOfURL:url cachePolicy:cachePolicy timeoutInterval:timeoutInterval];

    if ( !data ) {
        return nil;
    }
    
    return [[NSXMLParser alloc] initWithData:data];
}

@end
