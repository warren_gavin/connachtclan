//
//  CClanConstants.h
//  ConnachtClan
//
//  Created by Warren Gavin on 14/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#ifndef ConnachtClan_CClanConstants_h
#define ConnachtClan_CClanConstants_h

#define CCLAN_WEBVIEW_TEXT_FONT_SIZE    (12)
#define CCLAN_URL_REQUEST_TIMEOUT       (60)
#define CCLAN_URL_REQUEST_CACHE_POLICY  NSURLRequestUseProtocolCachePolicy

#define CCLAN_COLOUR                    [UIColor colorWithRed:(31.0/255.0) green:(127.0/255.0) blue:(26.0/255.0) alpha:1]
#define CCLAN_APP_URL                   @"http://www.connachtclan.com/wazza/app/"
#define CCLAN_DEFAULT_MAP_LOCATION      @"galway"

#endif
