//
//  NSArray+URLTimeout.h
//  ConnachtClan
//
//  Created by Warren Gavin on 09/11/12.
//
//

#import <Foundation/Foundation.h>

@interface NSArray (URLTimeout)

+ (NSArray *) arrayWithContentsOfURL:(NSURL *)url;

@end
