//
//  CClanTwitter.m
//  ConnachtClan
//
//  Created by Warren Gavin on 18/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanTwitter.h"
#import "CClanTweet.h"
#import "CClanErrorHandler.h"
#import "NSArray+Blocks.h"

#import <Social/SLRequest.h>
#import <Accounts/Accounts.h>
#import "STTwitter.h"

NSString * const CCLAN_TWITTER_NEW_TWEETS  = @"be.apokrupto.CClanTwitter.NewTweets";
NSString * const CCLAN_TWITTER_LOAD_FAILED = @"be.apokrupto.be.apokrupto.CClanTwitter.LoadFailed";
NSString * const CCLAN_TWITTER_DISABLED    = @"be.apokrupto.be.apokrupto.CClanTwitter.Disabled";
NSString * const CCLAN_TWITTER_ENABLED     = @"be.apokrupto.CClanTwitter.Enabled";

static NSString * const     L_APP_LIST_URL          = @"https://api.twitter.com/1.1/lists/statuses.json";
static const NSTimeInterval L_TIMELINE_REFRESH_RATE = 60.0;

// Singleton
static const CClanTwitter *g_instance;

@interface CClanTwitter ()

@property BOOL loading;
@property (strong, nonatomic) NSArray  * const timeline;
@property (strong, nonatomic) NSTimer  * const timer;
@property (strong, nonatomic) ACAccountStore * accountStore;
@property (strong, nonatomic) STTwitterAPI   * twitterAPI;

@end

@implementation CClanTwitter

+ (const CClanTwitter * const) instance
{
    if ( !g_instance ) {
        g_instance = [[CClanTwitter alloc] init];
        g_instance.loading = NO;
    }
    
    return g_instance;
}

- (void) resetPollingTimer
{
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:L_TIMELINE_REFRESH_RATE
                                                  target:self
                                                selector:@selector(loadTimeline)
                                                userInfo:nil
                                                 repeats:YES];
}

- (id) init
{
    self = [super init];
    
    if ( self ) {
        [self resetPollingTimer];
    }
    
    return self;
}

//! The timeline provided is sorted in ascending order, most recent tweet first
- (NSArray * const) timeline
{
    NSComparisonResult (^dateCompare)(CClanTweet *lhs, CClanTweet *rhs) = ^(CClanTweet *lhs, CClanTweet *rhs) {
        return [rhs.time compare:lhs.time];
    };
    
    return [_timeline sortedArrayUsingComparator:dateCompare];
}

- (void) addToTimeline:(const NSArray *)json
{
    self.loading = NO;

    if ( json.count ) {
        id (^createTweetFromJSON)( id element ) = ^( id element ) {
            return [[CClanTweet alloc] initWithJSONEntry:element];
        };
        
        const NSArray *timeline = [json convertWithModifierBlock:createTweetFromJSON];
        self.timeline = [timeline arrayByAddingObjectsFromArray:self.timeline];
    }

    dispatch_async( dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_NEW_TWEETS object:self];
    });
}

- (ACAccountStore *)accountStore
{
    if ( !_accountStore ) {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}

- (void) handleDownloadError
{
    dispatch_async( dispatch_get_main_queue(), ^{
        NSDictionary * const error_info     = @{ NSLocalizedDescriptionKey: @"Could not access Twitter feed" };
        NSError      * const parse_error    = [NSError errorWithDomain:[CClanErrorHandler domain]
                                                                  code:CCLAN_TWITTER_NO_FEED
                                                              userInfo:error_info];
        NSDictionary * const response_error = @{ @"error": parse_error };
        
        self.loading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_LOAD_FAILED
                                                            object:self
                                                          userInfo:response_error];
    });
}

- (void) twitterRequest
{
    [_twitterAPI getListsStatusesForSlug:@"AppTweets"
                              screenName:@"ConnachtClanApp"
                                 ownerID:nil
                                 sinceID:[self.timeline[0] identifier]
                                   maxID:nil
                                   count:@"20"
                         includeEntities:[NSNumber numberWithBool:YES]
                         includeRetweets:[NSNumber numberWithBool:YES]
                            successBlock:^(NSArray *statuses) {
                                [self addToTimeline:statuses];
                            } errorBlock:^(NSError *error) {
                                [self handleDownloadError];
                            }];
}

- (void) loadTimeline
{
    if ( self.loading ) {
        return;
    }
    
    self.loading = YES;
    
    dispatch_queue_t timeline_refresh = dispatch_queue_create("timeline refresh", NULL );
    
    dispatch_async( timeline_refresh, ^{
        [_twitterAPI verifyCredentialsWithSuccessBlock:^(NSString *username) {
            [self twitterRequest];
        } errorBlock:^(NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_DISABLED object:self];
            self.loading = NO;
        }];
    });

    // If we're about to fire a new loading request because of the time that is a waste
    // of resources, reset the timer
    [self resetPollingTimer];
}

- (void) pollTwitterStatus
{
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    NSArray *twitterAccounts = [self.accountStore accountsWithAccountType:accountType];
    if ( !twitterAccounts.count ) {
        // If the user has no twitter accounts on their device
        // then we do not ask for permission to use the account
        //
        // Instead we continue as normal and will use App-only
        // authentication when loading the timeline
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_ENABLED object:self];
        
        _twitterAPI = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"v9NO9pOn3PRVgBOAP1zQ"
                                                      consumerSecret:@"8YnF0y6ArYgs7InrwjVwzd77RBaSG77E8dqOCQg6P40"];
        [self loadTimeline];
        return;
    }

    // User has a twitter account. Ask for permission and either enable or
    // disable twitter depending on the user's choice.
    [self.accountStore requestAccessToAccountsWithType:accountType
                                               options:nil
                                            completion:^(BOOL granted, NSError *error) {
                                                if ( granted ) {
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_ENABLED object:self];
                                                    _twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:[twitterAccounts lastObject]];
                                                    [self loadTimeline];
                                                }
                                                else {
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_TWITTER_DISABLED object:self];
                                                    self.timeline = nil;
                                                    [self.timer invalidate];
                                                }
                                            }];
}

@end
