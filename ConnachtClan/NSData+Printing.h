//
//  NSData+Printing.h
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSData (Printing)

- (NSString *) hexString;

@end
