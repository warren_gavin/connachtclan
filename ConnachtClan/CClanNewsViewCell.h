//
//  CClanNewsViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 11/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanNewsViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel     *title;
@property (weak, nonatomic) IBOutlet UILabel     *description;

@end
