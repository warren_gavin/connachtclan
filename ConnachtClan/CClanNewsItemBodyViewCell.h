//
//  CClanNewsItemBodyViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/12/13.
//
//

#import "CClanTableView.h"

@interface CClanNewsItemBodyViewCell : CClanTableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
