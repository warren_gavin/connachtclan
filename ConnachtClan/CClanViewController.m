//
//  CClanViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 13/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanViewController.h"
#import "SWRevealViewController.h"

@implementation CClanViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

@end
