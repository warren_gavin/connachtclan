//
//  CClanMapViewGoogle.m
//  ConnachtClan
//
//  Created by Warren Gavin on 11/11/13.
//
//

#if defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanMapViewGoogle.h"
#import "CClanMapAnnotationGoogle.h"
#import <GoogleMaps/GoogleMaps.h>

@interface CClanMapViewGoogle ()

@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic, weak)   UIView     *infoWindow;

@end

@implementation CClanMapViewGoogle

@synthesize view         = _view;
@synthesize delegate     = _delegate;
@synthesize animatesDrop = _animatesDrop;

- (instancetype) init
{
    self = [super init];
    
    if ( self ) {
        _mapView = [GMSMapView mapWithFrame:CGRectZero camera:nil];
        _mapView.delegate = self;
        _mapView.accessibilityElementsHidden = NO;
        _view = _mapView;
    }
    
    return self;
}

- (void) removeAllMapAnnotations
{
    [_mapView clear];
}

- (void) addMapAnnotation:(id<CClanMapAnnotation>)annotation
{
    CClanMapAnnotationGoogle *marker = (CClanMapAnnotationGoogle *)annotation;
    
    if ( ![_mapView.markers containsObject:marker] ) {
        marker.appearAnimation = ( _animatesDrop ? kGMSMarkerAnimationPop : kGMSMarkerAnimationNone );
        marker.map = _mapView;
    }
}

- (void) setMapLocationRegion:(CClanMapViewRegion)region animated:(BOOL)animated
{
    CLLocationCoordinate2D  northWest = CLLocationCoordinate2DMake(region.latitude  + region.latitudeDelta/2.0,
                                                                   region.longitude + region.longitudeDelta/2.0);
    CLLocationCoordinate2D  southEast = CLLocationCoordinate2DMake(region.latitude  - region.latitudeDelta/2.0,
                                                                   region.longitude - region.longitudeDelta/2.0);
    GMSCoordinateBounds    *bounds    = [[GMSCoordinateBounds alloc] initWithCoordinate:northWest coordinate:southEast];

    if ( !_mapView.camera ) {
        _mapView.camera = [_mapView cameraForBounds:bounds insets:UIEdgeInsetsZero];
    }
    else {
        [_mapView moveCamera:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:UIEdgeInsetsZero]];
    }
}

- (void)setMapDisplayType:(CClanMapViewType)mapType
{
    switch (mapType) {
        case CClanMapViewTypeHybrid:
            _mapView.mapType = kGMSTypeHybrid;
            break;
        case CClanMapViewTypeSatelite:
            _mapView.mapType = kGMSTypeSatellite;
            break;
        case CClanMapViewTypeStandard:
        default:
            _mapView.mapType = kGMSTypeNormal;
            break;
    }
}

- (void) showUserLocation:(BOOL)showLocation
{
    _mapView.myLocationEnabled = showLocation;
}

#pragma mark - GMSMapViewDelegate
- (void) mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    [self.delegate mapView:self annotationTapped:(CClanMapAnnotationGoogle *)marker];

    // Hide info window after annotation details are displayed
    mapView.selectedMarker = nil;
}

@end

#endif
