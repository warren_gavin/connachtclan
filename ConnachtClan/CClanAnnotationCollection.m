//
//  CClanAnnotationCollection.m
//  ConnachtClan
//
//  Created by Warren Gavin on 29/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanAnnotationCollection.h"
#import "CClanDownloadedImagesCache.h"
#import "CClanErrorHandler.h"
#import "CClanAnnotation.h"
#import "CClanMapSearch.h"
#import "CClanMapView.h"

#import "NSArray+Blocks.h"
#import "NSArray+Plist.h"
#import "NSDictionary+Plist.h"

NSString * const CCLAN_ANNOTATION_COLLECTION_NEW_ANNOTATIONS = @"be.apokrupto.CClanAnnotationCollection.NewAnnotations";

static const CClanAnnotationCollection *g_instance = nil;

@interface CClanAnnotationCollection()

//! A dictionary of dictionaries
//!
//! key = "location"
//! value = dict( key = "latitude", value = "...", key = "longitude", value = "..." )
@property (nonatomic, strong) NSMutableDictionary *coordinates;

//! Annoation arrays keyed on locations, the arrays themselves are lazily instantiated
@property (nonatomic, strong) NSMutableDictionary *fixedAnnotations;
@property (nonatomic, strong) NSMutableDictionary *foundAnnotations;

//! Searchable locations
@property (nonatomic, strong) NSMutableArray *searchable;

@end

@implementation CClanAnnotationCollection

-(instancetype)init
{
    self = [super init];

    if ( self ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addAnnotations:)
                                                     name:CCLAN_NEW_MAP_SEARCH_HITS
                                                   object:nil];
        
        _coordinates        = [NSMutableDictionary dictionary];
        _fixedAnnotations   = [NSMutableDictionary dictionary];
        _foundAnnotations   = [NSMutableDictionary dictionary];
        _searchable         = [NSMutableArray array];

        [self loadMaps];
    }
    
    return self;
}

- (void) loadMaps
{
    dispatch_queue_t load_locations = dispatch_queue_create("Get map locations", NULL );
    dispatch_async( load_locations, ^{
        NSURL  *baseAppUrl = [NSURL URLWithString:CCLAN_APP_URL];
        NSURL  *url        = [NSURL URLWithString:@"locations.json" relativeToURL:baseAppUrl];
        NSData *jsonData   = [NSData dataWithContentsOfURL:url];
        id      json;
        
        if ( jsonData ) {
            json = [NSJSONSerialization JSONObjectWithData:jsonData
                                                   options:NSJSONReadingMutableLeaves
                                                     error:nil];
        }
        
        if ( ![json isKindOfClass:[NSArray class]] ) {
            NSString     * const message   = @"Could not download maps";
            NSDictionary * const errorInfo = @{ NSLocalizedDescriptionKey: message };
            NSError      * const error     = [NSError errorWithDomain:[CClanErrorHandler domain]
                                                                 code:CCLAN_MAP_NO_DOWNLOAD
                                                             userInfo:errorInfo];
            
            [CClanErrorHandler handleError:error withIndicator:nil];
            return;
        }
        
        NSArray *locations = json;
        for ( NSString *location in locations ) {
            [self loadAnnotationsAtLocation:[location lowercaseString]];
        }
    });
}

- (void) loadAnnotationsAtLocation:(NSString *)location
{
    dispatch_queue_t load_annotations = dispatch_queue_create("Get map annotations", NULL);
    dispatch_async( load_annotations, ^{
        NSURL  *baseAppUrl = [NSURL URLWithString:CCLAN_APP_URL];
        NSURL  *url        = [NSURL URLWithString:[NSString stringWithFormat:@"%@.json", location]
                                    relativeToURL:baseAppUrl];
        NSData *jsonData   = [NSData dataWithContentsOfURL:url];
        id      json;
        
        if ( jsonData ) {
            json = [NSJSONSerialization JSONObjectWithData:jsonData
                                                   options:NSJSONReadingMutableLeaves
                                                     error:nil];
        }
        
        if ( json ) {
            NSDictionary *mapInfo = json;
            NSDictionary *coords  = mapInfo[@"coordinates"];
            
            _coordinates[[coords[@"location"] lowercaseString]] = coords;
            
            NSArray *annotations = mapInfo[@"annotations"];
            _fixedAnnotations[location] = [annotations convertWithModifierBlock:^id(id element) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:element];
                
                dictionary[@"city"]    = coords[@"location"];
                dictionary[@"country"] = coords[@"country"];
                
                return [CClanAnnotation createAnnotation:dictionary];
            }];
            
            if ( !mapInfo[@"search"] ) {
                [_searchable addObject:location];
            }
            
            [self notifyNewAnnotations:location];
        }
        else {
            NSString     * const message   = [NSString stringWithFormat:@"Could not download %@ map", location];
            NSDictionary * const errorInfo = @{ NSLocalizedDescriptionKey: message };
            NSError      * const error     = [NSError errorWithDomain:[CClanErrorHandler domain]
                                                                 code:CCLAN_MAP_NO_LOCATION
                                                             userInfo:errorInfo];
            
            [CClanErrorHandler handleError:error withIndicator:nil];
        }
    });
}

- (NSArray *) locations
{
    return [_coordinates allValues];
}

- (const NSDictionary *)coordinatesForLocation:(NSString *)location
{
    NSDictionary *regionDetails = _coordinates[[location lowercaseString]];

    if ( !regionDetails ) {
        regionDetails = _coordinates[CCLAN_DEFAULT_MAP_LOCATION];
    }
    
    [self searchForAnnotations:location inRegion:regionDetails];

    return regionDetails;
}

- (void) searchForAnnotations:(NSString *)location inRegion:(NSDictionary *)regionDetails
{
    location = [location lowercaseString];
    // Return if we already have some found search hits, or if this location is not searchable
    if ( [_foundAnnotations[location] count] || ![_searchable containsObject:location] ) {
        [self notifyNewAnnotations:location];
        return;
    }
    
    CClanMapViewRegion region = CClanMapViewRegionMake([regionDetails[@"latitude"] floatValue],
                                                       [regionDetails[@"longitude"] floatValue],
                                                       [regionDetails[@"latitudeDelta"] floatValue],
                                                       [regionDetails[@"longitudeDelta"] floatValue]);
    
    [[CClanMapSearch searcher] performSearchForLocation:location inRegion:region];
}

- (void) notifyNewAnnotations:(NSString *)location
{
    dispatch_async( dispatch_get_main_queue(), ^{
        NSLog(@"Notifying %@", location);
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_ANNOTATION_COLLECTION_NEW_ANNOTATIONS
                                                            object:[location lowercaseString]];
    });
}

-(NSArray *)annotationsForLocation:(NSString *)location
{
    location = [location lowercaseString];
    NSLog(@"Requesting %@", location);
    NSMutableArray *allAnnotations = [NSMutableArray arrayWithArray:_fixedAnnotations[location]];
    
    if ( _foundAnnotations[location] ) {
        [allAnnotations addObjectsFromArray:_foundAnnotations[location]];
    }
    
    return [NSArray arrayWithArray:allAnnotations];
}

- (void) addAnnotations:(NSNotification *)notification
{
    NSDictionary       *userInfo    = notification.object;
    NSString           *location    = [userInfo[@"location"] lowercaseString];
    NSArray            *annotations = userInfo[@"results"];

    NSMutableArray *newAnnotations = [NSMutableArray arrayWithArray:annotations];
    [newAnnotations addObjectsFromArray:_foundAnnotations[location]];
    _foundAnnotations[location] = newAnnotations;

    [self notifyNewAnnotations:location];
}

+ (const CClanAnnotationCollection *) instance
{
    if ( nil == g_instance ) {
        g_instance = [[CClanAnnotationCollection alloc] init];
    }
    
    return g_instance;
}

- (void) dealloc
{
    g_instance = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
