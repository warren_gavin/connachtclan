//
//  EKEventStore+CClanEvent.m
//  ConnachtClan
//
//  Created by Warren Gavin on 17/11/12.
//
//

#import "EKEventStore+CClanEvent.h"
#import "EKEvent+CClanEvent.h"
#import "EKReminder+CClanEvent.h"

#import "CClanCalendarEvent.h"
#import "CClanErrorHandler.h"

@implementation EKEventStore (CClanEvent)

- (void) addToCalendarEvents:(const CClanCalendarEvent * const) event
                   withAlert:(BOOL)alert
                  controller:(id<UIAlertViewDelegate>) delegate
{
    EKEvent *ekevent  = [EKEvent eventWithEventStore:self];
    [ekevent setClanEvent:event calendar:[self defaultCalendarForNewEvents] alert:alert];
    
    NSPredicate *predicate = [self predicateForEventsWithStartDate:[ekevent.startDate dateByAddingTimeInterval:-60 * 60 * 24]
                                                           endDate:[ekevent.endDate   dateByAddingTimeInterval: 60 * 60 * 24]
                                                         calendars:nil];
    
    for ( EKEvent *match in [self eventsMatchingPredicate:predicate] ) {
        if ( [match.title isEqualToString:ekevent.title] ) {
            [self removeEvent:match span:EKSpanThisEvent commit:YES error:nil];
        }
    }
    
    NSError *error;
    [self saveEvent:ekevent span:EKSpanThisEvent commit:YES error:&error];
    
    if ( error ) {
        [CClanErrorHandler handleError:error withIndicator:nil];
    }
    else {
        dispatch_async( dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Event Saved"
                                                            message:nil
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Done", nil];
            
            [alert show];
        });
    }
}

- (void) addClanEvent:(const CClanCalendarEvent * const) event withAlert:(BOOL)alert controller:(id<UIAlertViewDelegate>)delegate
{
    if ( [self respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        [self requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            [CClanErrorHandler handleError:error withIndicator:nil];
            if ( granted ) {
                [self addToCalendarEvents:event withAlert:alert controller:delegate];
            }
        }];
    }
    else {
        dispatch_queue_t add_to_calendar = dispatch_queue_create("Add to Calendar", NULL );
        
        dispatch_async( add_to_calendar, ^{
            [self addToCalendarEvents:event withAlert:alert controller:delegate];
        });
    }
}

- (EKEvent *) storedClanEvent:(const CClanCalendarEvent * const) event
{
    EKEvent *ekevent  = [EKEvent eventWithEventStore:self];
    [ekevent setClanEvent:event calendar:[self defaultCalendarForNewEvents] alert:NO];
    
    NSPredicate *predicate = [self predicateForEventsWithStartDate:ekevent.startDate
                                                           endDate:ekevent.endDate
                                                         calendars:nil];
    
    for ( EKEvent *match in [self eventsMatchingPredicate:predicate] ) {
        if ( [match.title isEqualToString:ekevent.title] && [match.location isEqualToString:ekevent.location] ) {
            return match;
        }
    }
    
    return nil;
}

@end
