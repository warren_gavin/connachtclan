//
//  CClanCalendarEventWeatherView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarEventWeatherView.h"
#import "CClanWeatherImagesCache.h"

@implementation CClanCalendarEventWeatherView

- (void) setupWithWeatherReport:(const CClanWeatherReport *)weather
{
    [self.loading hideLoadingDisplay];
    
    self.description.text    = weather.current.description;
    self.details.text        = weather.current.details;
    self.temperature.text    = weather.current.temperature;
    self.condition.image     = weather.current.primary_icon;
    self.sub_condition.image = weather.current.secondary_icon;

    CGFloat original_height = self.forecast.frame.size.height;
    [self.forecast reloadData];
    
    CGFloat delta = self.forecast.frame.size.height - original_height;
    if ( delta < 0.0 )
        delta = 0.0;
    
    self.contentSize = CGSizeMake( self.frame.size.width, 
                                   self.frame.size.height + delta );
}

@end
