//
//  CClanErrorHandler.m
//  ConnachtClan
//
//  Created by Warren Gavin on 12/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanErrorHandler.h"

@implementation CClanErrorHandler

+ (void) handleError:(const NSError *)error withIndicator:(const UIActivityIndicatorView *) activity
{
    if ( error ) {
        dispatch_async( dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Ok", nil];
            
            [alert show];
        });
    }
}

+ (void) handleError:(const NSError *)error withLoadingView:(const CClanLoadingView *)loading
{
    if ( error ) {
        dispatch_async( dispatch_get_main_queue(), ^{
            [loading hideLoadingDisplay];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Ok", nil];
            
            [alert show];
        });
    }
}

+ (void) displayAlert:(NSString *)text
{
    dispatch_async( dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Whoops!"
                                                        message:text
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        
        [alert show];
    });
}

+ (NSString * const) domain
{
    return @"be.apokrupto.ConnnachtClan";
}

@end
