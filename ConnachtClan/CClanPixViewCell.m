//
//  CClanPixViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 28/01/13.
//
//

#import "CClanPixViewCell.h"
#import "CClanPix+Extensions.h"

#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"

@interface CClanPixViewCell ()

@property (strong, nonatomic) NSTimer * const timer;

@end

@implementation CClanPixViewCell

- (void) displayCellImage
{
    _imageView.image = [_asset thumbnailForSize:_imageView.frame.size];
    [_activity stopAnimating];
}

- (void) imageIsReady:(NSNotification *)notification
{
    [self displayCellImage];
    [self.timer invalidate];
}

- (void) checkImageIsReady
{
    if ( _asset.imageData ) {
        [self displayCellImage];
        [self.timer invalidate];
    }
}

- (void) setAsset:(CClanPix *)asset
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
    [notificationCenter addObserver:self
                           selector:@selector(imageIsReady:)
                               name:CCLAN_PIX_IMAGE_LOADED
                             object:asset];

    [_activity startAnimating];
    _imageView.image = nil;
    _asset = asset;
    
    if ( _asset.imageData ) {
        [self displayCellImage];
    }
    else {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(checkImageIsReady)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
