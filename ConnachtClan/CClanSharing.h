//
//  CClanSharing.h
//  ConnachtClan
//
//  Created by Warren Gavin on 05/01/13.
//
//

#import <Foundation/Foundation.h>
#import <Twitter/TWTweetComposeViewController.h>
#import <MessageUI/MessageUI.h>

typedef void (^CClanSharingCompletionHandler)(NSError *error);

@interface CClanSharing : NSObject <MFMailComposeViewControllerDelegate>

//! Sharing is available
@property (nonatomic, readonly) BOOL enabled;

//! Sharing via email is available
@property (nonatomic, readonly) BOOL email;

//! Sharing via twitter is available
@property (nonatomic, readonly) BOOL tweet;

//! Class is a singleton, there is no need for multiple
//! instances of a helper class with a set of bools
//! that never change
+ (const CClanSharing * const) instance;

//! A view controller for email
- (UINavigationController *) mailControllerWithSubject:(NSString * const)subject
                                            recipients:(NSArray  * const)recipients
                                           messageBody:(NSString * const)body
                                           attachement:(NSData   * const)attachment
                                    attachmentMimeType:(NSString * const)mime
                                    attachmentFilename:(NSString * const)filename
                                          onCompletion:(CClanSharingCompletionHandler)handler;

//! A view controller for tweeting
- (UIViewController *) twitterControllerWithText:(NSString * const)text
                                             url:(NSURL    * const)url
                                           image:(UIImage  * const)image
                                    onCompletion:(CClanSharingCompletionHandler)handler;

@end
