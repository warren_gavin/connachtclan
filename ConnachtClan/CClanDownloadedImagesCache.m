//
//  CClanDownloadedImagesCache.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanDownloadedImagesCache.h"

#import "UIImage+RoundedCorner.h"
#import "NSData+URL.h"

//static const CClanDownloadedImagesCache const *g_instance;

@interface CClanDownloadedImagesCache()

@property (nonatomic, strong) const NSMutableDictionary *cache;

- (CClanDownloadedImagesCache *) init;

@end

@implementation CClanDownloadedImagesCache

- (CClanDownloadedImagesCache *) init
{
    self = [super init];
    
    if ( self ) {
        self.cache = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

+ (UIImage *) imageAtURL:(NSString *)image_url
{
    NSData *imagedata = [NSData dataWithContentsOfURL:[NSURL URLWithString:image_url]
                                          cachePolicy:CCLAN_URL_REQUEST_CACHE_POLICY
                                      timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];

    UIImage *image;
    if ( imagedata ) {
        image = [UIImage imageWithData:imagedata scale:[[UIScreen mainScreen] scale]];
    }

    return image;
}

+ (UIImage *) imageAtURL:(NSString *)image_url
        withCornerRadius:(NSInteger)radius
               andBorder:(NSInteger)border
{
    CGFloat  scale   = [[UIScreen mainScreen] scale];
    UIImage *image   = [CClanDownloadedImagesCache imageAtURL:image_url];
    UIImage *rounded = [image roundedCornerImage:radius * scale borderSize:border * scale];
    
    // In some rare cases the rounding code fails, return the original image if that happens
    if ( !rounded ) {
        return image;
    }

    return rounded;
}

@end
