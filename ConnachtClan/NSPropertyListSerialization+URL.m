//
//  NSPropertyListSerialization+URL.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSPropertyListSerialization+URL.h"
#import "NSData+URL.h"

@implementation NSPropertyListSerialization (URL)

+ (id) plistWithContentsOfURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval
{
    id plist;
    
    NSData * const data = [NSData dataWithContentsOfURL:url cachePolicy:cachePolicy timeoutInterval:timeoutInterval];

    if ( data ) {
        NSPropertyListFormat  plist_format;
        NSError              *error;

        plist = [NSPropertyListSerialization propertyListWithData:data
                                                          options:0
                                                           format:&plist_format
                                                            error:&error];

        if ( error ) {
            plist = nil;
        }
    }
    
    return plist;
}

@end
