//
//  CClanWeatherImagesCache.h
//  ConnachtClan
//
//  Created by Warren Gavin on 27/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    CClanWeatherImagesCacheSunny,
    CClanWeatherImagesCacheFair,
    CClanWeatherImagesCacheFog,
    CClanWeatherImagesCacheCloudy,
    CClanWeatherImagesCacheMostlyCloudy,
    CClanWeatherImagesCacheLightShowers,
    CClanWeatherImagesCacheShowers,
    CClanWeatherImagesCacheWind,
    CClanWeatherImagesCacheRain,
    CClanWeatherImagesCacheLightRain,
    CClanWeatherImagesCacheThunder,
    CClanWeatherImagesCacheScatteredThunder,
    CClanWeatherImagesCacheSleetShowers,
    CClanWeatherImagesCacheSleet,
    CClanWeatherImagesCacheLightSnow,
    CClanWeatherImagesCacheSnow
};

typedef NSUInteger CClanWeatherImagesCacheWeatherCondition;

@interface CClanWeatherImagesCache : NSObject

+ (UIImage *) imageForCondition:(CClanWeatherImagesCacheWeatherCondition)condition isDaytime:(BOOL)isdaytime;

@end
