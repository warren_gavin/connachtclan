//
//  CClanTwitterTweetViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 30/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanTwitterTweetViewCell.h"
#import "CClanTweet.h"
#import "CClanDownloadedImagesCache.h"
#import "CClanErrorHandler.h"

#import "UIImage+Resize.h"

@interface CClanTwitterTweetViewCell ()
@property (nonatomic, weak) const CClanTweet             *tweet;
@property (nonatomic, weak) const CClanTwitterTweetTitle *title;
@end

@implementation CClanTwitterTweetTitle
@end

@implementation CClanTwitterTweetBody

- (void) setLogoImage:(const CClanTweet * const)tweet
{
    dispatch_queue_t load_image = dispatch_queue_create("load user logo", NULL );
    
    self.logo.image = nil;
    dispatch_async( load_image, ^{
        [tweet setLogoThumbnailSize:self.logo.frame.size.width];
        dispatch_async( dispatch_get_main_queue(), ^{
            self.logo.image = tweet.logo;
        });
    });
}

- (void) setTweet:(const CClanTweet *const)tweet
{
    [self setLogoImage:tweet];

    self.tweetTextView.attributedText = tweet.tweet;
    self.tweetTextView.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor blueColor] };
    [self.tweetTextView sizeToFit];
    self.tweetTextView.textContainer.lineFragmentPadding = 0;
    self.tweetTextView.textContainerInset = UIEdgeInsetsZero;
    self.tweetTextView.scrollEnabled = NO;
}

@end

@implementation CClanTwitterTweetRetweetDetails

@end

@implementation CClanTwitterTweetViewCell

- (void) resetDisplayRelativeTo:(NSDate *const)time
{
    _title.interval.text = [self.tweet tweetTimeRelativeTo:time];
}

- (void) displayTweet:(const CClanTweet * const)tweet
{
    self.tweet = tweet;
    [_tableView reloadData];
}

- (CClanTwitterTweetTitle *) titleCell
{
    CClanTwitterTweetTitle *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tweet title"];
    
    cell.interval.text = [_tweet tweetTimeRelativeTo:[NSDate date]];
    cell.user.text = [_tweet.twitterName stringByAppendingFormat:@" @%@", _tweet.screenName];

    // Keep a record of this so we can update the interval without performing
    // unnecessary lookups
    _title = cell;
    
    return cell;
}

- (CClanTwitterTweetBody *) bodyCell
{
    CClanTwitterTweetBody *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tweet body"];

    [cell setTweet:_tweet];
    
    return cell;
}

- (CClanTwitterTweetRetweetDetails *) retweetCell
{
    CClanTwitterTweetRetweetDetails *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tweet retweet"];
    
    cell.retweetedBy.text = [NSString stringWithFormat:@"@%@", _tweet.retweetedBy];
    
    return cell;
}

- (CGFloat) cellHeightForTweet:(const CClanTweet *const)tweet
{
    self.tweet = tweet;
    
    CGFloat height = [[self titleCell] height] + [[self bodyCell] height];
    
    if ( _tweet.retweetedBy ) {
        height += [[self retweetCell] height];
    }

    return height;
}

- (CClanTableViewCell *) cellForRow:(NSUInteger)row
{
    SEL cells[] = { @selector(titleCell), @selector(bodyCell), @selector(retweetCell) };
    
    CClanTableViewCell *cell;
    SuppressPerformSelectorLeakWarning( cell = [self performSelector:cells[row]] );
    //    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self cellForRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_tweet.retweetedBy ? 3 : 2);
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self cellForRow:indexPath.row] height];
}

@end
