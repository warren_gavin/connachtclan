//
//  CClanWeatherDetailsCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 19/01/13.
//
//

#import <UIKit/UIKit.h>

@interface CClanWeatherDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *condition;
@property (weak, nonatomic) IBOutlet UIImageView *sub_condition;
@property (weak, nonatomic) IBOutlet UILabel     *temperature;
@property (weak, nonatomic) IBOutlet UILabel     *description;
@property (weak, nonatomic) IBOutlet UILabel     *details;
@property (weak, nonatomic) IBOutlet UILabel     *location;

@end
