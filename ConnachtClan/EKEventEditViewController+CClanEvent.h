//
//  EKEventEditViewController+CClanEvent.h
//  ConnachtClan
//
//  Created by Warren Gavin on 04/01/13.
//
//

#import <EventKitUI/EventKitUI.h>

@interface EKEventEditViewController (CClanEvent)

+ (id) editorForEvent:(EKEvent * const)event store:(EKEventStore * const)store delegate:(id<EKEventEditViewDelegate>)delegate;

@end
