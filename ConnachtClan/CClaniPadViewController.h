//
//  CClaniPadViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 18/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"

@interface CClaniPadViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *clanNewsTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *clanNewsActivity;

@property (weak, nonatomic) IBOutlet UITableView *clanCalendarTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *clanCalendarActivity;

@property (weak, nonatomic) IBOutlet UITableView *clanTwitterTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *clanTwitterActivity;

@property (weak, nonatomic) IBOutlet AQGridView *clanVideoGrid;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *clanVideoActivity;

@end
