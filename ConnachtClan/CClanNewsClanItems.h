//
//  CClanNewsClanItems.h
//  ConnachtClan
//
//  Created by Warren Gavin on 11/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CClanNewsClanItems : NSObject

@property (strong, nonatomic) NSString * const description;
@property (strong, nonatomic) UIImage  * const image;

- (void) parseDescription:(const NSString * const) description;

@end
