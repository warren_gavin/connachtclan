//
//  CClanNews.h
//  ConnachtClan
//
//  Created by Warren Gavin on 08/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanMacros.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_NEWS_NOTIFY_NEW_ITEMS );

@interface CClanNews : NSObject

+ (const CClanNews * const) instance;

- (void) loadNews;

- (const NSArray * const) connachtClanNews;

@end
