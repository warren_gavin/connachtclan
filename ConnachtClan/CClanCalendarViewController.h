//
//  CClanCalendarViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanViewController.h"
#import "CClanLoadingView.h"

//! Infinite table containing calendar months
@interface CClanCalendarViewController : CClanViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet CClanLoadingView *loading;
@property (nonatomic, weak) IBOutlet UITableView      *table;

@end
