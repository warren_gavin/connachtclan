//
//  CClanMapSearchResult.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import <Foundation/Foundation.h>

@class CClanAnnotation;

@protocol CClanMapSearchResult <NSObject>

+(id<CClanMapSearchResult>) resultFromSearchResult:(NSDictionary *)searchResult userInfo:(NSDictionary *)userInfo;

- (CClanAnnotation *) annotation;

@end
