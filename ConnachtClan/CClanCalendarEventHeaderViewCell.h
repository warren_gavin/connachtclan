//
//  CClanEventHeaderViewCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 05/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanCalendarEventHeaderViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *month;

@end
