//
//  CClanHtmlDownloader.m
//  ConnachtClan
//
//  Created by Warren Gavin on 24/09/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanHtmlDownloader.h"
#import "CClanArticle.h"
#import "CClanDownloadedImagesCache.h"

#import "NSString+URL.h"

#if 0
# define L_DEBUG_XML(str) NSLog str
#else
# define L_DEBUG_XML(str)
#endif

static NSString *L_WEBVIEW_BASE_URL = @"http://www.connachtclan.com/";
static BOOL article_image;

@interface CClanHtmlDownloader () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableString    *headline;
@property (nonatomic, strong) NSMutableString    *body;
@property (nonatomic, strong) const CClanArticle *article;

- (BOOL) ignoreElement:(const NSString * const)element orClass:(const NSString * const)class;
- (BOOL) ignoreAttributes:(const NSString * const)element;

@end

@implementation CClanHtmlDownloader

- (id) initWithUrl:(NSString * const)url
{
    self = [super init];

    if ( self ) {
        self.url = url;
    }

    return self;
}

- (BOOL) ignoreElement:(const NSString *const)element orClass:(const NSString *const)class
{
    static const NSArray *ignore;
    
    if ( !ignore ) {
        ignore = @[ @"span", @"meta", @"fb:like", @"cmp_twitter_container", @"twitter-share-button" ];
    }
    
    return [ignore containsObject:element] || [ignore containsObject:class];
}

- (BOOL) ignoreAttributes:(const NSString *const)element
{
    static const NSArray *ignore;
    
    if ( !ignore ) {
        ignore = @[ @"p", @"table", @"tr", @"td", @"div" ];
    }
    
    return [ignore containsObject:element];
}

- (NSData *)xmlDataToParse: (NSString * const)url
{
    NSString *html = [NSString stringWithContentsOfURL:[NSURL URLWithString:url]
                                              encoding:NSUTF8StringEncoding
                                           cachePolicy:CCLAN_URL_REQUEST_CACHE_POLICY
                                       timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];
    
    if ( html ) {
        NSRange articlestart = [html rangeOfString:@"<div class=\"rt-article\">"];
        NSRange articleend   = [html rangeOfString:@"<div class=\"rt-box-bottom\">"];
        
        NSRange range = NSMakeRange(articlestart.location, articleend.location - articlestart.location);
        
        if ( NSNotFound == range.location ) {
            html = nil;
            [self emptyDownload];
        }
        else {
            html = [html substringWithRange:range];
            
            html = [html stringByReplacingOccurrencesOfString:@"\u00a0" withString:@" "];
            html = [html stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
            html = [html stringByReplacingOccurrencesOfString:@"<div class=\"clear\"></div>" withString:@""];
        }
    }

    return [html dataUsingEncoding:NSUTF8StringEncoding];
}

- (void) emptyDownload
{
    self.article = [[CClanArticle alloc] init];
    self.article.headline = @"No info";
    self.article.body = @"There are currently no details for this event";
    
    dispatch_async( dispatch_get_main_queue(), ^{
        [self.delegate htmlDownloader:self didFinishDownloadingArticle:self.article];
        self.article = nil;
    });
}

- (void) download
{
    dispatch_queue_t event_loading = dispatch_queue_create("loading event details", NULL );
    
    dispatch_async( event_loading, ^{
        NSData *xml = [self xmlDataToParse:self.url];
        
        if ( xml ) {
            const NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithData:xml];
            xmlparser.delegate = self;
            
            self.body = [NSMutableString stringWithFormat:@"<style type=\"text/css\">body{font-size: %dpx; font-family:Verdana, Arial, Helvetica, sans-serif} .title{text-align:center} table{font-size:%dpx}></style>", CCLAN_WEBVIEW_TEXT_FONT_SIZE, CCLAN_WEBVIEW_TEXT_FONT_SIZE];

            article_image = NO;
            [xmlparser parse];
        }
        else {
            [self emptyDownload];
        }
    });
}

#pragma mark - XML parsing
- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
     attributes:(NSDictionary *)attributeDict
{
    NSMutableString *element = [NSMutableString stringWithFormat:@"<%@", elementName];
    
    if ( ![self ignoreAttributes:elementName] ) {
        for (NSString *key in attributeDict.allKeys ) {
            [element appendFormat:@" %@=\"%@\"", key, attributeDict[key]];
        }
    }
    [element appendString:@">"];

    L_DEBUG_XML((@"%@", element));
    NSString *class = attributeDict[@"class"];
    if ( [self ignoreElement:elementName orClass:class] ) {
        return;
    }

    if ( [elementName isEqualToString:@"div"] ) {
        if ( [class isEqualToString:@"rt-article"] ) {
            self.article = [[CClanArticle alloc] init];
        }
    }
    
    if ( self.article ) {
        if ( [elementName isEqualToString:@"h1"] && [@"title" isEqualToString:class] ) {
            self.headline = [NSMutableString string];
        }
        else if ( [elementName isEqualToString:@"img"] && !self.article.image ) {
            article_image = YES;
            self.article.image = [CClanDownloadedImagesCache imageAtURL:[L_WEBVIEW_BASE_URL stringByAppendingString:attributeDict[@"src"]]];
        }
        else {
            [self.body appendString:element];
        }
    }
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    L_DEBUG_XML((@"%@", string));
    if ( [string isEqualToString:@"Fatal error"] ) {
        [parser abortParsing];
        [self emptyDownload];
    }

    if ( [string isEqualToString:@"Tweet"] ) {
        return;
    }
    
    if ( self.article ) {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]];
        
        if ( self.headline && string.length ) {
            [self.headline appendString:string];
        }
        else if ( string.length ) {
            [self.body appendString:string];
        }
    }
}

- (void) parser:(NSXMLParser *)parser
  didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    L_DEBUG_XML((@"</%@>", elementName));
    if ( [self ignoreElement:elementName orClass:nil] || (article_image && [elementName isEqualToString:@"img"]) ) {
        return;
    }

    if ( self.article ) {
        if ( !self.headline ) {
            // if we are still creating the headline don't add end tags to the body
            [self.body appendFormat:@"</%@>", elementName];
        }

        if ( [elementName isEqualToString:@"h1"] && self.headline ) {
            self.article.headline = [self.headline stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            self.headline = nil;
        }
    }
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
    dispatch_async( dispatch_get_main_queue(), ^{
        NSString *body = [self.body stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        body = [body stringByReplacingOccurrencesOfString:@"<p></p>" withString:@"<p/>"];
        body = [body stringByReplacingOccurrencesOfString:@"<br></br>" withString:@"<p/>"];
        body = [body stringByReplacingOccurrencesOfString:@"<br />" withString:@"<p/>"];
        body = [body stringByReplacingOccurrencesOfString:@"<br/>" withString:@"<p/>"];
        body = [body stringByReplacingOccurrencesOfString:@"<p/><p/>" withString:@"<p/>"];
        
        self.article.body = body;
        [self.delegate htmlDownloader:self didFinishDownloadingArticle:self.article];
        self.article = nil;
    });
}

@end
