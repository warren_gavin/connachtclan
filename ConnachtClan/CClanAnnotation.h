//
//  CClanAnnotation.h
//  ConnachtClan
//
//  Created by Warren Gavin on 30/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanMacros.h"
#import "CClanMapView.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_ANNOTATION_NOTIFY_NEW_ANNOTATIONS );

@class CClanAnnotation;
@class CClanExtendedAnnotation;

@protocol CClanAnnotationDelegate <NSObject>

- (void) annotation:(CClanAnnotation * const) annotation didDownloadCalloutImage:(UIImage * const) image;

@optional
- (void) annotation:(CClanExtendedAnnotation * const) annotation didDownloadAnnotationImage:(UIImage * const) image;

@end

//! Map annotation
@interface CClanAnnotation : NSObject

@property (nonatomic, weak) id<CClanAnnotationDelegate> delegate;

//! Name of the annotation, will be the MKAnnotation title value
@property (nonatomic, strong) NSString *name;

//! needed for the coordinate property of MKAnnotation protocol
@property (nonatomic) CLLocationCoordinate2D location;

//! A collection of text values
@property (nonatomic, strong) NSArray *types;

//! Image to display on the annotation details view
@property (nonatomic, strong) UIImage  *image;

//! Location's address
@property (nonatomic, strong) NSString *address;

//! City location
@property (nonatomic, strong) NSString *city;

//! Location country
@property (nonatomic, strong) NSString *country;

//! Location's phone number
@property (nonatomic, strong) NSString *phone;

//! Locations' site
@property (nonatomic, strong) NSString *website;

//! Flag for fixed/found annotations
@property (nonatomic) BOOL fixed;

//! Annotation info, e.g. Ground guide
@property (nonatomic, strong) NSDictionary *info;

//! Factory method
+ (CClanAnnotation *) createAnnotation:(NSDictionary *)details;

//! reuse identifier for the annotation
+ (NSString        *) identifier;

//! Description of annotation types in printable form such
//! as "Bar & Restaurant"
- (NSString *) details;

//! Returns YES if the annotation should be displayed
//! according to the map settings
- (BOOL) shouldDisplay:(NSSet *)showable;

@end

//! And extended annotation is a map annotation that has a different
//! appearance on the map. In this case it's a special image that 
//! is displayed instead of a pin.
//!
//! While annotations can be switched on and off via the settings an
//! extended annotation is always displayed
@interface CClanExtendedAnnotation : CClanAnnotation

//! The special image to display on the map instead of a pin
@property (nonatomic, strong) UIImage * annotationImage;

//! Location of the special image
@property (nonatomic, strong) NSString * annotationImagePath;

- (instancetype) initWithAnnotationImage:(UIImage *)image;

- (void) getAnnotationExtensions;

@end
