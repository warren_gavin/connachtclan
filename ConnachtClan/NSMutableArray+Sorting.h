//
//  NSMutableArray+Sorting.h
//  ConnachtClan
//
//  Created by Warren Gavin on 23/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Sorting)

- (void) moveElementAtIndex:(NSUInteger)source toIndex:(NSUInteger) destination;

@end
