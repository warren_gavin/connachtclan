//
//  ABNewPersonViewController+CClanAnnotation.h
//  ConnachtClan
//
//  Created by Warren Gavin on 20/11/12.
//
//

#import <AddressBookUI/AddressBookUI.h>

@class CClanAnnotation;

@interface ABNewPersonViewController (CClanAnnotation)

+ (UINavigationController *) controllerForCClanAnnotation:(const CClanAnnotation * const) annotation
                                                 delegate:(id<ABNewPersonViewControllerDelegate>) delegate;

@end
