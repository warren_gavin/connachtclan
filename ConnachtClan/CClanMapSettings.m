//
//  CClanMapSettings.m
//  ConnachtClan
//
//  Created by Warren Gavin on 03/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanMapSettings.h"
#import "CClanMapView.h"
#import "CClanConstants.h"
#import "CClanAnnotation.h"

static NSString * const L_MAPTYPE_KEY         = @"be.Apokrupto.ConnachtClan.CClanMapSettings.maptype";
static NSString * const L_LOCATION_KEY        = @"be.Apokrupto.ConnachtClan.CClanMapSettings.location";
static NSString * const L_SHOWBARS_KEY        = @"be.Apokrupto.ConnachtClan.CClanMapSettings.showbars";
static NSString * const L_SHOWCAFES_KEY       = @"be.Apokrupto.ConnachtClan.CClanMapSettings.showcafes";
static NSString * const L_SHOWRESTAURANTS_KEY = @"be.Apokrupto.ConnachtClan.CClanMapSettings.showrestaurants";
static NSString * const L_SHOWACCOM_KEY       = @"be.Apokrupto.ConnachtClan.CClanMapSettings.showaccommodation";
static NSString * const L_PERMANENT_SET       = @"be.Apokrupto.ConnachtClan.CClanMapSettings.permanent";

@interface CClanMapSettings()

@property (nonatomic, strong) NSArray *permanent;

- (void) saveSettings;

@end

@implementation CClanMapSettings

+ (CClanMapSettings *) instance
{
    static CClanMapSettings *g_instance = nil;
    
    if ( !g_instance ) {
        g_instance = [[CClanMapSettings alloc] init];
    }
    
    return g_instance;
}

- (CClanMapSettings *) init
{
    self = [super init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // Default values if there are no saved settings.
    _maptype  = CClanMapViewTypeStandard;
    _location = CCLAN_DEFAULT_MAP_LOCATION;
    
    _showbars          = YES;
    _showcafes         = YES;
    _showrestaurants   = YES;
    _showaccommodation = YES;
    
    
    // Override defaults if there are saved settings
    // Just check for one key, if one is there they're all there.
    if ( [defaults objectForKey:L_MAPTYPE_KEY] ) {
        _maptype = (CClanMapViewType)[defaults integerForKey:L_MAPTYPE_KEY];
        _location = [defaults stringForKey:L_LOCATION_KEY];
        if ( !_location ) {
            _location = CCLAN_DEFAULT_MAP_LOCATION;
        }
        
        _showbars          = [defaults boolForKey:L_SHOWBARS_KEY];
        _showcafes         = [defaults boolForKey:L_SHOWCAFES_KEY];
        _showrestaurants   = [defaults boolForKey:L_SHOWRESTAURANTS_KEY];
        _showaccommodation = [defaults boolForKey:L_SHOWACCOM_KEY];
        _permanent         = [defaults objectForKey:L_PERMANENT_SET];
    }
    
    return self;
}

- (void) setMaptype:(CClanMapViewType)maptype
{
    _maptype = maptype;
    [self saveSettings];
}

- (void) setLocation:(NSString *)location
{
    _location = location;
    [self saveSettings];
}

- (void) setShowbars:(BOOL) on
{
    _showbars = on;
    [self saveSettings];
}

- (void) setShowcafes:(BOOL) on
{
    _showcafes = on;
    [self saveSettings];
}

- (void) setShowrestaurants:(BOOL) on
{
    _showrestaurants = on;
    [self saveSettings];
}

- (void) setShowaccommodation:(BOOL) on
{
    _showaccommodation = on;
    [self saveSettings];
}

- (void) setPermanent:(NSArray *)permanent
{
    _permanent = permanent;
    [self saveSettings];
}

- (NSSet *)showable
{
    NSMutableSet *enabled = [NSMutableSet setWithCapacity:4];
    
    if ( self.showbars )
        [enabled addObject:@"Bar"];
    
    if ( self.showcafes )
        [enabled addObject:@"Cafe"];
    
    if ( self.showrestaurants )
        [enabled addObject:@"Restaurant"];
    
    if ( self.showaccommodation )
        [enabled addObject:@"Accommodation"];
    
    return [NSSet setWithSet:enabled];
}

- (BOOL) isPermanent:(const CClanAnnotation *const)location
{
    return [_permanent containsObject:location.description];
}

- (void) addPermanent:(const CClanAnnotation *const)location
{
    NSString * const description = location.description;

    if ( ![self.permanent containsObject:description] ) {
        NSMutableArray *newPermanent = [NSMutableArray arrayWithArray:self.permanent];
        [newPermanent addObject:description];
        
        self.permanent = newPermanent;
    }
}

- (void) removePermanent:(const CClanAnnotation *const)location
{
    NSString * const description = location.description;

    if ( [self.permanent containsObject:description] ) {
        NSMutableArray *newPermanent = [NSMutableArray arrayWithArray:self.permanent];
        [newPermanent removeObject:description];
        
        self.permanent = newPermanent;
    }
}

- (void) saveSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setInteger:self.maptype forKey:L_MAPTYPE_KEY];
    [defaults setObject:_location forKey:L_LOCATION_KEY];
    
    [defaults setBool:self.showbars          forKey:L_SHOWBARS_KEY];
    [defaults setBool:self.showcafes         forKey:L_SHOWCAFES_KEY];
    [defaults setBool:self.showrestaurants   forKey:L_SHOWRESTAURANTS_KEY];
    [defaults setBool:self.showaccommodation forKey:L_SHOWACCOM_KEY];
    [defaults setObject:self.permanent       forKey:L_PERMANENT_SET];
    
    [defaults synchronize];
}

@end
