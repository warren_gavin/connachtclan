//
//  CClanCalendarWeekViewCell.m
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarWeekViewCell.h"
#import "CClanCalendarDayView.h"

@implementation CClanCalendarWeekViewCell

- (void) setDaysInWeek:(NSInteger)index forMonth:(NSDate *)month inCalendar:(NSCalendar *)calendar
{
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit | NSYearCalendarUnit
                                               fromDate:month];
    components.weekOfMonth = index;

    for (int i = 1; i <= 7; ++i ) {
        components.weekday = i + [calendar firstWeekday] - 1;

        CClanCalendarDayView *dayview = (CClanCalendarDayView *)[self viewWithTag:100 * i];
        [dayview setDetailsForDate:[calendar dateFromComponents:components]
                         ifInMonth:month
                      withCalendar:calendar];
    }
}

@end
