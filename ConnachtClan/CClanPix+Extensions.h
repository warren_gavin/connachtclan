//
//  CClanPix+Extensions.h
//  ConnachtClan
//
//  Created by Warren Gavin on 16/02/13.
//
//

#import "CClanPix.h"
#import "CClanMacros.h"

CCLAN_DECLARE_NOTIFICATION( CCLAN_PIX_IMAGE_LOADED );

@interface CClanPix (Extensions)

- (UIImage *) thumbnailForSize:(CGSize) size;

+ (NSURL *) urlForClanPixLocation;

+ (NSURL *) urlForClanPixDatabase;

+ (CClanPix *) clanPixWithImage:(UIImage * const)image forManagedObjectContext:(NSManagedObjectContext *)moc;

@end
