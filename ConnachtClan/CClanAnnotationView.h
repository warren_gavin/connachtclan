//
//  CClanAnnotationView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 31/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#if !defined(CCLAN_MAP_USE_GOOGLE)
#import <MapKit/MapKit.h>

@class CClanMapAnnotationApple;

//! Map annoation view factory for specific annotations
@interface CClanAnnotationViewFactory : NSObject 

//! Facotory method. Will return either a ::CClanAnnotationView or ::CClanExtendedAnnotationView
//! depending on the annotation the view represents
+ (MKAnnotationView  *) viewForAnnotation:(CClanMapAnnotationApple *) annotation;

@end

//! General pin annotation view
@interface CClanAnnotationView : MKPinAnnotationView
@end

//! An extended annotation view with an image as the annotation
@interface CClanExtendedAnnotationView : MKAnnotationView
@end

#endif