//
//  CClanTableViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 20/01/13.
//
//

#import "CClanTableViewController.h"
#import "CClanBundle.h"

@implementation CClanTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *background = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"background.png"]];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:background];
    self.tableView.backgroundView.contentMode = UIViewContentModeBottom;
}

@end
