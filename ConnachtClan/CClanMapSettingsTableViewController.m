//
//  CClanMapSettingsTableViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 02/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanMapSettingsTableViewController.h"
#import "CClanMapView.h"
#import "CClanBundle.h"

@interface CClanMapSettingsTableViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *barscontroller;
@property (weak, nonatomic) IBOutlet UISwitch *restaurantcontroller;
@property (weak, nonatomic) IBOutlet UISwitch *cafecontroller;
@property (weak, nonatomic) IBOutlet UISwitch *accommodationcontroller;

@end

@implementation CClanMapSettingsTableViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view delegate

- (IBAction)setSateliteView:(const UISwitch * const) sender
{
    if ( sender.on ) {
        [self.delegate setMapViewSatellite];
        self.hybridcontroller.enabled = YES;
    }
    else {
        [self.delegate setMapViewStandard];
        [self.hybridcontroller setOn:NO animated:YES];
        self.hybridcontroller.enabled = NO;
    }
}

- (IBAction)setHybrid:(const UISwitch * const)sender
{
    if ( sender.on )
        [self.delegate setMapViewHybrid];
    else
        [self.delegate setMapViewSatellite];
}


- (IBAction)setBars:(const UISwitch * const)sender
{
    [self.delegate showBars:sender.on];
}

- (IBAction)setRestaurants:(const UISwitch * const)sender
{
    [self.delegate showRestaurants:sender.on];
}

- (IBAction)setCafes:(const UISwitch * const)sender
{
    [self.delegate showCafes:sender.on];
}

- (IBAction)setAccommodation:(const UISwitch * const)sender
{
    [self.delegate showAccommodation:sender.on];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if ( CClanMapViewTypeStandard == self.settings.maptype  ) {
        self.satellitecontroller.on   = NO;
        self.hybridcontroller.on      = NO;
        self.hybridcontroller.enabled = NO;
    }
    else {
        self.satellitecontroller.on   = YES;
        self.hybridcontroller.on      = NO;
        self.hybridcontroller.enabled = YES;
        
        if ( CClanMapViewTypeHybrid == self.settings.maptype )
            self.hybridcontroller.on = YES;
    }
    
    self.barscontroller.on = self.settings.showbars;
    self.cafecontroller.on = self.settings.showcafes;
    self.restaurantcontroller.on = self.settings.showrestaurants;
    self.accommodationcontroller.on = self.settings.showaccommodation;
}

- (void)viewDidUnload
{
    self.satellitecontroller = nil;
    self.hybridcontroller = nil;

    [self setBarscontroller:nil];
    [self setRestaurantcontroller:nil];
    [self setCafecontroller:nil];
    [self setAccommodationcontroller:nil];
    [super viewDidUnload];
}
@end
