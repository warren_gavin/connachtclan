//
//  NSDictionary+Plist.h
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Plist)

+ (NSDictionary *) dictionaryFromPlistAtURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval;

@end
