//
//  CClanCalendarEvent.h
//  ConnachtClan
//
//  Created by Warren Gavin on 21/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CClanMatchDetails.h"

//! An event that corresponds to a given date
@interface CClanCalendarEvent : NSObject

// @@TODO Check ownership 
@property (nonatomic, strong) NSDate   *date;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *headline;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSArray  *broadcasters;
@property (nonatomic, strong) NSString *competition;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSArray  *coordinates;
@property (nonatomic, strong) CClanMatchDetails *matchDetails;

//! Factory method to create an event with given data
+ (const CClanCalendarEvent *) calendarEventFromData: (NSDictionary *)data;

@end
