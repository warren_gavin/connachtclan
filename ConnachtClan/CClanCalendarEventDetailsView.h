//
//  CClanCalendarEventDetails.h
//  ConnachtClan
//
//  Created by Warren Gavin on 22/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CClanLoadingView.h"

@class CClanCalendarEvent;

//! Modal view of an event
@interface CClanCalendarEventDetailsView : UIScrollView

@property (nonatomic, weak) IBOutlet UILabel *headline;

@property (nonatomic, weak) IBOutlet UIImageView *imageview;

@property (nonatomic, weak) IBOutlet UIImageView *eventBackground;

//! The event details are displayed in a scrollable web view
@property (nonatomic, weak) IBOutlet UIWebView *webview;

@property (nonatomic, weak) IBOutlet CClanLoadingView *loading;

//! Set the view with the information in a given events URL
- (void) setupWithDetails:(NSString * const)url;

@end
