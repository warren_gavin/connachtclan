//
//  CClanMatchWeatherCell.h
//  ConnachtClan
//
//  Created by Warren Gavin on 28/09/13.
//
//

#import <UIKit/UIKit.h>
#import "CClanWeatherReport.h"

@interface CClanMatchWeatherCell : UITableViewCell

//! Graphical representation of the forecast
@property (nonatomic, weak) IBOutlet UIImageView *image;

//! An additional image for the forecast to lay over the
//! main image, such as an indicator of an extra detail. E.g. Wind
@property (weak, nonatomic) IBOutlet UIImageView *detail;

//! The day of the forecast
@property (nonatomic, weak) IBOutlet UILabel     *day;

//! The forecast text
@property (nonatomic, weak) IBOutlet UILabel     *conditions;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activity;

+(CClanMatchWeatherCell *) cellForTable:(UITableView *)tableView withWeather:(const CClanWeatherReport * const)weather atIndex:(NSInteger)index;

@end
