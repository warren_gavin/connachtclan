//
//  NSData+Hash.h
//  ConnachtClan
//
//  Created by Warren Gavin on 31/01/13.
//
//

#import <Foundation/Foundation.h>

@interface NSData (Hash)

- (NSData *) sha1;

@end
