//
//  UIImage+Mask.m
//  ConnachtClan
//
//  Created by Warren Gavin on 02/01/14.
//
//

#import "UIImage+Mask.h"

@implementation UIImage (Mask)

- (UIImage *) maskedImageForColour:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);

    UIGraphicsBeginImageContextWithOptions(rect.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self drawInRect:rect];

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextSetBlendMode(context, kCGBlendModeSourceAtop);
    CGContextFillRect(context, rect);

    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return result;
}

@end
