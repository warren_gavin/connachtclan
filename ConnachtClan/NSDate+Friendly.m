//
//  NSDate+Friendly.m
//  ConnachtClan
//
//  Created by Warren Gavin on 09/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "NSDate+Friendly.h"

@implementation NSDate (Friendly)

- (NSString * const) friendlyDayOfWeek
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDoesRelativeDateFormatting:YES];
    
    NSString *day = [formatter stringFromDate:self];
    if ( ![day isEqualToString:@"Today"] && ![day isEqualToString:@"Tomorrow"] ) {
        [formatter setDateFormat:@"EEEE"];
        day = [formatter stringFromDate:self];
    }
    
    return day;
}

@end
