//
//  CClanAppDelegate.m
//  ConnachtClan
//
//  Created by Warren Gavin on 13/03/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanAppDelegate.h"
#import "CClanNews.h"
#import "CClanCalendar.h"
#import "CClanTwitter.h"
#import "CClanTwitterFeedViewController.h"
#import "CClanAnnotationCollection.h"
#import "CClanPictures.h"
#import "CClanBundle.h"

#if defined(CCLAN_MAP_USE_GOOGLE)
# import "CClanGoogleAPI.h"
# import <GoogleMaps/GoogleMaps.h>
#endif

@interface CClanAppDelegate () <UINavigationControllerDelegate>

@property (nonatomic) BOOL twitterEnabled;

@end

@implementation CClanAppDelegate
    
- (void) setVersionInfoInSettings
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    [userDefaults setObject:appVersion forKey:@"version"];
    [userDefaults synchronize];
}

- (void) preloadData
{
    // Pre-load instances
    [CClanAnnotationCollection instance];
    
    // Pre-load constant data
    [[CClanCalendar instance] loadEvents];
    
    // Pre-load mutable data. Here we have data that will be loaded everytime
    // a controller is made visible
    [[CClanNews    instance] loadNews];
}

// Override point for customization after application launch.
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#if defined(CCLAN_MAP_USE_GOOGLE)
    [GMSServices provideAPIKey:[CClanGoogleAPI apiKey]];
#endif
    [self setVersionInfoInSettings];
    [self preloadData];

    self.window.tintColor = [UIColor blackColor];
    return YES;
}
							
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    application.applicationIconBadgeNumber = 0;
    [[CClanTwitter instance] pollTwitterStatus];
}

- (void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Calendar Event", nil)
                                                    message:notification.alertBody
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];

    [alert show];
}

@end
