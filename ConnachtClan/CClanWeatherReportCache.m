//
//  CClanWeatherReportCache.m
//  ConnachtClan
//
//  Created by Warren Gavin on 08/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanConstants.h"
#import "CClanWeatherReportCache.h"
#import "CClanWeatherWWO.h"

#import "NSData+URL.h"

NSString * const CCLAN_WEATHER_LOADED = @"be.apokrupto.CClanWeather.WeatherLoaded";
NSString * const CCLAN_WEATHER_FAILED = @"be.apokrupto.CClanWeather.WeatherFailed";

static const NSTimeInterval           L_CACHE_EXPIRATION = 3 * 60 * 60;
static const CClanWeatherReportCache *g_instance;

// @@TODO Move to WWO class
static NSString * const L_WWO_WEATHER_URL = @"http://api.worldweatheronline.com/free/v1/weather.ashx?num_of_days=5&format=json&key=tgfakrdqdndzcy7gh8mafajc";

@interface CClanWeatherReportCache ()

@property (nonatomic, strong) const NSMutableDictionary *cache;
@property (nonatomic, strong) const NSTimer *timer;

@end

@implementation CClanWeatherReportCache

+ (const CClanWeatherReportCache *) instance;
{
    if ( nil == g_instance ) {
        g_instance = [[CClanWeatherReportCache alloc] init];
        g_instance.cache = [NSMutableDictionary dictionary];
        g_instance.timer = [NSTimer scheduledTimerWithTimeInterval:L_CACHE_EXPIRATION
                                                            target:g_instance.cache
                                                          selector:@selector(removeAllObjects)
                                                          userInfo:nil
                                                           repeats:YES];
    }
    
    return g_instance;
}

+ (void) loadWeatherForLocation:(NSString *)location atCoordinates:(const CLLocationCoordinate2D)coordinates
{
    const CClanWeatherReportCache *weather = [CClanWeatherReportCache instance];

    __block const CClanWeatherWWO *forecast = weather.cache[location];
    if ( forecast ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_WEATHER_LOADED object:forecast];
    }
    else {
        dispatch_queue_t get_weather = dispatch_queue_create("get weather", NULL );
        
        dispatch_async( get_weather, ^{
            NSString *url = [NSString stringWithFormat:@"%@&q=%f,%f", L_WWO_WEATHER_URL, coordinates.latitude, coordinates.longitude];
            
            NSData *weather_data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]
                                                     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData  // Never use a cached weather report
                                                 timeoutInterval:CCLAN_URL_REQUEST_TIMEOUT];
            
            if ( weather_data ) {
                forecast = [[CClanWeatherWWO alloc] init];
                forecast.location = location;
                [forecast parseWeather:weather_data];
                weather.cache[location] = forecast;
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_WEATHER_LOADED object:forecast];
                });
            }
            else {
                dispatch_async( dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_WEATHER_FAILED object:nil];
                } );
            }
        });
    }
}

@end

