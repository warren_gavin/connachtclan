//
//  CClanNavigationViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 23/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CClanNavigationViewController : UINavigationController

@property (nonatomic, weak) IBOutlet UIBarButtonItem *sidebarButton;

@end
