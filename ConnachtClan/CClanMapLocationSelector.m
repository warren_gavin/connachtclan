//
//  CClanMapLocationSelector.m
//  ConnachtClan
//
//  Created by Warren Gavin on 01/07/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanMapLocationSelector.h"
#import "CClanMapLocationCell.h"
#import "CClanBundle.h"
#import "CClanDownloadedImagesCache.h"
#import "UIImage+RoundedCorner.h"

@implementation CClanMapLocationSelector

- (void) setLocations:(NSArray *)locations
{
    _locations = [locations sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *lhs, NSDictionary *rhs) {
        NSString *lhs_location = lhs[@"location"];
        NSString *rhs_location = rhs[@"location"];
        
        return [lhs_location compare:rhs_location];
    }];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.locations.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Map Locator";
    CClanMapLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    if ( !cell ) {
        cell = [[CClanMapLocationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }

    NSDictionary * const location = self.locations[indexPath.row];
    NSString * const imagepath = location[@"image"];

    cell.title.text = location[@"location"];
    cell.imageview.image = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:imagepath]];

    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *location = self.locations[indexPath.row];
    
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate mapLocationSelector:self didSelectLocation:location[@"location"]];
}

@end
