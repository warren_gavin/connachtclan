//
//  NSArray+Plist.m
//  ConnachtClan
//
//  Created by Warren Gavin on 06/01/13.
//
//

#import "NSArray+Plist.h"
#import "NSPropertyListSerialization+URL.h"

@implementation NSArray (Plist)

+ (NSArray *) arrayFromPlistAtURL:(NSURL *)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval
{
    id plist = [NSPropertyListSerialization plistWithContentsOfURL:url cachePolicy:cachePolicy timeoutInterval:timeoutInterval];

    if ( ![plist isKindOfClass:[NSArray class]] ) {
        plist = nil;
    }

    return plist;
}

@end
