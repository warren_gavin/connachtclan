//
//  CClanCalendarMonthView.m
//  ConnachtClan
//
//  Created by Warren Gavin on 26/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanCalendarMonthView.h"
#import "CClanBundle.h"

@interface CClanCalendarMonthView()

@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, weak) UITableView *table;

@end

@implementation CClanCalendarMonthView

@synthesize month    = _month;
@synthesize calendar = _calendar;
@synthesize table    = _table;

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [self.calendar setFirstWeekday:2]; // Monday
    
    return self;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // TODO Fix
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO Fix
    return nil;
}

@end
