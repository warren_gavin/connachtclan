//
//  UIImage+Mask.h
//  ConnachtClan
//
//  Created by Warren Gavin on 02/01/14.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Mask)

- (UIImage *) maskedImageForColour:(UIColor *)color;

@end
