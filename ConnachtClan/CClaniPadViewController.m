//
//  CClaniPadViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 18/08/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClaniPadViewController.h"
#import "CClanErrorHandler.h"

// CClanNews
#import "CClanNews.h"
#import "CClanNewsViewCell.h"
#import "CClanNewsItemViewController.h"
#import "CClanRssItem.h"

// CClanCalendar
#import "CClanCalendar.h"
#import "CClanCalendarEventHeaderViewCell.h"
#import "CClanCalendarEventCell.h"
#import "CClanCalendarEvent.h"
#import "CClanCalendarEventDetailsViewController.h"

// CClanTwitter
#import "CClanTwitter.h"
#import "CClanTwitterTweetViewCell.h"

// CClanVideos
#import "CClanVideos.h"
#import "CClanVideoViewCell.h"
#import "CClanVideoDisplayViewController.h"

@interface CClaniPadViewController () <CClanNewsDelegate,
                                       CClanCalendarDelegate,
                                       CClanTwitterDelegate,
                                       CClanVideosDelegate,
                                       AQGridViewDataSource>

@property (strong, nonatomic) const CClanNews * const news;
@property (strong, nonatomic) const CClanCalendar * const calendar;
@property (strong, nonatomic) const CClanTwitter * const twitter;
@property (strong, nonatomic) const CClanVideos * const videoloader;
@property (strong, nonatomic) const NSArray * const videos;

@property (weak, nonatomic) NSArray * const timeline;

@end

@implementation CClaniPadViewController

@synthesize clanNewsTable    = _clanNewsTable;
@synthesize clanNewsActivity = _clanNewsActivity;
@synthesize clanCalendarTable = _clanCalendarTable;
@synthesize clanCalendarActivity = _clanCalendarActivity;
@synthesize clanTwitterTable = _clanTwitterTable;
@synthesize clanTwitterActivity = _clanTwitterActivity;
@synthesize clanVideoGrid = _clanVideoGrid;
@synthesize clanVideoActivity = _clanVideoActivity;

@synthesize news     = _news;
@synthesize calendar = _calendar;
@synthesize twitter  = _twitter;
@synthesize timeline = _timeline;
@synthesize videoloader = _videoloader;
@synthesize videos      = _videos;

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        self.news = [CClanNews instance];
        self.news.delegate = self;
        [self.news loadNews];

        self.calendar = [CClanCalendar instance];
        self.calendar.delegate = self;
        [self.calendar loadEvents];
        
        self.twitter = [CClanTwitter instance];
        self.twitter.delegate = self;
        [self.twitter reloadTimeline:nil];

        self.videoloader = [CClanVideos instance];
        self.videoloader.delegate = self;
        [self.videoloader loadVideos];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.clanVideoGrid.layoutDirection = AQGridViewLayoutDirectionHorizontal;

    [self.clanNewsActivity startAnimating];
    [self.clanCalendarActivity startAnimating];
    [self.clanTwitterActivity startAnimating];
    [self.clanVideoActivity startAnimating];
}

- (void)viewDidUnload
{
    [self setClanNewsTable:nil];
    [self setClanNewsActivity:nil];
    [self setClanCalendarTable:nil];
    [self setClanCalendarActivity:nil];
    [self setClanTwitterTable:nil];
    [self setClanTwitterActivity:nil];
    [self setClanVideoGrid:nil];
    [self setClanVideoActivity:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:@"News Item"] ) {
        const CClanRssItem * const item = sender;
        [segue.destinationViewController setUrl:item.link];
    }
    else if ( [segue.identifier isEqualToString:@"Calendar Event"] ) {
        CClanCalendarEventCell *cell = sender;
        [segue.destinationViewController setEvent:cell.event];
    }
    else if ( [segue.identifier isEqualToString:@""] ) {
        const CClanVideoViewCell const              *cell  = sender;
        const CClanVideoDisplayViewController const *video = segue.destinationViewController;
        
        video.clip_html = cell.clip_html;
        video.details = cell.details_text;
    }
}

#pragma mark - CClanNewsDelegate
- (void) newsReaderDidFinishLoading:(const CClanNews *const)reader
{
    [self.clanNewsActivity stopAnimating];
    [self.clanNewsTable reloadData];
}

#pragma mark - CClanCalendarDelegate
- (void) calendarDidFinishLoadingEvents:(CClanCalendar *)calendar
{
    [self.clanCalendarActivity stopAnimating];
    [self.clanCalendarTable reloadData];
}

#pragma mark - CClanTwitterDelegate
- (void) timelineLoader:(const CClanTwitter *const)loader didFailWithError:(const NSError *const)error
{
    [CClanErrorHandler handleError:error withIndicator:self.clanTwitterActivity];
}

- (void) timelineLoaderDidFinishLoading:(const CClanTwitter *const)loader
{
    self.timeline = loader.timeline;

    [self.clanTwitterActivity stopAnimating];
    [self.clanTwitterTable reloadData];
}

#pragma mark - CClanVideosDelegate
- (void) videoLoader:(const CClanVideos *const)loader didFailWithError:(const NSError *const)error
{
    [CClanErrorHandler handleError:error withIndicator:self.clanVideoActivity];
}

- (void) videoLoader:(const CClanVideos *const)loader didReceiveVideos:(const NSArray *const)videos
{
    self.videos = videos;

    [self.clanVideoActivity stopAnimating];
    [self.clanVideoGrid reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ( self.clanCalendarTable == tableView ) {
        return [self.calendar numberOfMonthsWithEvents];
    }
    
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( self.clanNewsTable == tableView ) {
        const NSArray * const items = [self.news connachtClanNews];
        return items.count;
    }
    else if ( self.clanCalendarTable == tableView ) {
        // Add one for the header cell
        return [self.calendar numberOfEventsForMonthAtIndex:section] + 1;
    }
    else if ( self.clanTwitterTable == tableView ) {
        return self.timeline.count;
    }

    return 0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.clanNewsTable == tableView ) {
        const CClanRssItem * const item = [[self.news connachtClanNews] objectAtIndex:indexPath.row];
        CClanNewsViewCell * const cell = [tableView dequeueReusableCellWithIdentifier:@"News Item"];
        
        cell.description.text = item.description;
        cell.title.text       = item.title;

        return cell;
    }
    else if ( self.clanCalendarTable == tableView ) {
        if ( 0 == indexPath.row ) {
            static NSString *header_identifier = @"Calendar Event Header";
            CClanCalendarEventHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:header_identifier];
            
            cell.month.text = [self.calendar titleForMonthAtIndex:indexPath.section];
            return cell;
        }
        
        static NSString *event_identifier = @"Calendar Event";
        CClanCalendarEventCell *cell = [tableView dequeueReusableCellWithIdentifier:event_identifier];
        
        [cell setEventData:[[self.calendar eventsForMonth:indexPath.section] objectAtIndex:indexPath.row - 1]];
        
        return cell;
    }
    else if ( self.clanTwitterTable == tableView ) {
        static NSString *twitter_identifier = @"TwitterCell";
        CClanTwitterTweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:twitter_identifier];
        
        [cell displayTweet:[self.timeline objectAtIndex:indexPath.row]];
        
        return cell;
    }

    return nil;
}

#pragma  mark - UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.clanNewsTable == tableView ) {
        const CClanRssItem * const item = [[self.news connachtClanNews] objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"News Item" sender:item];
    }
    else if ( self.clanCalendarTable == tableView ) {
        CClanCalendarEventCell *cell = (CClanCalendarEventCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if ( cell.event.url || cell.event.woeid ) {
            [self performSegueWithIdentifier:@"Calendar Event" sender:cell];
        }
    }
}

#pragma mark - AQGridViewDataSource
- (NSUInteger) numberOfItemsInGridView:(AQGridView *)gridView
{
    return self.videos.count;
}

- (AQGridViewCell *) gridView:(AQGridView *)gridView cellForItemAtIndex:(NSUInteger)index
{
    static NSString *identifier = @"Video cell";
    CClanVideoViewCell *cell = (CClanVideoViewCell *)[gridView dequeueReusableCellWithIdentifier:identifier];
    
    if ( !cell ) {
        cell = [[CClanVideoViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:identifier];
    }
    
    [cell setVideo:[self.videos objectAtIndex:index]];
    
    return (AQGridViewCell *)cell;
}


@end
