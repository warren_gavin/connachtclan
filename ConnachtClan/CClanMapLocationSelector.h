//
//  CClanMapLocationSelector.h
//  ConnachtClan
//
//  Created by Warren Gavin on 01/07/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CClanMapLocationSelector;

@protocol CClanMapLocationSelectorDelegate <NSObject>

- (void) mapLocationSelector:(CClanMapLocationSelector *)locator didSelectLocation:(NSString *)location;

@end

//! Table view controller for a list of possible locations to display on the map
@interface CClanMapLocationSelector : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *table;

//! Array of dictionary types with location names and icons
@property (nonatomic, strong) NSArray *locations;

//! A delegate that will be called when a location has been selected
@property (nonatomic, weak) id<CClanMapLocationSelectorDelegate> delegate;

@end
