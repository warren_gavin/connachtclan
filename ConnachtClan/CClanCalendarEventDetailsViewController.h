//
//  CClanCalendarEventDetailsViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 22/04/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CClanCalendarEvent;
@class CClanCalendarEventDetailsView;
@class CClanCalendarEventWeatherView;

//! Controller than contains both the event view and the weather view, displaying 
//! whichever view is selected in a segmented control
@interface CClanCalendarEventDetailsViewController : UIViewController

//! The event view
@property (weak, nonatomic) IBOutlet CClanCalendarEventDetailsView *eventview;

//! The event details that are displayed in the event view
- (void) setEvent:(const CClanCalendarEvent *)event;

@end
