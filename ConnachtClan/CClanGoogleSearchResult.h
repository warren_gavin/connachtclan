//
//  CClanGoogleSearchResult.h
//  ConnachtClan
//
//  Created by Warren Gavin on 26/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CClanMapSearchResult.h"

@interface CClanGoogleSearchResult : NSObject <CClanMapSearchResult>

@property (nonatomic, strong) NSString * const name;
@property (nonatomic, strong) NSArray  * const categories;
@property (nonatomic, strong) NSString * const address;
@property (nonatomic, strong) NSString * const rating;
@property (nonatomic, strong) NSString * const imageRef;
@property (nonatomic, strong) NSString * const city;
@property (nonatomic, strong) NSString * const country;
@property (nonatomic, strong) NSString * const phone;
@property (nonatomic, strong) NSString * const website;

@property (nonatomic) CLLocationCoordinate2D location;

@end
