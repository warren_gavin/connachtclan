//
//  NSMutableArray+Sorting.m
//  ConnachtClan
//
//  Created by Warren Gavin on 23/01/13.
//
//

#import "NSMutableArray+Sorting.h"

@implementation NSMutableArray (Sorting)

- (void) moveElementAtIndex:(NSUInteger)source toIndex:(NSUInteger)destination
{
    if ( source != destination ) {
        id object = self[source];
        [self removeObjectAtIndex:source];
        
        if ( destination >= self.count ) {
            [self addObject:object];
        }
        else {
            [self insertObject:object atIndex:destination];
        }
    }
}

@end
