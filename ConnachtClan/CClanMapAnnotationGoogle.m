//
//  CClanMapAnnotationGoogle.m
//  ConnachtClan
//
//  Created by Warren Gavin on 11/11/13.
//
//

#if defined(CCLAN_MAP_USE_GOOGLE)
#import "CClanMapAnnotationGoogle.h"
#import "CClanAnnotation.h"
#import "CClanMapSettings.h"

@implementation CClanMapAnnotationGoogle

@synthesize annotation = _annotation;

- (UIImage *)iconForAnnotation
{
    if ( [_annotation isMemberOfClass:[CClanExtendedAnnotation class]] ) {
        CClanExtendedAnnotation *extAnnotation = (CClanExtendedAnnotation *)_annotation;
        if ( !extAnnotation.annotationImage ) {
            extAnnotation.annotationImage = [UIImage imageNamed:extAnnotation.annotationImagePath];
        }
        return extAnnotation.annotationImage;
    }

    CClanMapSettings *settings = [CClanMapSettings instance];
    NSString         *icon;
    
    if ( [[CClanMapSettings instance] isPermanent:_annotation] ) {
        icon = @"permanent.png";
    }
    else if ( [_annotation.types containsObject:@"Accommodation"] && [settings.showable containsObject:@"Accommodation"] ) {
        icon = @"hotel-marker";
    }
    else if ( [_annotation.types containsObject:@"Bar"] && [settings.showable containsObject:@"Bar"] ) {
        icon = @"bar-marker";
    }
    else if ( [_annotation.types containsObject:@"Restaurant"] && [settings.showable containsObject:@"Restaurant"] ) {
        icon = @"restaurant-marker";
    }
    else {
        icon = @"cafe-marker";
    }

    return [UIImage imageNamed:icon];
}

- (void) setAnnotation:(CClanAnnotation *)annotation
{
    if ( _annotation != annotation ) {
        _annotation   = annotation;
        
        self.position = _annotation.location;
        self.title    = _annotation.name;
        self.snippet  = [_annotation details];
        self.icon     = [self iconForAnnotation];
    }
}

//! Overload the setMap: method to make sure we use the right icon marker
//!
//! This is important because this marker may be being added to the map after
//! being removed as one annotation type and added as a new one. When this happens
//! the annotation image must be updated
- (void) setMap:(GMSMapView *)map
{
    [super setMap:map];
    self.icon = [self iconForAnnotation];
}

- (BOOL) isEqual:(id)object
{
    CClanMapAnnotationGoogle *mapAnnotation = object;
    return [_annotation.description isEqualToString:mapAnnotation.annotation.description];
}

@end

#endif
