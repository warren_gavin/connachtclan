//
//  EKReminder+CClanEvent.h
//  ConnachtClan
//
//  Created by Warren Gavin on 18/11/12.
//
//

#import <EventKit/EventKit.h>

@class CClanCalendarEvent;

@interface EKReminder (CClanEvent)

- (void) setClanEvent:(const CClanCalendarEvent * const) event calendar:(EKCalendar * const) calendar alert:(BOOL)alert;

@end
