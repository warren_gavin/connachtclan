//
//  CClanGoogleAPI.h
//  ConnachtClan
//
//  Created by Warren Gavin on 11/11/13.
//
//

#import <Foundation/Foundation.h>

@interface CClanGoogleAPI : NSObject

+ (NSString *) apiKey;

@end
