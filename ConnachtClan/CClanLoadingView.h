//
//  CClanLoadingView.h
//  ConnachtClan
//
//  Created by Warren Gavin on 21/10/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CClanLoadingView : UIView

- (void) setText:(NSString * const)text;

- (void) showLoadingDisplay;
- (void) hideLoadingDisplay;

@end
