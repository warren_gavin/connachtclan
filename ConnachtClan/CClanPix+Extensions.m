//
//  CClanPix+Extensions.m
//  ConnachtClan
//
//  Created by Warren Gavin on 16/02/13.
//
//

#import "CClanPix+Extensions.h"
#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"

@implementation CClanPix (Extensions)

NSString * const CCLAN_PIX_IMAGE_LOADED = @"be.apokrupto.CClanPix.ImageLoaded";

- (UIImage *) thumbnailForSize:(CGSize) size
{
    if ( !self.thumbnail ) {
        UIImage *image = [UIImage imageWithData:self.imageData];
        CGFloat  scale = [[UIScreen mainScreen] scale];
        
        UIImage *thumbnail = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                         bounds:CGSizeMake(scale * size.width,
                                                                           scale * size.height)
                                           interpolationQuality:kCGInterpolationDefault];
        
        self.thumbnail = UIImagePNGRepresentation([thumbnail roundedCornerImage:6 borderSize:0]);
    }
    
    return [UIImage imageWithData:self.thumbnail];
}

+ (NSURL *) urlForClanPixLocation
{
    NSURL *url;
    
#if 0
    url = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    if ( !url ) {
        url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    }
    else {
        url = [url URLByAppendingPathComponent:@"Documents"];
    }
#else
    url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"ClanPix"];
#endif

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ( ![fileManager fileExistsAtPath:[url path]] ) {
        [fileManager createDirectoryAtPath:[url path] withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    return url;
}

+ (NSURL *) urlForClanPixDatabase
{
    return [[CClanPix urlForClanPixLocation] URLByAppendingPathComponent:@"ClanPix.db"];
}

+ (CClanPix *) clanPixWithImage:(UIImage * const)image forManagedObjectContext:(NSManagedObjectContext *)moc
{
    CClanPix *pic = [NSEntityDescription insertNewObjectForEntityForName:@"CClanPix" inManagedObjectContext:moc];
    
    // Setting the image may take a while, so post a notification when it's complete. Display
    // cells will wait for this notification before displaying the image.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        pic.imageData = UIImagePNGRepresentation(image);
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:CCLAN_PIX_IMAGE_LOADED object:self];
        });
    });
    
    return pic;
}

// Override this method to set default values in the managed object before any insert notifications
// are posted
- (void) awakeFromInsert
{
    [super awakeFromInsert];
    
    // Set date here. If we set the date in the constructor it may not be set before the
    // insert notification is posted and notification observers will have null data
    self.creationDate = [NSDate date];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<ClanPix { creationDate: %@ }>", self.creationDate];
}

@end
