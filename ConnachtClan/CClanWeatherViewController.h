//
//  CClanWeatherViewController.h
//  ConnachtClan
//
//  Created by Warren Gavin on 19/01/13.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "CClanLoadingView.h"
#import "CClanWeatherReport.h"
#import "CClanCalendarEventWeatherView.h"

@interface CClanWeatherViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView      *table;
@property (weak, nonatomic) IBOutlet CClanLoadingView *loading;

@property (weak, nonatomic) NSString * const           location;
@property (nonatomic)       CLLocationCoordinate2D     coordinates;

@end
