//
//  CClanPicturesViewController.m
//  ConnachtClan
//
//  Created by Warren Gavin on 27/05/12.
//  Copyright (c) 2012 Apokrupto. All rights reserved.
//

#import "CClanPicturesViewController.h"
#import "CClanPictureViewCell.h"
#import "CClanPictureInspectorViewController.h"
#import "CClanPictures.h"
#import "CClanPictureAsset.h"
#import "CClanPictureAssetButton.h"
#import "CClanBundle.h"
#import "CClanErrorHandler.h"

#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"

static const CGFloat     L_MAX_PHOTO_DIMENSION    = 620.0;
static const CGFloat     L_MAX_CELL_WIDTH         = 250.0;

static NSString *L_CLAN_PIX_INTRO = @"The ClanPix photo album is your collection of Connacht Rugby and Clan photos.\n\nShare them with the Clan via twitter, import photos from existing photo albums and export them to your device's photo album";

//! Generic error handler
void (^handleFailure)(NSError *error) = ^(NSError *error) { [CClanErrorHandler handleError:error withIndicator:nil]; };

@interface CClanPicturesViewController () <UIImagePickerControllerDelegate,
                                           UINavigationControllerDelegate,
                                           UIPopoverControllerDelegate,
                                           CClanPictureInspectorDelegate,
                                           CClanPicturesDelegate>

//! Internal photo album
@property (strong, nonatomic) CClanPictures *photos;

//! Photo inspector - shows the photo in more detail and allows operations
//! to be performed on the photo, such as deleting, tweeting etc
@property (strong, nonatomic) UIPopoverController *popover;

//! Launch an instance of the devices camera, if supported
- (void) launchCamera:(UIBarButtonItem *)sender;

//! Import a photo from the Devices photo library
- (void) addPhotosFromCameraRoll:(UIBarButtonItem *)sender;

@end



@implementation CClanPicturesViewController

@synthesize grid             = _grid;
@synthesize description      = _description;
@synthesize descriptionbg    = _descriptionbg;
@synthesize loading          = _loading;
@synthesize photos           = _photos;
@synthesize popover          = _popover;

//! The grid' cells have different sizes depending on the device
- (CGFloat) maxCellWidth
{
    CGFloat width = L_MAX_CELL_WIDTH;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        width /= 2.5;
    }
    
    return width;
}

//! A cell's height is three quarters of the width
- (CGFloat) maxCellHeight
{
    return [self maxCellWidth] * 0.75;
}

//! Sets the buttons to display on the view navigation bar. 
- (void) loadBarButtons
{
    UIBarButtonItem *camera = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                            target:self
                                                                            action:@selector(launchCamera:)];
    
    UIBarButtonItem *addpic = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                            target:self
                                                                            action:@selector(addPhotosFromCameraRoll:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:camera, addpic, nil];
}

#pragma mark - UIViewController
- (void) viewDidLoad
{
    [super viewDidLoad];

    self.description.text = L_CLAN_PIX_INTRO;
    self.description.hidden = YES;
    self.descriptionbg.hidden = YES;

    self.photos = [CClanPictures instance];
    self.photos.delegate = self;

    [self.loading showLoadingDisplay];
    [self.photos loadPhotoAlbum];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self loadBarButtons];
}

- (void) viewDidUnload
{
    self.grid    = nil;
    self.photos  = nil;
    self.popover = nil;
    
    [self setLoading:nil];
    [self setDescription:nil];
    [self setDescriptionbg:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
         UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        return NO;
    }

    return YES;
}

//! Creates an image picker object of the specified type
- (UIImagePickerController *)imagePickerOfType:(UIImagePickerControllerSourceType)type
{
    UIImagePickerController *controller = nil;

    if ( [UIImagePickerController isSourceTypeAvailable:type] ) {
        NSString *imagetype = (NSString *)kUTTypeImage;
        NSArray  *available = [UIImagePickerController availableMediaTypesForSourceType:type];

        if ( [available containsObject:imagetype] ) {
            controller = [[UIImagePickerController alloc] init];
            
            controller.delegate = self;
            controller.sourceType = type;
            controller.mediaTypes = [NSArray arrayWithObject:imagetype];
        }
    }

    return controller;
}

//! Open a camera view 
- (void) launchCamera:(UIBarButtonItem *)sender 
{
    UIImagePickerController *controller = [self imagePickerOfType:UIImagePickerControllerSourceTypeCamera];

    if ( controller ) {
        controller.allowsEditing = NO;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        [CClanErrorHandler displayAlert:@"Can't find a camera."];
    }
}

//! Open a popover to allow the user to select images to add to the photo albu,
- (void) addPhotosFromCameraRoll:(UIBarButtonItem *)sender
{
    UIImagePickerController *controller = [self imagePickerOfType:UIImagePickerControllerSourceTypePhotoLibrary];

    if ( controller ) {
        controller.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            if ( !self.popover ) {
                self.popover = [[UIPopoverController alloc] initWithContentViewController:controller];
            }

            [self.popover presentPopoverFromBarButtonItem:sender
                                 permittedArrowDirections:UIPopoverArrowDirectionAny
                                                 animated:YES];
        }
        else {
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else {
        [CClanErrorHandler displayAlert:@"Can't find the photo library."];
    }
}

- (void) buttonClicked:(CClanPictureAssetButton *)button
{
    [self performSegueWithIdentifier:@"Picture Inspector" sender:button];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    const CClanPictureAssetButton const       *button    = sender;
    const CClanPictureInspectorViewController *inspector = segue.destinationViewController;

    inspector.delegate = self;
    [inspector setImageToDisplay:button.asset];
    
    self.navigationItem.rightBarButtonItems = nil;
}

#pragma mark - CClanPictureInspector delegate methods
- (void) elementWasSelectedForDeletion:(CClanPictureAsset *)asset
{
    [self.photos removePhotoFromAlbum:asset];
    [self.grid reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) saveElement:(CClanPictureAsset *)asset
{
    [self.loading setText:@"Saving..."];
    [self.loading showLoadingDisplay];
    [self.photos addPhotoToSavedPhotos:asset.image];
}

#pragma mark - UIImagePickerController delegate methods

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *orig = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *logo = [UIImage imageWithContentsOfFile:[CClanBundle fileInBundle:@"clanpics.png"]];
    
    UIImage *pic = [orig resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                              bounds:CGSizeMake(L_MAX_PHOTO_DIMENSION, L_MAX_PHOTO_DIMENSION)
                                interpolationQuality:kCGInterpolationHigh];

    UIGraphicsBeginImageContextWithOptions(pic.size, NO, [[UIScreen mainScreen] scale]);
    [pic drawInRect:CGRectMake(0, 0, pic.size.width, pic.size.height)];
    [logo drawInRect:CGRectMake(pic.size.width - logo.size.width - 10,
                                pic.size.height - logo.size.height - 10,
                                logo.size.width,
                                logo.size.height)];

    UIImage *clanpic = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.popover dismissPopoverAnimated:YES];

    [self.loading showLoadingDisplay];
    [self.photos addPhotoToAlbum:clanpic];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIPopoverController delegate methods
- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

#pragma mark - AQGridView dataSource methods
- (NSUInteger) numberOfItemsInGridView:(AQGridView *)gridView
{
    NSUInteger count = [self.photos albumSize];

    if ( count ) {
        self.description.hidden = YES;
        self.descriptionbg.hidden = YES;
    }
    else {
        self.description.hidden = NO;
        self.descriptionbg.hidden = NO;
    }

    return count;
}

- (AQGridViewCell *) gridView: (AQGridView *) gridView cellForItemAtIndex: (NSUInteger) index
{
    static NSString *identifier = @"Picture View Cell";
    CClanPictureViewCell *cell = (CClanPictureViewCell *)[gridView dequeueReusableCellWithIdentifier:identifier];

    if ( !cell ) {
        cell = [[CClanPictureViewCell alloc] initWithFrame:CGRectMake(0.0, 0.0, [self maxCellWidth] - 10.0, [self maxCellHeight] - 10.0)
                                           reuseIdentifier:identifier];

        [cell.button addTarget:self
                        action:@selector(buttonClicked:)
              forControlEvents:UIControlEventTouchUpInside];
    }

    CClanPictureAsset * const asset = [self.photos pictureInAlbumAtIndex:index];
    cell.button.asset = asset;

    CGFloat scale = [[UIScreen mainScreen] scale];
    UIImage *thumbnail = [asset.image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                           bounds:CGSizeMake(scale * cell.button.bounds.size.width,
                                                                             scale * cell.button.bounds.size.height)
                                             interpolationQuality:kCGInterpolationDefault];

    cell.button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell.button setImage:[thumbnail roundedCornerImage:6 borderSize:1]
                 forState:UIControlStateNormal];


    return cell;
}

- (CGSize) portraitGridCellSizeForGridView:(AQGridView *) gridView
{
    return CGSizeMake([self maxCellWidth], [self maxCellHeight]);
}

#pragma mark - CClanPicturesDelegate
- (void) clanPicturesDidFinishLoading:(const CClanPictures *const)clanpix
{
    [self.loading hideLoadingDisplay];
    [self.grid reloadData];
}

- (void) clanPicturesDidFail:(const CClanPictures *const)clanpix withError:(NSError * const)error
{
    handleFailure( error );
}

@end
